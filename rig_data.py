from imp import reload
import os
import re
import json

import maya.cmds as mc
import maya.OpenMaya as om
import maya.OpenMayaAnim as oma

# pkrig Modules
from . import core as prc
from . import skin_tools
from . import blend_shape_tools as bst

reload(prc)
reload(skin_tools)
reload(bst)

RIG_DATA = "rig_data"
PRERUN = "prerun"
SKIN_DATA = "skin"
CTRL_DATA = "ctrl"
BLEND_SHAPE = "blendshape"
SCRIPT = "python"
CONTROL_SUFFIXES = ("Ctrl", "Gmbl", "ConCtrl")


def get_rig_data_path(cwd):
    """Gets rig data path from the current scene.

    Args:
        cwd (str/None): Current working directory.
            If None is given, script will find cwd from current scene path.

    Returns:
        str: A path to current rig data folder.
    """
    if not cwd:
        scene_path = mc.file(q=True, sn=True)
        if not scene_path:
            scene_path = mc.file(q=True, l=True)[0]
        cwd = os.path.dirname(os.path.abspath(scene_path))
    rig_data_path = os.path.join(cwd, RIG_DATA)

    if not os.path.exists(rig_data_path):
        os.mkdir(rig_data_path)

    return rig_data_path


def get_data_path(cwd, data):
    """
    Args:
        cwd (str/None): Current working directory.
            If None is given, script will find cwd from current scene path.
        data (str): A name of the data type. It is created as a folder
            in rig_data.

    Returns:
        str: A path to created data folder.
    """
    rig_data_path = get_rig_data_path(cwd)
    data_path = os.path.join(rig_data_path, data)

    if not os.path.exists(data_path):
        os.mkdir(data_path)

    return data_path


def get_skin_data_path(cwd):
    """
    Args:
        cwd (str/None): Current working directory.
            If None is given, script will find cwd from current scene path.
    """
    return get_data_path(cwd, SKIN_DATA)


def get_ctrl_data_path(cwd):
    """
    Args:
        cwd (str/None): Current working directory.
            If None is given, script will find cwd from current scene path.
    """
    return get_data_path(cwd, CTRL_DATA)


def get_prerun_path(cwd):
    """
    Args:
        cwd (str/None): Current working directory.
            If None is given, script will find cwd from current scene path.
    """
    return get_data_path(cwd, PRERUN)


def get_script_path(cwd):
    """
    Args:
        cwd (str/None): Current working directory.
            If None is given, script will find cwd from current scene path.
    """
    return get_data_path(cwd, SCRIPT)


def get_blend_shape_path(cwd):
    """
    Args:
        cwd (str/None): Current working directory.
            If None is given, script will find cwd from current scene path.
    """
    return get_data_path(cwd, BLEND_SHAPE)


def write_skin_to(geo, file_path):
    """
    Args
        geo (str):
        file_path (str):
    """
    geo = prc.Dag(geo)
    skindat = skin_tools.get_skin_data_from_bound_object(geo)
    with open(file_path, "w") as jsonfile:
        json.dump(skindat, jsonfile, indent=4, sort_keys=True)

    return True


def write_skin(geo):
    """Writes skin data of the selected geos."""
    skin_data_path = get_skin_data_path(None)
    write_skin_to(geo, os.path.join(skin_data_path, "{}.json".format(geo)))

    return True


def write_selected_skin():
    """Writes skin data of the selected geos."""
    for sel in mc.ls(sl=True):
        write_skin(sel)

    return True


def read_skin_from(geo, file_path):
    geo = prc.Dag(geo)
    with open(file_path, "r") as jsonfile:
        skindat = json.load(jsonfile)
        skin_tools.bind_skin_data_to(skindat, geo)

    return True


def set_skin_from(geo, file_path):
    geo = prc.Dag(geo)
    with open(file_path, "r") as jsonfile:
        skindat = json.load(jsonfile)
        skin_tools.set_skin_data_to(skindat, geo)

    return True


def read_skin(geo):
    skin_data_path = get_skin_data_path(None)
    read_skin_from(geo, os.path.join(skin_data_path, "{}.json".format(geo)))

    return True


def set_skin(geo):
    skin_data_path = get_skin_data_path(None)
    set_skin_from(geo, os.path.join(skin_data_path, "{}.json".format(geo)))

    return True


def read_selected_skin():
    for sel in mc.ls(sl=True):
        read_skin(sel)

    return True


def get_all_ctrls():
    ctrls = []
    for ctrlsuffix in CONTROL_SUFFIXES:
        ctrls += mc.ls("*_{}".format(ctrlsuffix))

    return ctrls


def write_ctrl_to(file_path):
    ctrldat = {}
    for ctrl in get_all_ctrls():
        shapes = mc.listRelatives(ctrl, s=True)
        if not shapes:
            continue
        if mc.nodeType(shapes[0]) != "nurbsCurve":
            continue

        for cv in prc.get_all_components(shapes[0]):
            ctrldat[cv] = mc.xform(cv, q=True, os=True, t=True)

    with open(file_path, "w") as jsonfile:
        json.dump(ctrldat, jsonfile, indent=4, sort_keys=True)

    return True


def write_ctrl(file_name):
    ctrl_dat_path = get_ctrl_data_path(None)
    ctrl_file_path = os.path.join(ctrl_dat_path, "{}.json".format(file_name))
    write_ctrl_to(ctrl_file_path)

    return True


def read_ctrl(file_name):
    ctrl_dat_path = get_ctrl_data_path(None)
    ctrl_file_path = os.path.join(ctrl_dat_path, "{}.json".format(file_name))
    if not os.path.exists(ctrl_file_path):
        return False
    with open(ctrl_file_path, "r") as jsonfile:
        ctrldat = json.load(jsonfile)
        for cv in ctrldat:
            if not mc.objExists(cv):
                continue
            mc.xform(cv, os=True, t=ctrldat[cv])

    return True


def write_blend_shape_to(geo, file_path):
    """Writes out the blend shape that's connected to the given geo.

    Args
        geo (str):
        file_path (str):
    """
    geo = prc.Dag(geo)
    blend_nodes = bst.get_connected_blend_shape(geo)

    if not blend_nodes:
        return False

    data = bst.get_data(blend_nodes[0])
    with open(file_path, "w") as jsonfile:
        json.dump(data, jsonfile, indent=4, sort_keys=True)

    return True


def write_blend_shape(geo):
    data_path = get_blend_shape_path(None)
    write_blend_shape_to(geo, os.path.join(data_path, "{}.json".format(geo)))


def write_selected_blend_shape():
    for sel in mc.ls(sl=True):
        write_blend_shape(sel)

    return True


def read_blend_shape_from(geo, file_path):
    geo = prc.Dag(geo)
    with open(file_path, "r") as jsonfile:
        dat = json.load(jsonfile)
        bst.rebuild_data_to(geo, dat)

    return True


def read_blend_shape(geo):
    data_path = get_blend_shape_path(None)
    read_blend_shape_from(geo, os.path.join(data_path, "{}.json".format(geo)))


def read_selected_blend_shape():
    for sel in mc.ls(sl=True):
        read_blend_shape(sel)

    return True
