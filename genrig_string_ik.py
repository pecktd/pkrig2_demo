# System Modules
from imp import reload

# Maya Modules
import maya.cmds as mc
import maya.OpenMaya as om

# pkrig Modules
from . import core as prc
from . import utils as pru
from . import naming_tools as prnt
from . import rig_base
from . import sticker
from . import spline_weight

reload(prc)
reload(rig_base)


class StringCurve(rig_base.BaseRig):

    def __init__(
            self,
            curve,
            control_shape,
            joint_amount,
            tip_offset,
            mod,
            desc,
            side,
            ctrl_grp_parent,
            skin_parent
    ):
        """Attaches controllers to the given curve with
        twist distribution and squesh.

        The meta node is snapped to the position of the given ctrl_grp_parent
        and its X axis is an up vector of all controls by default.

        Args:
            curve (prc.Dag):
            control_shape (prc.Cp):
            joint_amount (int): Has to > 1
            tip_offset (float): From 0.00 to 1.00/(joint_amount - 1.00)
            mod (str):
            desc (str):
            side (prc.Side/None):
            ctrl_grp_parent (prc.Dag):
            skin_parent (prc.Dag):
        """
        # Main Groups
        super(StringCurve, self).__init__(None, mod, desc, False)

        self.meta.name = prnt.compose(
            '{m}{d}'.format(m=self.mod_name, d=self.desc), None, side, 'Grp')
        if ctrl_grp_parent:
            self.meta.snap(ctrl_grp_parent)
            self.meta.set_parent(ctrl_grp_parent)

        # Twist Attrs
        pru.add_divide_attr(curve, 'twist')
        curve.add(ln='autoTwist', min=0, max=1, k=True)
        curve.add(ln='twist', k=True)
        curve.add(ln='rootTwist', k=True)
        curve.add(ln='endTwist', k=True)
        curve.add(ln='rootAutoTwist', k=True)
        curve.add(ln='endAutoTwist', k=True)

        # Squesh Attrs
        pru.add_divide_attr(curve, 'squash')
        curve.add(ln='autoSquash', k=True, min=0, max=1)
        curve.add(ln='squash', k=True)
        curve.add(ln='currSquash', k=True)

        # Up/Aim Vector
        pru.add_divide_attr(curve, 'aim')
        pru.add_vector_attr(curve, 'aimVector', True)
        pru.add_vector_attr(curve, 'upVector', True)
        pru.add_vector_attr(curve, 'worldUpVector', True)

        curve.attr('aimVector').v = (0, 1, 0)
        curve.attr('upVector').v = (1, 0, 0)
        curve.attr('worldUpVector').v = (0, 1, 0)

        # Arc Length
        pru.add_divide_attr(curve, 'arcLen')
        self.curve_info = self.init_node(
            prc.create('curveInfo'), '{}Crv'.format(desc), None, side, 'Cif')
        curve.attr('worldSpace[0]') >> self.curve_info.attr('inputCurve')
        curve.add(ln='origLen', k=True, dv=self.curve_info.attr('arcLength').v)
        curve.add(ln='currLen', k=True, dv=self.curve_info.attr('arcLength').v)
        self.curve_info.attr('arcLength') >> curve.attr('currLen')

        # Squesh
        sqdiv = self.init_node(
            prc.create('multiplyDivide'), 'Sq', None, side, 'Div')
        sqpow = self.init_node(
            prc.create('multiplyDivide'), 'Sq', None, side, 'Pow')
        sqnorm = self.init_node(
            prc.create('multiplyDivide'), 'SqNorm', None, side, 'Div')

        sqdiv.attr('operation').v = 2
        sqpow.attr('operation').v = 3
        sqnorm.attr('operation').v = 2

        curve.attr('currLen') >> sqnorm.attr('i1x')
        curve.attr('origLen') >> sqnorm.attr('i2x')
        sqnorm.attr('ox') >> sqpow.attr('i1x')
        sqpow.attr('i2x').v = 2
        sqdiv.attr('i1x').v = 1
        sqpow.attr('ox') >> sqdiv.attr('i2x')

        sqblender = self.init_node(
            prc.create('blendTwoAttr'), 'AtSq', None, side, 'Blender')
        sqblender.add(ln='default', dv=1)
        sqblender.attr('default') >> sqblender.attr('i').last()
        sqdiv.attr('ox') >> sqblender.attr('i').last()
        curve.attr('autoSquash') >> sqblender.attr('ab')

        sqadd = self.init_node(
            prc.create('addDoubleLinear'), 'Sq', None, side, 'Add')
        sqblender.attr('o') >> sqadd.attr('i1')
        self.init_node(
            pru.attr_amper(
                curve.attr('squash'), sqadd.attr('i2'), 0.1, 'sqAmp'),
            'Sq', None, side, 'Amp')
        sqadd.attr('o') >> curve.attr('currSquash')

        def _create_rig(index, crv_param):
            """Creates a rig at the given crv_param of the curve.

            Args:
                index (int):
                crv_param (float):

            Returns:
                prc.Node: pointOnCurveInfo
                prc.Dag: Controller
                prc.Dag: Control's offset
                prc.Dag: Control's zero
                prc.Dag: Joint
            """
            # Controller
            ctrl, ctrl_ori, ctrl_ofst, ctrl_zr = self.add_controller(
                'Det', index, side, control_shape, 2, ('v',))
            ctrl.attr('rotateOrder').v = prc.Ro.yzx
            ctrl_zr.set_parent(self.meta)

            # Point On Curve
            poc = self.add_poci(ctrl_zr, curve, crv_param)

            ctrlshape = ctrl.get_shape()
            ctrlshape.add(ln='parameter', dv=crv_param, k=True)
            ctrlshape.attr('parameter') >> poc.poci.attr('parameter')

            crvattrs = (
                'autoTwist', 'twist', 'rootTwist', 'endTwist', 'rootAutoTwist',
                'endAutoTwist')
            for crvattr in crvattrs:
                curve.attr(crvattr) >> poc.poci.attr(crvattr)

            for aimattr in ('aimVector', 'upVector', 'worldUpVector'):
                curve.attr(aimattr) >> poc.aimcon.attr(aimattr)

            # Squesh
            ctrl.add(ln='squash', k=True)
            curr_sqadd = self.init_node(
                prc.create('addDoubleLinear'), 'Sq', index, side, 'Add')
            self.init_node(
                pru.attr_amper(
                    ctrl.attr('squash'), curr_sqadd.attr('i1'), 0.1, 'sqAmp'),
                'Sq', index, side, 'Amp')
            curve.attr('currSquash') >> curr_sqadd.attr('i2')
            curr_sqadd.attr('o') >> ctrl.attr('sx')
            curr_sqadd.attr('o') >> ctrl.attr('sz')
            ctrl.lhattrs('s')

            # Joint
            jnt = self.init_dag(
                pru.create_joint_at(ctrl), 'Skin', index, side, 'Jnt')
            jnt.attr('ssc').v = False
            jnt.attr('rotateOrder').v = ctrl.attr('rotateOrder').v
            if skin_parent:
                jnt.set_parent(skin_parent)
            prc.parent_constraint(ctrl, jnt)
            ctrl.attr('s') >> jnt.attr('s')

            return poc.poci, ctrl, ctrl_ofst, ctrl_ori, ctrl_zr, jnt

        # If joint amount is 1 the POC is created at the middle(0.5).
        # If it's 2 the POCs are created at 0.33 and 0.67.
        if joint_amount == 1:
            tip_offset = 0.5
        elif joint_amount == 2:
            tip_offset = 0.33

        self.pocis = []
        self.ctrls = []
        self.ctrl_ofsts = []
        self.ctrl_ories = []
        self.ctrl_zrs = []
        self.jnts = []
        for ix in range(joint_amount):
            idx = ix + 1
            if ix == 0:
                param = tip_offset
            elif ix == joint_amount - 1:
                param = 1.00 - tip_offset
            else:
                param = float(ix) / (float(joint_amount) - 1.00)

            (curr_poci,
             curr_ctrl,
             curr_ctrl_ofst,
             curr_ctrl_ori,
             curr_ctrl_zr,
             curr_jnt) = _create_rig(idx, param)

            self.pocis.append(curr_poci)
            self.ctrls.append(curr_ctrl)
            self.ctrl_ofsts.append(curr_ctrl_ofst)
            self.ctrl_ories.append(curr_ctrl_ori)
            self.ctrl_zrs.append(curr_ctrl_zr)
            self.jnts.append(curr_jnt)


class StringIk(rig_base.BaseRig):

    def __init__(
            self,
            len,
            root,
            end,
            joint_amount,
            side,
            mod,
            desc,
            ctrl_grp_parent,
            skin_parent,
            still_parent
    ):
        """
        Args:
            len (float):
            root (prc.Dag):
            end (prc.Dag):
            joint_amount (int):
            side (prc.Side/None):
            mod (str):
            desc (str):
            ctrl_grp_parent (prc.Dag/None):
            skin_parent (prc.Dag/None):
            still_parent (prc.Dag/None):
        """
        # Main Groups
        super(StringIk, self).__init__(None, mod, desc, False)

        self.meta.name = prnt.compose(
            '{m}{d}'.format(m=self.mod_name, d=self.desc), None, side, 'Grp')
        if ctrl_grp_parent:
            self.meta.set_parent(ctrl_grp_parent)
        
        self.rigup_vmp = self.init_node(
            prc.create('vectorProduct'), 'Ctrl', None, side, 'VecMatProd')
        self.rigup_vmp.attr('operation').v = 3
        self.rigup_vmp.attr('i1').v = (0, 0, 1)
        self.meta.attr('worldMatrix[0]') >> self.rigup_vmp.attr('matrix')

        self.still_grp = self.init_dag(prc.Null(), 'Still', None, None, 'Grp')
        if still_parent:
            self.still_grp.set_parent(still_parent)

        # Main Controllers
        def _create_tip_control(part):
            """Creates Root/End controller with its groups.

            Args:
                part (str):

            Returns:
                prc.Dag: Controller
                prc.Dag: Zero Group
                prc.Dag: Aim Group
            """
            ctrl = self.init_dag(
                prc.Controller(prc.Cp.plus), part, None, side, 'Ctrl')
            ctrl.attr('rotateOrder').v = prc.Ro.yzx
            ctrl.lhattrs('r', 's', 'v')
            ctrl.set_default_color(0)
            ctrlzr = self.init_dag(
                prc.group(ctrl), '{}Ctrl'.format(part), None, side, 'Zr')
            ctrlaim = self.init_dag(
                prc.Null(), '{}Ctrl'.format(part), None, side, 'Aim')
            rigjnt = self.init_dag(
                prc.Joint(), part, None, side, 'RigJnt')
            rigjnt.attr('drawStyle').v = prc.DrawStyle.none

            rigjnt.set_parent(ctrlaim)
            ctrlaim.set_parent(ctrl)
            ctrlzr.set_parent(self.meta)

            return ctrl, ctrlzr, ctrlaim, rigjnt

        # Root/End/Mid
        (self.root_ctrl,
         self.root_ctrl_zr,
         self.root_ctrl_aim,
         self.root_rigjnt) = _create_tip_control('Root')
        (self.end_ctrl,
         self.end_ctrl_zr,
         self.end_ctrl_aim,
         self.end_rigjnt) = _create_tip_control('End')
        self.end_ctrl_zr.attr('ty').v = len

        self.mid_ctrl = self.init_dag(
            prc.Controller(prc.Cp.circle), 'Mid', None, side, 'Ctrl')
        self.mid_ctrl.attr('rotateOrder').v = prc.Ro.yzx
        self.mid_ctrl.lhattrs('r', 's', 'v')
        self.mid_ctrl.set_default_color(0)
        midctrl_shape = self.mid_ctrl.get_shape()
        midctrl_shape.add(ln='detailVis', k=True, min=0, max=1)

        self.root_ctrl.scale_shape(len/10.00)
        self.end_ctrl.scale_shape(len/10.00)
        self.mid_ctrl.scale_shape(len/5.00)

        self.mid_rigjnt = self.init_dag(
            prc.Joint(), 'Mid', None, side, 'RigJnt')
        self.mid_rigjnt.attr('drawStyle').v = prc.DrawStyle.none
        self.mid_rigjnt.set_parent(self.mid_ctrl)
        self.mid_ctrl_aim = self.init_dag(
            prc.group(self.mid_ctrl), 'MidCtrl', None, side, 'Aim')
        self.mid_ctrl_zr = self.init_dag(
            prc.group(self.mid_ctrl_aim), 'MidCtrl', None, side, 'Zr')
        self.mid_ctrl_zr.set_parent(self.meta)
        prc.point_constraint(self.root_ctrl, self.end_ctrl, self.mid_ctrl_zr)

        # Attrs
        self.mid_ctrl.add(ln='twist', k=True)
        self.mid_ctrl.add(ln='autoTwist', min=0, max=1, k=True, dv=1)
        self.mid_ctrl.add(ln='rootTwist', k=True)
        self.mid_ctrl.add(ln='endTwist', k=True)
        self.mid_ctrl.add(ln='autoSquash', min=0, max=1, k=True)
        self.mid_ctrl.add(ln='squash', k=True)

        # Aims
        prc.aim_constraint(
            self.end_ctrl, self.root_ctrl_aim, aim=(0, 1, 0), u=(1, 0, 0),
            wut='objectrotation', wuo=self.meta, wu=(1, 0, 0))
        prc.aim_constraint(
            self.root_ctrl, self.end_ctrl_aim, aim=(0, -1, 0), u=(1, 0, 0),
            wut='objectrotation', wuo=self.meta, wu=(1, 0, 0))
        prc.aim_constraint(
            self.end_ctrl, self.mid_ctrl_aim, aim=(0, 1, 0), u=(1, 0, 0),
            wut='objectrotation', wuo=self.meta, wu=(1, 0, 0))
        
        # Local Xforms
        (self.root_xform_multmat,
         self.root_xform_decomp) = pru.accumulate_xform_matrices(
            (self.root_ctrl, self.root_ctrl_zr, self.root_ctrl_aim), '')
        (self.end_xform_multmat,
         self.end_xform_decomp) = pru.accumulate_xform_matrices(
            (self.end_ctrl, self.end_ctrl_zr, self.end_ctrl_aim), '')
        (self.mid_xform_multmat,
         self.mid_xform_decomp) = pru.accumulate_xform_matrices(
            (self.mid_ctrl, self.mid_ctrl_aim, self.mid_ctrl_zr), '')

        self.init_node(
            self.root_xform_multmat, 'RootTrs', None, side, 'MultMat')
        self.init_node(
            self.root_xform_decomp, 'RootTrs', None, side, 'Decomp')
        self.init_node(
            self.end_xform_multmat, 'EndTrs', None, side, 'MultMat')
        self.init_node(
            self.end_xform_decomp, 'EndTrs', None, side, 'Decomp')
        self.init_node(
            self.mid_xform_multmat, 'MidTrs', None, side, 'MultMat')
        self.init_node(
            self.mid_xform_decomp, 'MidTrs', None, side, 'Decomp')

        # Length
        midctrl_shape.add(ln='origLen', dv=len, k=True)
        midctrl_shape.attr('origLen').lock = True
        midctrl_shape.add(ln='vctrLen', k=True)
        midctrl_shape.add(ln='len', k=True)

        self.vctr_dist = self.init_node(
            prc.create('distanceBetween'), 'Vctr', None, side, 'Dist')
        self.root_dist = self.init_node(
            prc.create('distanceBetween'), 'Root', None, side, 'Dist')
        self.end_dist = self.init_node(
            prc.create('distanceBetween'), 'End', None, side, 'Dist')
        self.string_add = self.init_node(
            prc.create('addDoubleLinear'), 'Len', None, side, 'Add')
        
        self.root_ctrl.attr('localTranslate') >> self.vctr_dist.attr('p1')
        self.end_ctrl.attr('localTranslate') >> self.vctr_dist.attr('p2')
        self.vctr_dist.attr('distance') >> midctrl_shape.attr('vctrLen')

        self.root_ctrl.attr('localTranslate') >> self.root_dist.attr('p1')
        self.mid_ctrl.attr('localTranslate') >> self.root_dist.attr('p2')
        self.end_ctrl.attr('localTranslate') >> self.end_dist.attr('p1')
        self.mid_ctrl.attr('localTranslate') >> self.end_dist.attr('p2')
        self.root_dist.attr('distance') >> self.string_add.attr('i1')
        self.end_dist.attr('distance') >> self.string_add.attr('i2')
        self.string_add.attr('o') >> midctrl_shape.attr('len')

        # Squash Rig
        midctrl_shape.add(ln='squash', k=True)
        sqdiv = self.init_node(
            prc.create('multiplyDivide'), 'Sq', None, side, 'Div')
        sqpow = self.init_node(
            prc.create('multiplyDivide'), 'Sq', None, side, 'Pow')
        sqnorm = self.init_node(
            prc.create('multiplyDivide'), 'SqNorm', None, side, 'Div')

        sqdiv.attr('operation').v = 2
        sqpow.attr('operation').v = 3
        sqnorm.attr('operation').v = 2

        midctrl_shape.attr('len') >> sqnorm.attr('i1x')
        midctrl_shape.attr('origLen') >> sqnorm.attr('i2x')
        sqnorm.attr('ox') >> sqpow.attr('i1x')
        sqpow.attr('i2x').v = 2
        sqdiv.attr('i1x').v = 1
        sqpow.attr('ox') >> sqdiv.attr('i2x')

        sqblender = self.init_node(
            prc.create('blendTwoAttr'), 'AtSq', None, side, 'Blender')
        sqblender.add(ln='default', dv=1)
        sqblender.attr('default') >> sqblender.attr('i').last()
        sqdiv.attr('ox') >> sqblender.attr('i').last()
        self.mid_ctrl.attr('autoSquash') >> sqblender.attr('ab')

        sqadd = self.init_node(
            prc.create('addDoubleLinear'), 'Sq', None, side, 'Add')
        sqblender.attr('o') >> sqadd.attr('i1')
        self.init_node(
            pru.attr_amper(
                self.mid_ctrl.attr('squash'), sqadd.attr('i2'), 0.1, 'sqAmp'),
            'Sq', None, side, 'Amp')
        sqadd.attr('o') >> midctrl_shape.attr('squash')

        # Curve
        tmpcurve = prc.Dag(mc.curve(d=1, p=[(0, 0, 0), (0, len, 0)]))
        rebuilt_curve = mc.rebuildCurve(
            tmpcurve, ch=False, rpo=1, rt=0, end=1, kr=0, kcp=0,
            kep=1, kt=0, s=2, d=3, tol=0.01)[0]
        self.spline_curve = self.init_dag(
            prc.Dag(rebuilt_curve), 'Spline', None, side, 'Crv')
        self.spline_curve.set_parent(self.still_grp)

        # Bend Cluster
        clus_name, clushandle_name = mc.cluster(self.spline_curve.name)
        self.bend_clus = self.init_node(
            prc.Node(clus_name), 'Bend', None, side, 'Clus')
        self.bend_clushandle = self.init_dag(
            prc.Dag(clushandle_name), 'Bend', None, side, 'ClusterHandle')
        self.bend_clushandle_zr = self.init_dag(
            prc.group(self.bend_clushandle), 'BendClusHandle', None, side,
            'Zr')
        self.bend_clushandle_zr.set_parent(self.still_grp)

        self.mid_ctrl.attr('tx') >> self.bend_clushandle.attr('tx')
        self.mid_ctrl.attr('tz') >> self.bend_clushandle.attr('tz')

        self.bendclus_scale_div = self.init_node(
            prc.create('multiplyDivide'), 'BendClusSca', None, side, 'Div')
        self.bendclus_scale_div.attr('operation').v = prc.Operator.divide
        midctrl_shape.attr('origLen') >> self.bendclus_scale_div.attr('i1x')
        midctrl_shape.attr('vctrLen') >> self.bendclus_scale_div.attr('i2x')

        self.bendclus_scale_mult = self.init_node(
            prc.create('multDoubleLinear'), 'BendClusSca', None, side, 'Mult')
        self.bendclus_scale_div.attr('ox') \
            >> self.bendclus_scale_mult.attr('i1')
        self.mid_ctrl.attr('ty') >> self.bendclus_scale_mult.attr('i2')
        self.bendclus_scale_mult.attr('o') >> self.bend_clushandle.attr('ty')
        self.bend_clushandle.hide = True

        for ix in range(5):
            wattr = 'w{}'.format(ix)
            clus_wattr = 'weightList[0].weights[{}]'.format(ix)
            self.spline_curve.add(ln=wattr, k=True, dv=0)
            self.spline_curve.attr(wattr) >> self.bend_clus.attr(clus_wattr)

        self.spline_curve.attr('w1').v = 0.4
        self.spline_curve.attr('w2').v = 1
        self.spline_curve.attr('w3').v = 0.4

        # Xform Cluster
        clus_name, clushandle_name = mc.cluster(self.spline_curve.name)
        self.xform_clus = self.init_node(
            prc.Node(clus_name), 'Xform', None, side, 'Clus')
        self.xform_clushandle = self.init_dag(
            prc.Dag(clushandle_name), 'Xform', None, side, 'ClusterHandle')
        self.xform_clushandle_zr = self.init_dag(
            prc.group(self.xform_clushandle), 'XformClusHandle', None, side,
            'Zr')
        self.xform_clushandle_zr.set_parent(self.mid_ctrl_aim)

        self.clus_scale_div = self.init_node(
            prc.create('multiplyDivide'), 'ClusSca', None, side, 'Div')
        self.clus_scale_div.attr('operation').v = prc.Operator.divide
        midctrl_shape.attr('vctrLen') >> self.clus_scale_div.attr('i1x')
        midctrl_shape.attr('origLen') >> self.clus_scale_div.attr('i2x')
        self.clus_scale_div.attr('ox') >> self.xform_clushandle.attr('sy')
        self.xform_clushandle.hide = True

        # Orient StrIk Rig
        self.meta.snap(root)
        pru.aim(self.meta, end, (0, 1, 0), (1, 0, 0), root.x_axis)
        if skin_parent:
            prc.parent_constraint(skin_parent, self.meta, mo=True)

        # String Rig
        self.string_curve = StringCurve(
            self.spline_curve, prc.Cp.nurbs_circle, joint_amount, 0.05,
            '{}Crv'.format(self.mod_name), self.desc, side, self.meta,
            skin_parent)
        midctrl_shape.attr('detailVis') >> self.string_curve.meta.attr('v')
        self.spline_curve.attr('upVector').v = (0, 0, 0)

        self.mid_ctrl.attr('squash') >> self.spline_curve.attr('squash')
        self.mid_ctrl.attr('autoSquash') >> self.spline_curve.attr('autoSquash')

        self.mid_ctrl.attr('twist') >> self.spline_curve.attr('twist')
        self.mid_ctrl.attr('autoTwist') >> self.spline_curve.attr('autoTwist')
        self.mid_ctrl.attr('rootTwist') >> self.spline_curve.attr('rootTwist')
        self.mid_ctrl.attr('endTwist') >> self.spline_curve.attr('endTwist')
        self.rigup_vmp.attr('o') >> self.spline_curve.attr('worldUpVector')

        if joint_amount == 1:
            self.mid_ctrl.lhattrs('rootTwist', 'endTwist')


def add_strik(
        root, end, joint_amount, mod, desc, side, ctrl_grp_parent, skin_parent,
        still_parent, root_twist, end_twist, root_target, end_target):
    """Instances a StrIk module then attach to the given root and end joints.

    Args:
        root (prc.Dag):
        end (prc.Dag):
        joint_amount (int):
        mod (str):
        desc (str):
        side (prc.Side):
        ctrl_grp_parent (prc.Dag):
        skin_parent (prc.Dag):
        still_parent (prc.Dag):
        root_twist (prc.Attribute/None):
        end_twist (prc.Attribute/None):
        root_target (prc.Dag):
        end_target (prc.Dag):
    """
    strik_vctr = om.MVector(*end.world_pos) - om.MVector(*root.world_pos)
    strik_len = strik_vctr.length()
    strik = StringIk(
        len=strik_len,
        root=root,
        end=end,
        joint_amount=joint_amount,
        side=side,
        mod=mod,
        desc=desc,
        ctrl_grp_parent=ctrl_grp_parent,
        skin_parent=skin_parent,
        still_parent=still_parent
    )

    if root_twist:
        root_twist >> strik.spline_curve.attr('rootAutoTwist')
    
    if end_twist:
        end_twist >> strik.spline_curve.attr('endAutoTwist')
    
    if root_target:
        prc.point_constraint(root_target, strik.root_ctrl)
    
    if end_target:
        prc.point_constraint(end_target, strik.end_ctrl)

    strik.end_ctrl.lhattrs('t')
    strik.root_ctrl.lhattrs('t')
    strik.end_ctrl_zr.hide = True
    strik.root_ctrl_zr.hide = True

    return strik
