# pkrig2
pkrig2 is a second version of pkrig, a Maya rigging framework, created by peckpeckpeckpeckpeck@gmail.com\
It helps riggers to create/maintain scripted rigging modules.
It also has modules to import/export rigging data such as skin weights, control shapes and blend shapes.

# Object Hierarchies
```mermaid
classDiagram
    class Node{
        +MSelectionList sel_list
        +MObject m_object
        +MFnDependencyNode fn_dependency_node
        +name() str
        +attr() Attr
        +add() Attr
    }
    class Dag{
        +MDagPath m_dag_path
        +MFnDagNode fn_dag_node
        +Color color
        +disable()
        +create_curve()
        +scale_shape()
        +set_default_color()
        +set_parent()
    }
    class Attr{
        +str name
        +Node node
        +str attr
        +bool exists
        +list children
        +value v
        +bool lock
        get_input()
        get_output()
        connections()
    }
    Node --|> Dag: Inherit
```

# Modules
[core](#core)

[naming_tools](#naming_tools)

# core
core is a collection of pkrig objects and basic functions those are used to manipulate the rigging code in pkrig2.

### mobject_from_str()
Gets MObject from the given name.

    Args:
    
        name (str): A name of Maya node.

    Returns:
    
        MObject

### dag_path_from_dag_str()
Gets MDagpath from the given name.

    Args:
        name (str): A name of DAG node.

    Returns:
        MDagPath

### create()
Uses createNode command to create a Maya node
then cast the created node to pkrig node.

    Returns:
        A pkrig object or None.
        Dag is returned if the given name is one of an XFORM_TYPES.
        Node is returned if the given name is not one of an XFORM_TYPES.

## Side
Side is used to specify a side of the rigging component.

    left = 'L'
    right = 'R'

## Color
Color is used to specify index color of a shape node.

    brown = 11
    green = 14
    dark_green = 7
    soft_yellow = 21
    yellow = 17
    soft_blue = 18
    blue = 6
    dark_blue = 15
    soft_gray = 3
    gray = 2
    black = 1
    white = 16
    soft_red = 31
    red = 13
    dark_red = 12
    none = None

## Operator
Represents operators in utility nodes.

    no_operation = 0
    sum = 1
    subtract = 2
    average = 3
    multiply = 1
    divide = 2
    power = 3
    equal = 0
    not_equal = 1
    greater_than = 2
    greater_or_equal = 3
    less_than = 4
    less_or_equal = 5

## CurveParam
CurveParam is a collection of curve parameter in pkrig2.
It is generally used to create a rig controller.

    cross
    plus
    circle
    cube
    capsule
    stick
    sphere
    square
    none

## Attr
Attr is an object to make set/get/connect on maya node attribute more object oriented.\
It is normally instanced when `Node.attr('attr_name')` is called.

#### Examples
```python
from pkrig2 import core as prc
null = prc.Null()
```
To set attribute value(s).
```python
null.attr('tx').v = 10
null.attr('t').v = (10, 0, 0)
```
To get attribute value(s).
```python
print(null.attr('tx').v)
print(null.attr('t').v)
```
To connect attributes.
```python
null.attr('tx') >> null.attr('ty')
```
Connects to the last index of multi(array) attribute.
```python
pma = prc.PlusMinusAverage()
null.attr('tx') >> pma.attr('input1D')
```

## Node
Node is a base class for all Maya nodes in pkrig2.

#### Examples
To cast an existing Maya node to a pkrig2 Node.
```python
import maya.cmds as mc
from pkrig2 import core as prc
mult = mc.createNode('multDoubleLinear')
mult_obj = prc.Node(mult)

# Sets value of input1 attribute to 10
mult_obj.attr('i1').v = 10

# Creates a multiplyDivideNode and gets/sets vector attribute.
mdv = prc.create('multiplyDivideNode')
print(mdv.attr('o').v)
mdv.attr('i1').v = (10.0, 20.0, 0.1)

# Connects to the last array attribute.
pma = prc.create('plusMinusAverage')
mdv.attr('o') >> pma.attr('input3D').last()
```

## Dag
Dag is a base class for all Maya DAG nodes in pkrig2.\
It has methods to manipulate attributes, hierarchy, shape of Maya DAG nodes.

#### Examples
To cast an existing Maya DAG node to a pkrig2 Dag.
```python
import maya.cmds as mc
from pkrig2 import core as prc
null = prc.Dag(mc.group(em=True))
```

To add a plus shape to current object.
```python
null.create_curve(CurveParam.plus)
```

To set color of current controller to blue.
```python
null.color = Color.blue
```

To add a keyable float attribute to current object.
```python
null.add(ln='default', at='double', k=True, dv=0, min=0, max=1)
```

Other tools.
```python
target = prc.Dag(mc.group(em=True))
target.attr('t').v = (10, 10, 5)
target.attr('r').v = (90, 45, 45)
# Snaps point/orient or both.
null.snap_point(target)
null.snap_orient(target)
null.snap(target)
# Sets parent.
null.set_parent(target)
# Adds an attribute to shape node.
shape = null.get_shape()
shape.add(ln='test', k=True, dv=10)
```

## naming_tools
A set of functions to manipulate a name of maya node
regarding the Naming Convention.

**Naming Convention**
```regexp
[Name]_[Index]_[Side]_[Type]
CompoDesc(_[0-9])(_L|R)_Type
```
**Example**
Root_Ctrl\
LegFk_L_Ctrl\
Index_1_L_Jnt\
Spine_1_Jnt

### tokenize()
Tokenize the given name regarding naming convention.

    Args:
        name (str): A node name.

    Returns:
        list: A list of component, index, side, type_

### compose()
Composes given tokens to a node name.

    Args:
        compo (str): A component.
        index (str/None/int): An index.
        side (str/None): A side.
        type_ (str): A node type.

    Returns:
        str: A composed name.

### add_desc()
Capitalizes the first charadter of desc then adds to the first part of
the old_name. Old type will be replaced if type_ is defined.

    Args:
        old_name (str): An old name.
        desc (str): A description.
        type_ (str): A new type.

    Returns:
        str: A new name with description added. The old type is replaced if
            type_ is defined.

### change_type()
Changes type of given old_name.

    Args:
        old_name (str): An old name.
        type_ (str): A new type.

    Returns:
        str: A new name with given new type.
