# Maya Modules
from imp import reload
import maya.cmds as mc

# pkrig Modules
from . import core as prc
from . import utils as pru
from . import naming_tools as prnt
from . import rig_base

reload(rig_base)


class FkRig(rig_base.BaseRig):

    def __init__(
            self,
            tmpjnts,
            tip_ctrl,
            add_squash,
            side,
            mod,
            desc,
            joint_descs,
            joint_type,
            ctrl_shape,
            tip_ctrl_shape,
            parents_attrnames,
            ctrl_grp_parent,
            fk_joint_parent
    ):
        """A generic FK rigging module.

        Args:
            tmpjnts (tuple): A tuple of template joint names(str).
            tip_ctrl (bool): Whether to have tip controller.
            add_squash (bool): Whether to add squash control.
            side (prc.Side/None):
            mod (str):
            desc (str):
            joint_descs (tuple/None): If None is given, index is used to
                identify each joint and controller.
                If tuple is given, values in tuple are used to identify
                each joint and controller and the length of joint_descs to be
                equal to length of tmpjnts - 1(ignore the tip joint).
            joint_type (str): The given type is used to all joints
                generated in this module.
            ctrl_shape (prc.Cp):
            parents_attrnames (tuple/None): A tuple of dictionaries
                that keys are parent nodes(prc.Dag) and
                values are attribute names.
                E.g., [{root_rig.root_jnt: 'root'}, {neck_rig.neck_jnt: 'neck'}]
            ctrl_grp_parent (prc.Dag):
            fk_joint_parent (prc.Dag):

        Attributes:
            meta (prc.Dag):
            jnts (list): A list of prc.Joint
            point_ctrls(list): A list of prc.Dag
            ctrls (list): A list of prc.Dag
            gmbls (list): A list of prc.Dag
            ctrl_ofsts (list): A list of prc.Dag
            ctrl_ories (list): A list of prc.Dag
            ctrl_zrs (list): A list of prc.Dag
            tip_jnt (prc.Joint):
            tip_ctrl (prc.Dag):
            tip_zr (prc.Dag):
        """
        # Casts args to pkrig objects.
        jnts = prc.to_dags(tmpjnts)

        # Main Groups
        super(FkRig, self).__init__(None, mod, desc, False)

        self.meta.name = prnt.compose(
            '{m}{d}'.format(m=self.mod_name, d=self.desc), None, side, 'Grp')

        if ctrl_grp_parent:
            self.meta.set_parent(ctrl_grp_parent)

        if fk_joint_parent and (fk_joint_parent != ctrl_grp_parent):
            prc.parent_constraint(fk_joint_parent, self.meta, mo=True)
            prc.scale_constraint(fk_joint_parent, self.meta, mo=True)

        self.jnts = []
        self.point_ctrls = []
        self.ctrls = []
        self.gmbls = []
        self.ctrl_ofsts = []
        self.ctrl_ories = []
        self.ctrl_zrs = []
        self.jnt_parconses = []
        for ix in range(len(jnts) - 1):
            jnt = jnts[ix]
            compo = ''
            index = ix + 1
            if joint_descs:
                compo = joint_descs[ix]
                index = None

            pntctrl = self.init_dag(
                prc.Controller(ctrl_shape), '{}Pnt'.format(compo), index,
                side, 'Ctrl')
            pntctrl.scale_shape(1.2)
            ctrl = self.init_dag(
                prc.Controller(ctrl_shape), compo, index, side, 'Ctrl')
            ctrl.scale_shape(2)
            gmbl = self.init_dag(
                pru.add_gimbal_helper(ctrl), compo, index, side, 'Gmbl')
            ofst = self.init_dag(
                prc.group(ctrl), '{}Ctrl'.format(compo), index, side,
                'Ofst')
            ori = self.init_dag(
                prc.group(ofst), '{}Ctrl'.format(compo), index, side, 'Ori')
            zr = self.init_dag(
                prc.group(ori), '{}Ctrl'.format(compo), index, side, 'Zr')

            pntctrl.lhattrs('s', 'v')
            pntctrl.set_default_color(1)
            pntctrl.set_parent(ctrl)
            ctrl.set_default_color(0)
            ctrl.attr('rotateOrder').v = prc.Ro.yzx
            ctrl.lhattrs('s', 'v')
            ofst.lhattrs('s', 'v')
            zr.snap(jnt)
            pru.add_shape_vis_controller(ctrl, pntctrl, 'detailVis')

            currjnt = self.init_dag(
                pru.create_joint_at(jnt), compo, index, side, joint_type)
            currjnt.attr('ssc').v = False

            self.jnts.append(currjnt)
            self.ctrls.append(ctrl)
            self.gmbls.append(gmbl)
            self.ctrl_ofsts.append(ofst)
            self.ctrl_ories.append(ori)
            self.ctrl_zrs.append(zr)

            jntparcons = prc.parent_constraint(pntctrl, currjnt)
            self.jnt_parconses.append(jntparcons)

            if jnt == jnts[0]:
                zr.set_parent(ctrl_grp_parent)
                currjnt.set_parent(fk_joint_parent)
            else:
                zr.set_parent(self.gmbls[ix - 1])
                currjnt.set_parent(self.jnts[ix - 1])

            # Adds squash.
            if add_squash:
                mult, add = pru.add_offset_controller(
                    ctrl, 'squash', [currjnt.attr('sx'), currjnt.attr('sz')],
                    0.1, 1)
                self.init_node(mult, '{}Sq'.format(compo), index, side, 'Mult')
                self.init_node(add, '{}Sq'.format(compo), index, side, 'Add')

        self.ctrl_zrs[0].set_parent(self.meta)

        # Adds tip controller.
        tip_compo = 'Tip'
        if joint_descs:
            tip_compo = '{}Tip'.format(joint_descs[-1])
        self.tip_jnt = self.init_dag(
            pru.create_joint_at(jnts[-1]), tip_compo, None, side, 'Jnt')
        self.tip_jnt.set_parent(self.jnts[-1])
        self.tip_jnt.attr('ssc').v = False

        if add_squash:
            tip_squash_ctrl = self.ctrls[-1]
            tip_squash_attr = 'tipSquash'

        if tip_ctrl:
            self.tip_ctrl = self.init_dag(
                prc.Controller(tip_ctrl_shape), tip_compo, None, side, 'Ctrl')
            self.tip_ctrl.lhattrs('s', 'v')
            self.tip_ctrl.set_default_color(2)
            self.tip_ctrl_zr = self.init_dag(
                prc.group(self.tip_ctrl), '{}Ctrl'.format(tip_compo), None,
                side, 'Zr')

            self.tip_ctrl_zr.snap(self.tip_jnt)
            self.tip_ctrl_zr.set_parent(self.gmbls[-1])
            prc.parent_constraint(self.tip_ctrl, self.tip_jnt)

            if add_squash:
                tip_squash_ctrl = self.tip_ctrl
                tip_squash_attr = 'squash'

        if add_squash:
            tipsq_mult, tipsq_add = pru.add_offset_controller(
                tip_squash_ctrl, tip_squash_attr,
                [self.tip_jnt.attr('sx'), self.tip_jnt.attr('sz')], 0.1, 1)
            self.init_node(
                tipsq_mult, '{}TipSq'.format(tip_compo), None, side, 'Mult')
            self.init_node(
                tipsq_add, '{}TipSq'.format(tip_compo), None, side, 'Add')

        # Adds stretch.
        for ix in range(len(self.jnts)):
            compo = ''
            index = ix + 1
            if joint_descs:
                compo = joint_descs[ix]
                index = None

            if ix == len(self.jnts) - 1:
                if tip_ctrl:
                    currtar = self.tip_ctrl_zr.attr('ty')
                else:
                    currtar = self.tip_jnt.attr('ty')
            else:
                currtar = self.ctrl_zrs[ix + 1].attr('ty')

            mult, add = pru.add_offset_controller(
                self.ctrls[ix], 'stretch', [currtar], 1, currtar.v)
            self.init_node(mult, '{}St'.format(compo), index, side, 'Mult')
            self.init_node(add, '{}St'.format(compo), index, side, 'Add')

        # Adds orient blend.
        if parents_attrnames:
            self.add_space_blender(
                parents_attrnames, prc.orient_constraint, self.ctrl_ories[0],
                self.ctrls[0], self.meta)
        
        # Skin Joints
        self.skin_jnts = self.jnts
