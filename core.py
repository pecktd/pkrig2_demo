# Python modules.
import re

# Maya modules.
import maya.cmds as mc
import maya.OpenMaya as om

# pkrig modules.
from . import naming_tools


def mobject_from_str(name):
    """Gets om.MObject from the given name.

    Args:
        name (str): A name of Maya node.

    Returns:
        om.MObject:
    """
    sellist = om.MSelectionList()
    sellist.add(str(name))

    mobj = om.MObject()
    sellist.getDependNode(0, mobj)

    return mobj


def dag_path_from_dag_str(name):
    """Gets om.MDagpath from the given name.

    Args:
        name (str): A name of DAG node.

    Returns:
        om.MDagPath:
    """
    mobj = mobject_from_str(str(name))

    if not mobj.hasFn(om.MFn.kDagNode):
        return None

    dagpath = om.MDagPath().getAPathTo(mobj)

    return dagpath


def dag_path_and_mobject_from_dag_str(name):
    """Gets om.MDagpath and om.MObject(component) from the given name.

    Args:
        name (str): A name of DAG node.

    Returns:
        om.MDagPath:
        om.MObject:
    """
    sellist = om.MSelectionList()
    sellist.add(str(name))

    dagpath = om.MDagPath()
    mobj = om.MObject()

    sellist.getDagPath(0, dagpath, mobj)

    return dagpath, mobj


def is_transform(name):
    """Checks is the given name a transform node.

    Args:
        name (str): Represents a node name.

    Returns:
        bool: True if the given name is a transform node else False.
    """
    mobj = mobject_from_str(name)

    if mobj.hasFn(om.MFn.kTransform):
        return True
    elif mobj.hasFn(om.MFn.kMesh):
        return True
    elif mobj.hasFn(om.MFn.kNurbsCurve):
        return True
    elif mobj.hasFn(om.MFn.kNurbsSurface):
        return True

    return False


def is_shape(name):
    """Checks is the given name a shape node.

    Args:
        name (str): Represents a node name.

    Returns:
        bool: True if the given name is a shape node else False.
    """
    mobj = mobject_from_str(name)

    if mobj.hasFn(om.MFn.kShape):
        return True

    return False


def is_attribute(name):
    """Checks is the given name an attribute.

    Args:
        name (str): Represents an attribute name.

    Returns:
        bool: True if the given name is an attribute else False.
    """
    sellist = om.MSelectionList()
    sellist.add(name)
    mplug = om.MPlug()

    try:
        sellist.getPlug(0, mplug)
        return True
    except RuntimeError:
        return False


def is_component(name):
    """Checks is the given name a component.

    Args:
        name (str): Represents a component name.

    Returns:
        bool: True if the given name is a component node else False.
    """
    sellist = om.MSelectionList()
    sellist.add(name)
    dagpath = om.MDagPath()
    mobj = om.MObject()
    sellist.getDagPath(0, dagpath, mobj)

    if mobj.hasFn(om.MFn.kComponent):
        return True

    return False


def to_pkrig_node(name):
    """Casts the existed Maya node or attribute to pkrig node.

    Args:
        name (str): A name of a Maya node or a component.

    Returns:
        Node/Attr/Dag/Component/None:
    """
    if not mc.objExists(name):
        raise ValueError('{} does not exist.'.format(name))

    if '.' in name:
        if is_attribute(name):
            return Attr(name)
        else:
            # Assumes that given name is a component.
            return Component(name)

    if is_transform(name):
        return Dag(name)
    else:
        return Node(name)


def to_pkrig_nodes(names):
    """Casts a list of object's names to pkrig nodes.

    Args:
        names (list/tuple): A list or tuple of node names.

    Returns:
        tuple: A tuple of pkrig objects.
    """
    return tuple([to_pkrig_node(name) for name in names])


def to_dags(names):
    """Casts a list of object's names to pkrig Dag objects.

    Args:
        names (list/tuple): A list or tuple of node names.

    Returns:
        tuple: A tuple of pkrig Dag objects.
    """
    return tuple([Dag(name) for name in names])


def create(*args, **kwargs):
    """Uses createNode command to create a Maya node
    then cast the created node to pkrig node.

    Returns:
        Dag/Node:
    """
    node = mc.createNode(*args, **kwargs)
    return to_pkrig_node(node)


def group(*args, **kwargs):
    """Translates mc.group command to pkrig command.

    Returns:
        Dag:
    """
    grp = Dag(mc.group(*args, **kwargs))
    grp.attr('rp').v = (0, 0, 0)
    grp.attr('sp').v = (0, 0, 0)

    return grp


def cluster(*args, **kwargs):
    """Translates mc.cluster command to pkrig command.

    Returns:
        Node: A cluster node.
        Dag: A cluster handle.
    """
    clust, clust_handle = mc.cluster(*args, **kwargs)
    return Node(clust), Dag(clust_handle)


def aim_constraint(*args, **kwargs):
    """Translates mc.aimConstraint command to pkrig command.

    Returns:
        Dag:
    """
    return Dag(mc.aimConstraint(*args, **kwargs)[0])


def orient_constraint(*args, **kwargs):
    """Translates mc.orientConstraint command to pkrig command.

    Returns:
        Dag:
    """
    cons = Dag(mc.orientConstraint(*args, **kwargs)[0])
    cons.attr('interpType').v = 2
    return cons


def point_constraint(*args, **kwargs):
    """Translates mc.pointConstraint command to pkrig command.

    Returns:
        Dag:
    """
    return Dag(mc.pointConstraint(*args, **kwargs)[0])


def parent_constraint(*args, **kwargs):
    """Translates mc.parentConstraint command to pkrig command.

    Returns:
        Dag:
    """
    cons = Dag(mc.parentConstraint(*args, **kwargs)[0])
    cons.attr('interpType').v = 2
    return cons


def scale_constraint(*args, **kwargs):
    """Translates mc.pointConstraint command to pkrig command.

    Returns:
        Dag:
    """
    return Dag(mc.scaleConstraint(*args, **kwargs)[0])


def ps_constraint(*args, **kwargs):
    """Applies parentConstraint and scalConstraint.

    Returns:
        Dag: A parentConstraint node.
        Dag: A scaleConstraint node.
    """
    parcons = Dag(mc.parentConstraint(*args, **kwargs)[0])
    scacons = Dag(mc.scaleConstraint(*args, **kwargs)[0])

    return parcons, scacons


def get_all_components(xform_str):
    """Gets all components of the given transform node.

    Args:
        xform_str (str): A name of a transform node.

    Returns:
        list: A list of all component names.
    """
    sel_list = om.MSelectionList()
    sel_list.add(xform_str)
    dag_path = om.MDagPath()
    sel_list.getDagPath(0, dag_path)

    dag_path.extendToShape()

    compoes = None
    # Mesh
    if dag_path.hasFn(om.MFn.kMesh):
        mesh_fn = om.MFnMesh(dag_path)
        num_vertices = mesh_fn.numVertices()
        compoes = '{dp}.vtx[0:{vtx}]'.format(
            dp=dag_path.partialPathName(),
            vtx=num_vertices - 1)
    # NURBs Curve
    elif dag_path.hasFn(om.MFn.kNurbsCurve):
        curve_fn = om.MFnNurbsCurve(dag_path)
        spans = curve_fn.numSpans()
        degree = curve_fn.degree()
        form = curve_fn.form()

        num_cvs = spans + degree

        if form == 3:
            num_cvs -= degree

        compoes = '{dp}.cv[0:{cv}]'.format(
            dp=dag_path.partialPathName(), cv=num_cvs - 1)
    # NURBs Surface
    elif dag_path.hasFn(om.MFn.kNurbsSurface):
        nurbs_fn = om.MFnNurbsSurface(dag_path)

        spans_u = nurbs_fn.numSpansInU()
        degree_u = nurbs_fn.degreeU()

        spans_v = nurbs_fn.numSpansInV()
        degree_v = nurbs_fn.degreeV()

        form_u = nurbs_fn.formInU()
        form_v = nurbs_fn.formInV()

        num_u = spans_u + degree_u

        if form_u == 3:
            num_u -= degree_u

        num_v = spans_v + degree_v

        if form_v == 3:
            num_v -= degree_v

        compoes = '{dp}.cv[0:{u}][0:{v}]'.format(
            dp=dag_path.partialPathName(),
            u=num_u - 1,
            v=num_v - 1)
    # Lattice
    elif dag_path.hasFn(om.MFn.kLattice):
        latname = dag_path.partialPathName()
        ns = mc.getAttr('{}.sDivisions'.format(latname))
        nt = mc.getAttr('{}.tDivisions'.format(latname))
        nu = mc.getAttr('{}.uDivisions'.format(latname))
        compoes = '{dp}.pt[0:{ns}][0:{nt}][0:{nu}]'.format(
            dp=latname, ns=ns, nt=nt, nu=nu)

    return mc.ls(compoes, fl=True)


def get_compo_index(compo_str):
    """Gets an index of the given component.
    Ex.
    pCylinder1.vtx[16] -> 16
    nurbsPlane1.cv[1][1] -> 5

    Args:
        compo_str(str):

    Returns:
        int:
    """
    allcompoes = get_all_components(compo_str.split('.')[0])
    for ix, compo in enumerate(allcompoes):
        if compo.split('.')[-1] == compo_str.split('.')[-1]:
            return ix


class Operator:
    """Operators on utility nodes.
    """
    # plusMinusAverage
    no_operation = 0
    sum = 1
    subtract = 2
    average = 3

    # multiplyDivide
    multiply = 1
    divide = 2
    power = 3

    # condition
    equal = 0
    not_equal = 1
    greater_than = 2
    greater_or_equal = 3
    less_than = 4
    less_or_equal = 5

    # vectorProduct
    no = 0
    dot = 1
    cross = 2
    vector_mat = 3
    point_mat = 4


class Side:
    """Specifies side of the rig component.
    """
    left = 'L'
    right = 'R'


class Ro:
    """Rotate orders.
    """
    xyz = 0
    yzx = 1
    zxy = 2
    xzy = 3
    yxz = 4
    zyx = 5


class Color:
    """Indices of colors those are used in pkrig module.
    """
    brown = 11
    green = 14
    dark_green = 7
    soft_yellow = 21
    yellow = 17
    soft_blue = 18
    blue = 6
    dark_blue = 15
    soft_gray = 3
    gray = 2
    black = 1
    white = 16
    soft_red = 31
    red = 13
    dark_red = 12
    none = None


class DrawStyle:
    """Draw style attribute values on joint node.
    """
    bone = 0
    box = 1
    none = 2


class CurveParam:
    """Parameters of one degree curve shapes those are used
    in pkrig module.
    """
    cross = (
        (0, 0, -2.193), (0.852, 0, -1.267), (0.511, 0, -1.267),
        (0.511, 0, -0.633), (0.633, 0, -0.511), (1.267, 0, -0.511),
        (1.267, 0, -0.852), (2.193, 0, 0), (1.267, 0, 0.852), (1.267, 0, 0.511),
        (0.633, 0, 0.511), (0.511, 0, 0.633), (0.511, 0, 1.267),
        (0.852, 0, 1.267), (0, 0, 2.193), (-0.852, 0, 1.267),
        (-0.511, 0, 1.267), (-0.511, 0, 0.633), (-0.633, 0, 0.511),
        (-1.267, 0, 0.511), (-1.267, 0, 0.852), (-2.193, 0, 0),
        (-1.267, 0, -0.852), (-1.267, 0, -0.511), (-0.633, 0, -0.511),
        (-0.511, 0, -0.633), (-0.511, 0, -1.267), (-0.852, 0, -1.267),
        (0, 0, -2.193)
    )
    plus = (
        (0, 1, 0), (0, -1, 0), (0, 0, 0), (-1, 0, 0), (1, 0, 0), (0, 0, 0),
        (0, 0, -1), (0, 0, 1)
    )
    circle = (
        (1.125, 0.0, 0.0), (1.004, 0.0, 0.0), (0.71, 0.0, -0.71),
        (0.0, 0.0, -1.004), (0.0, 0.0, -1.125), (0.0, 0.0, -1.004),
        (-0.71, 0.0, -0.71), (-1.004, 0.0, 0.0), (-1.125, 0.0, 0.0),
        (-1.004, 0.0, 0.0), (-0.71, 0.0, 0.71), (0.0, 0.0, 1.004),
        (0.0, 0.0, 1.125), (0.0, 0.0, 1.004), (0.71, 0.0, 0.71),
        (1.004, 0.0, 0.0)
    )
    cube = (
        (-1, -1, -1), (-1, -1, 1), (-1, 1, 1), (-1, -1, 1), (1, -1, 1),
        (1, 1, 1), (1, -1, 1), (1, -1, -1), (1, 1, -1), (1, -1, -1),
        (-1, -1, -1), (-1, 1, -1), (1, 1, -1), (1, 1, 1), (-1, 1, 1),
        (-1, 1, -1)
    )
    capsule = (
        (-2.011, 0, 0), (-1.977, 0.262, 0), (-1.876, 0.506, 0),
        (-1.715, 0.715, 0), (-1.506, 0.876, 0), (-1.262, 0.977, 0),
        (-1, 1.011, 0), (1, 1.011, 0), (1.262, 0.977, 0), (1.506, 0.876, 0),
        (1.715, 0.715, 0), (1.876, 0.506, 0), (1.977, 0.262, 0), (2.011, 0, 0),
        (1.977, -0.262, 0), (1.876, -0.506, 0), (1.715, -0.715, 0),
        (1.506, -0.876, 0), (1.262, -0.977, 0), (1, -1.011, 0), (-1, -1.011, 0),
        (-1.262, -0.977, 0), (-1.506, -0.876, 0), (-1.715, -0.715, 0),
        (-1.876, -0.506, 0), (-1.977, -0.262, 0), (-2.011, 0, 0)
    )
    stick = (
        (0.0, 0.0, 0.0), (0.0, 1.5, 0.0), (0.5, 2.0, 0.0),
        (0.0, 2.5, 0.0), (-0.5, 2.0, 0.0), (0.0, 1.5, 0.0)
    )
    sphere = (
        (-1.16, 0.0, 0.0), (-1.0, 0.0, 0.0), (-0.707, 0.707, 0.0),
        (0.0, 1.0, 0.0), (0.0, 1.16, 0.0), (0.0, 1.0, 0.0), (0.707, 0.707, 0.0),
        (1.0, 0.0, 0.0), (1.16, 0.0, 0.0), (1.0, 0.0, 0.0),
        (0.707, -0.707, 0.0), (0.0, -1.0, 0.0), (0.0, -1.16, 0.0),
        (0.0, -1.0, 0.0), (-0.707, -0.707, 0.0), (-1.0, 0.0, 0.0),
        (-0.7, 0.0, 0.7), (0.0, 0.0, 1.0), (0.0, 0.0, 1.16), (0.0, 0.0, 1.0),
        (0.7, 0.0, 0.7), (1.0, 0.0, 0.0), (0.7, 0.0, -0.7), (0.0, 0.0, -1.0),
        (0.0, 0.0, -1.16), (0.0, 0.0, -1.0), (0.0, 0.707, -0.707),
        (0.0, 1.0, 0.0), (0.0, 0.707, 0.707), (0.0, 0.0, 1.0),
        (0.0, -0.707, 0.707), (0.0, -1.0, 0.0), (0.0, -0.707, -0.707),
        (0.0, 0.0, -1.0), (-0.7, 0.0, -0.7), (-1.0, 0.0, 0.0)
    )
    fkik = (
        (0, 0, 0), (0, 0, 0), (0.5, 3, 0.5), (0, 0, 0), (0, 0, 0),
        (0.5, 3, -0.5), (0, 0, 0), (0, 0, 0), (-0.5, 3, -0.5), (0, 0, 0),
        (0, 0, 0), (-0.5, 3, 0.5), (-0.5, 3, -0.5), (0.5, 3, -0.5),
        (0.5, 3, 0.5), (-0.5, 3, 0.5),
    )
    square = ((-1, -1, 0), (-1, 1, 0), (1, 1, 0), (1, -1, 0), (-1, -1, 0))
    none = ((0, 0, 0), (0, 0, 0))
    nurbs_circle = 'nurbsCircle'


Cp = CurveParam


class Attr(object):
    """Base class for attribute objects in pkrig.

    Args:
        attr_str (str): A string in a format of node.attribute.
    """
    def __init__(self, attr_str):

        tokens = str(attr_str).split('.')

        self.name = attr_str
        self.node = to_pkrig_node(tokens[0])
        self.attr = '.'.join(tokens[1:])
        self.query_name = re.findall(r'([_a-zA-Z0-9]+)', tokens[-1])[0]
        self.children = None
        if self.exists:
            self.children = mc.attributeQuery(
                self.query_name, node=self.node, listChildren=True)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __rshift__(self, target):
        """Connects current attribute to the target.

        Args:
            target (str): A string in the format of node.attribute or
            Attr object.
        """
        mc.connectAttr(self, target, f=True)

    def __floordiv__(self, target):
        """disconnects current attribute to the target.

        Args:
            target (str): A string in the format of node.attribute or
            Attr object.
                """
        mc.disconnectAttr(self, target)

    def _setter(self, *args, **kwargs):
        """Sets current attribute and its children using mc.setAttr
        with the given args and kwargs.

        If a type of current attribute is typed,
        sets the attribute in a string format.

        If current attribute doesn't have children attrbutes,
        simply use setAttr command.

        If current attribute has children attributes.
        Checks first if no args assigned, sets the attribute properties
        else sets the attribute values.
        """
        if self.type == 'typed':
            # In case of current attribtue type is string.
            mc.setAttr(self, *args, type='string', **kwargs)
            return True

        if not self.children:
            mc.setAttr(self, *args, **kwargs)
            return True
        else:
            if not args:
                # Setting properties of attribute, e.g. sets lock and hide.
                for child in self.children:
                    current_attr = '{node_attr}.{child_attr}'.format(
                        node_attr=self, child_attr=child)
                    mc.setAttr(current_attr, **kwargs)
            else:
                if len(args[0]) == len(self.children):
                    # Checks the length of given args(values).
                    # It needs to match with the length of children attributes.
                    for sub_val, child in zip(args[0], self.children):
                        current_attr = '{node_attr}.{child_attr}'.format(
                            node_attr=self, child_attr=child)
                        mc.setAttr(current_attr, sub_val, **kwargs)
                    return True
                else:
                    print(
                        'Mismatch of lengthes between assigning values'
                        + ' and children attributes.')
                    return False

    def last(self):
        """Finds the last connectable of the 'multi' attribtue.

        Returns:
            Attr:
        """
        if not mc.attributeQuery(self.attr, node=self.node.name, multi=True):
            return self

        ix = 0
        while True:
            curr_attr = '{a}[{i}]'.format(a=self.attr, i=ix)
            if self.node.attr(curr_attr).get_input():
                ix += 1
                continue
            else:
                return self.node.attr(curr_attr)

    @property
    def value(self):
        val = mc.getAttr(self)

        if self.type == 'typed':
            # For array type attrbute.
            return val

        if isinstance(val, list) or isinstance(val, tuple):
            if isinstance(val[0], list) or isinstance(val[0], tuple):
                val = val[0]
        return val

    @value.setter
    def value(self, val):
        self._setter(val)

    v = value

    @property
    def lock(self):
        return mc.getAttr(self, lock=True)

    @lock.setter
    def lock(self, val):
        self._setter(lock=val)

    @property
    def hide(self):
        return not mc.getAttr(self, k=True)

    @hide.setter
    def hide(self, val):
        self._setter(k=not val)
        self._setter(cb=not val)

    @property
    def type(self):
        return mc.attributeQuery(
            self.query_name, node=self.node, attributeType=True)

    @property
    def exists(self):
        return mc.attributeQuery(
            self.query_name, node=self.node.name, ex=True)

    def connections(self, **kwargs):
        cons = mc.listConnections(self, **kwargs)
        if not cons:
            return None
        return to_pkrig_nodes(cons)

    def get_input(self):
        cons = mc.listConnections(self, s=True, d=False, p=True)
        if cons:
            return Attr(cons[0])
        else:
            return None

    def get_output(self):
        """Gets only the first plug of the out plugs.

        Returns:
            Attr/None:
        """
        cons = mc.listConnections(self, s=False, d=True, p=True)
        if cons:
            return Attr(cons[0])
        else:
            return None


class Node(object):
    """Base class for all nodes in pkrig.

    Args:
        name (str): A name of a Maya node.
    """
    def __init__(self, name):

        if not mc.objExists(name):
            raise ValueError('{} does not exist.'.format(name))

        self.sel_list = om.MSelectionList()
        self.sel_list.add(name)

        self.m_object = om.MObject()
        self.sel_list.getDependNode(0, self.m_object)

        self.fn_dependency_node = om.MFnDependencyNode(self.m_object)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    @property
    def name(self):
        return self.fn_dependency_node.name()

    @name.setter
    def name(self, new_name):
        mc.rename(self.name, new_name)

    def attr(self, attr_str):
        """Instances Attr object to the given attr_str.

        Args:
            attr_str (str): A name of a node's attribute.

        Returns:
            Attr:
        """
        return Attr('{node}.{attr}'.format(node=self, attr=attr_str))

    def add(self, *args, **kwargs):
        """Adds an attribute using addAttr command.
        """
        attrname = ''
        if 'ln' in kwargs:
            attrname = kwargs['ln']
        elif 'longName' in kwargs:
            attrname = kwargs['longName']

        mc.addAttr(self, *args, **kwargs)

        return self.attr(attrname)

    def lhattrs(self, *args):
        """Locks and hides given attributes.
        """
        for attr in args:
            self.attr(attr).lock = True
            self.attr(attr).hide = True

    def get_side(self):
        """Gets side of the current node.

        Returns:
            Side/None
        """
        if not naming_tools.is_legal(self.name):
            return None

        compo, index, side, type_ = naming_tools.tokenize(self.name)

        if side == Side.left:
            return Side.left
        elif side == Side.right:
            return Side.right
        else:
            return None


class Dag(Node):
    """Base class for all DAG objects in pkrig.
    Casts existed Maya node to pkrig Dag nobject.

    Args:
        name (str): A name of a Maya DAG node.
    """
    def __init__(self, name):

        super(Dag, self).__init__(name)

        self.m_dag_path = om.MDagPath()
        self.sel_list.getDagPath(0, self.m_dag_path)

        self.fn_dag_node = om.MFnDagNode(self.m_dag_path)

    @property
    def name(self):
        if len(mc.ls(self.m_dag_path.partialPathName())) > 1:
            return self.m_dag_path.fullPathName()
        else:
            return self.m_dag_path.partialPathName()

    @name.setter
    def name(self, new_name):
        mc.rename(self.name, new_name)

    @property
    def x_axis(self):
        mat = self.m_dag_path.inclusiveMatrix()
        getarrayitem = om.MScriptUtil.getDoubleArrayItem

        return tuple(getarrayitem(mat[0], ix) for ix in range(3))

    @property
    def y_axis(self):
        mat = self.m_dag_path.inclusiveMatrix()
        getarrayitem = om.MScriptUtil.getDoubleArrayItem

        return tuple(getarrayitem(mat[1], ix) for ix in range(3))

    @property
    def z_axis(self):
        mat = self.m_dag_path.inclusiveMatrix()
        getarrayitem = om.MScriptUtil.getDoubleArrayItem

        return tuple(getarrayitem(mat[2], ix) for ix in range(3))

    def create_curve(self, curve_param):
        """Creates a curve shape and parent it under
        current transform node.

        Args:
            curve_param (tuple): A curve parameter.

        Returns:
            str, a name of created shape.
        """
        shape = self.get_shape()
        if shape:
            print('{xform} already has a shape node.'.format(xform=self))
            return shape

        shape = mc.createNode(
            'nurbsCurve', n='{xform}Shape'.format(xform=self.name), p=self)
        mc.setAttr('{node}.v'.format(node=shape), k=False)

        if curve_param == CurveParam.nurbs_circle:
            mc.setAttr(
                '{s}.cc'.format(s=shape),
                3, 12, 2, False, 3,
                (-2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14),
                17,
                15,
                (0.5234, 0.0, -0.9065),
                (0.0, 0.0, -1.0467),
                (-0.5234, 0.0, -0.9065),
                (-0.9065, 0.0, -0.5234),
                (-1.0467, 0.0, -0.0),
                (-0.9065, 0.0, 0.5234),
                (-0.5234, 0.0, 0.9065),
                (0.0, 0.0, 1.0467),
                (0.5234, 0.0, 0.9065),
                (0.9065, 0.0, 0.5234),
                (1.0467, 0.0, 0.0),
                (0.9065, 0.0, -0.5234),
                (0.5234, 0.0, -0.9065),
                (0.0, 0.0, -1.0467),
                (-0.5234, 0.0, -0.9065),
                type='nurbsCurve'
            )
        else:
            values = [
                '{s}.cc'.format(s=shape),
                1, len(curve_param) - 1, 0, False, 3,
                [ix for ix in range(len(curve_param))],
                len(curve_param),
                len(curve_param)
            ]
            values += curve_param
            mc.setAttr(*values, type='nurbsCurve')

        return shape

    def get_parent(self):
        """Gets a parent of current Dag.

        Returns:
            Dag if parent is a DAG, None if parent is world.
        """
        parent = self.fn_dag_node.parent(0)
        if parent.apiType() == om.MFn.kWorld:
            return None
        parentname = om.MFnDagNode(parent).fullPathName()
        return Dag(parentname)

    def get_shape(self):
        """Gets the first shape of current Dag.

        Returns:
            A pkrig Dag of the first shape.
            None if current Dag doesn't have shape node.
        """
        shapes = mc.listRelatives(self, s=True, f=True)

        if not shapes:
            return None

        return Dag(shapes[0])

    def get_all_shapes(self):
        shapes = mc.listRelatives(self, s=True, f=True)
        if not shapes:
            return None
        return [Dag(shape) for shape in shapes]

    def get_child(self):
        """Gets the first child Dag.

        Returns:
            Dag/None:
        """
        children = mc.listRelatives(self.name, type='transform')
        if children:
            return Dag(children[0])
        else:
            None
    
    def get_all_children(self):
        """Gets all child Dags

        Returns:
            List:
        """
        children = mc.listRelatives(self.name, type='transform')
        if children:
            return [Dag(chd) for chd in children]

    def freeze(self, **kwargs):
        mc.makeIdentity(self, a=True, **kwargs)
    
    def rotate(self, values, **kwargs):
        mc.rotate(values[0], values[1], values[2], self.name, **kwargs)

    def lhtrs(self):
        """Locks and hides translate, rotate and scale attributes.
        """
        self.attr('t').lock = True
        self.attr('t').hide = True
        self.attr('r').lock = True
        self.attr('r').hide = True
        self.attr('s').lock = True
        self.attr('s').hide = True

    @property
    def hide(self):
        return mc.getAttr('{n}.v'.format(n=self))

    @hide.setter
    def hide(self, val):
        mc.setAttr('{n}.v'.format(n=self), not val)

    @property
    def color(self):
        """Gets overrideColor of current shape."""
        shape = self.get_shape()
        if not shape:
            return None

        return mc.getAttr('{node}.overrideColor'.format(node=shape))

    @color.setter
    def color(self, value):
        """Sets override color of current shape node.

        Args:
            value (int): An index color.
        """
        shape = self.get_shape()
        if shape:
            shape.attr('overrideEnabled').value = True
            shape.attr('overrideColor').value = value

    def set_default_color(self, tone):
        """Sets color of current DAG regarding to its side and the given tone.
        Valid tones are 0 = normal, 1 = soft, 2 = dark.

        Args:
            tone (int): A color tone, 0 = normal, 1 = soft, 2 = dark.
        """
        _, _, side, _ = naming_tools.tokenize(self.name)
        normcol = Color.yellow
        softcol = Color.soft_yellow
        darkcol = Color.brown

        if side == Side.left:
            normcol = Color.red
            softcol = Color.soft_red
            darkcol = Color.dark_red
        elif side == Side.right:
            normcol = Color.blue
            softcol = Color.soft_blue
            darkcol = Color.dark_blue

        if tone == 0:
            self.color = normcol
        elif tone == 1:
            self.color = softcol
        elif tone == 2:
            self.color = darkcol

    @property
    def rotate_order(self):
        return mc.getAttr('{node}.rotateOrder'.format(node=self))

    @rotate_order.setter
    def rotate_order(self, value):
        mc.setAttr('{node}.rotateOrder'.format(node=self), value)

    @property
    def world_pos(self):
        return mc.xform(self, q=True, rp=True, ws=True)

    @world_pos.setter
    def world_pos(self, pos=(0, 0, 0)):
        mc.xform(self, t=pos, ws=True)
    
    # Joint Properties
    @property
    def radi(self):
        if self.attr('radi').exists:
            return self.attr('radi').v
    
    @radi.setter
    def radi(self, value):
        if self.attr('radi').exists:
            self.attr('radi').v = value
    
    @property
    def ssc(self):
        if self.attr('ssc').exists:
            return self.attr('ssc').v
    
    @ssc.setter
    def ssc(self, value):
        if self.attr('ssc').exists:
            self.attr('ssc').v = value

    # Snapping methods.
    def snap_point(self, target):
        """Snaps position of current DAG object to target.

        Args:
            target (Dag):
        """
        mc.matchTransform(
            self.name, target.name, pos=True, rot=False, scl=False)

    def snap_orient(self, target):
        """Snaps orientation of current DAG object to target.

        Args:
            target (Dag):
        """
        mc.matchTransform(
            self.name, target.name, pos=False, rot=True, scl=False)

    def snap_scale(self, target):
        """Snaps scale of current DAG object to target.

        Args:
            target (Dag):
        """
        mc.matchTransform(
            self.name, target.name, pos=False, rot=False, scl=True)

    def snap(self, target):
        """Snaps transformation of current DAG object to target.

        Args:
            target (Dag):
        """
        mc.matchTransform(self.name, target.name)

    def set_parent(self, parent):
        """Parents current Dag to given parent node.

        Args:
            parent (Dag):
        """
        if self.m_object.hasFn(om.MFn.kShape):
            mc.parent(self, parent, s=True, r=True)
        else:
            if parent:
                try:
                    mc.parent(self, parent)
                except RuntimeError:
                    # If the object is already a child of the given parent.
                    pass
            else:
                if self.get_parent():
                    mc.parent(self, w=True)
    
    def set_pivot(self, pos):
        """Sets the rotate and scale pivots to the given pos.

        Args:
            pos (tuple): A tuple of three floats as a world position.
        """
        mc.move(
            pos[0], pos[1], pos[2], '{}.scalePivot'.format(self.name),
            '{}.rotatePivot'.format(self.name),absolute=True)

    # Shape tools.
    def scale_shape(self, value):
        """Scale current shape.

        Args:
            value (float): Scale value.
        """
        compoes = get_all_components(self.name)
        piv = mc.xform(self.name, q=True, rp=True, ws=True)
        mc.scale(value, value, value, compoes, pivot=piv, r=True)

    def rotate_shape(self, rotate_values):
        """Rotates currnet shape.

        Args:
            rotate_values (tuple): A tuple of three floats.

        Returns:
            None
        """
        compoes = get_all_components(self.name)
        mc.rotate(
            rotate_values[0], rotate_values[1], rotate_values[2],
            compoes, relative=True, objectSpace=True)

    def move_shape(self, values):
        """Moves shape under the object space.

        Args:
            values: Translate values.

        Returns:
            None
        """
        compoes = get_all_components(self.name)
        mc.move(
            values[0], values[1], values[2], compoes, relative=True,
            objectSpace=True, worldSpaceDistance=True)

    def disable(self):
        """Locks and hides all keyable attributes and hide its shape.
        """
        for attr in mc.listAttr(self.name, k=True):
            self.attr(attr).lock = True
            self.attr(attr).hide = True

        shape = self.get_shape()
        if shape:
            shape.attr('v').v = False


class Null(Dag):
    """A transform node.
    """
    def __init__(self):
        xform = mc.createNode('transform', n='null')
        super(Null, self).__init__(xform)


class Locator(Dag):
    """A locator node.
    """
    def __init__(self):
        xform = mc.spaceLocator()[0]
        super(Locator, self).__init__(xform)


class Joint(Dag):
    """A joint node.
    """
    def __init__(self):
        xform = mc.createNode('joint')
        super(Joint, self).__init__(xform)
    
    @property
    def ssc(self):
        return self.attr('ssc').v
    
    @ssc.setter
    def ssc(self, val):
        self.attr('ssc').v = val


class Controller(Dag):
    """Creates a transform with curve shape specified
    by curve_param.

    Args:
        curve_param (list/CurveParam): A curve parameter.
    """
    def __init__(self, curve_param):
        xform = mc.createNode('transform', n='control')
        super(Controller, self).__init__(xform)

        if curve_param:
            self.create_curve(curve_param)


class JointController(Dag):
    """Creates a joint, to be used as a controller,
    with curve shape specified by curve_param.

    Args:
        curve_param (list/CurveParam): A curve parameter.
    """
    def __init__(self, curve_param):
        xform = mc.createNode('joint', n='jointControl')
        super(JointController, self).__init__(xform)

        if curve_param:
            self.create_curve(curve_param)

        self.attr('drawStyle').v = DrawStyle.none

        for lockedattr in ('radi', 'v'):
            self.attr(lockedattr).lock = True
            self.attr(lockedattr).hide = True


class Component(object):
    """Base class for component objects in pkrig.

    Args:
        name (str): A name of a component .e.g., pSphere.vtx[0]
    """
    def __init__(self, name):
        self.node_name, self.compo_name = name.split('.')

        self.sel_list = om.MSelectionList()
        self.sel_list.add(name)

        self.m_object = om.MObject()
        self.m_dag_path = om.MDagPath()
        self.sel_list.getDagPath(0, self.m_dag_path, self.m_object)

        self.fn_comp_node = om.MFnSingleIndexedComponent(self.m_object)

        # Gets component type.
        if self.m_object.hasFn(om.MFn.kMeshVertComponent):
            self.comp_type_str = 'vtx'
        elif self.m_object.hasFn(om.MFn.kCurveCVComponent) \
                or self.m_object.hasFn(om.MFn.kSurfaceCVComponent):
            self.comp_type_str = 'cv'
        else:
            self.comp_type_str = 'map'

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    @property
    def name(self):
        return '{o}.{t}[{i}]'.format(
            o=self.m_dag_path.fullPathName(),
            t=self.comp_type_str,
            i=self.fn_comp_node.element(0)
        )

    @property
    def obj_name(self):
        return self.m_dag_path.fullPathName()

    @property
    def world_pos(self):
        """Gets the world space coordinates of component
        """
        return mc.xform(self.name, q=True, t=True, ws=True)

    def get_order(self):
        allcompoes = [
            compo.split('.')[-1]
            for compo in get_all_components(self.m_dag_path.fullPathName())]
        for ix, compo in enumerate(allcompoes):
            if self.name.split('.')[-1] == compo:
                return ix


class Pos(object):

    def __init__(
            self,
            surface,
            dag,
            pu,
            pv
    ):
        """Network of DG nodes to create a point on surface.
        The Point On Surface aims to the V direction.
        """
        self.dag = dag
        self.surface = surface
        self.shape = surface.get_shape()

        self.dag.attr('rotateOrder').v = Ro.yzx

        self.posi = create('pointOnSurfaceInfo')
        self.posi.attr('turnOnPercentage').v = True
        self.posi.attr('parameterU').v = pu
        self.posi.attr('parameterV').v = pv
        self.shape.attr('worldSpace[0]') >> self.posi.attr('inputSurface')

        self.up_vec = create('vectorProduct')
        self.up_vec.attr('op').v = 2
        self.posi.attr('normalizedTangentV') >> self.up_vec.attr('i1')
        self.posi.attr('normalizedNormal') >> self.up_vec.attr('i2')

        # Transform Matrices
        self.fbf = create('fourByFourMatrix')
        self.posi.attr('positionX') >> self.fbf.attr('in30')
        self.posi.attr('positionY') >> self.fbf.attr('in31')
        self.posi.attr('positionZ') >> self.fbf.attr('in32')
        self.up_vec.attr('outputX') >> self.fbf.attr('in00')
        self.up_vec.attr('outputY') >> self.fbf.attr('in01')
        self.up_vec.attr('outputZ') >> self.fbf.attr('in02')
        self.posi.attr('normalizedTangentVX') >> self.fbf.attr('in10')
        self.posi.attr('normalizedTangentVY') >> self.fbf.attr('in11')
        self.posi.attr('normalizedTangentVZ') >> self.fbf.attr('in12')
        self.posi.attr('normalizedNormalX') >> self.fbf.attr('in20')
        self.posi.attr('normalizedNormalY') >> self.fbf.attr('in21')
        self.posi.attr('normalizedNormalZ') >> self.fbf.attr('in22')

        self.multmat = create('multMatrix')
        self.decomp = create('decomposeMatrix')

        self.fbf.attr('output') >> self.multmat.attr('matrixIn').last()
        self.dag.attr('parentInverseMatrix[0]') \
            >> self.multmat.attr('matrixIn').last()
        self.multmat.attr('matrixSum') >> self.decomp.attr('inputMatrix')
        self.dag.attr('rotateOrder') >> self.decomp.attr('inputRotateOrder')

        self.decomp.attr('outputTranslate') >> dag.attr('t')

        # Twist
        self.posi.add(ln='twist', k=True)
        self.posi.add(ln='autoTwist', k=True, dv=1)
        self.posi.add(ln='rootTwist', k=True)
        self.posi.add(ln='endTwist', k=True)
        self.posi.add(ln='rootAutoTwist', k=True)
        self.posi.add(ln='endAutoTwist', k=True)

        # Twists by param.
        self.roottw_param_mult = create('multDoubleLinear')
        self.endtw_param_mult = create('multDoubleLinear')
        self.param_rev = create('reverse')

        self.posi.attr('parameterV') >> self.param_rev.attr('ix')
        self.param_rev.attr('ox') >> self.roottw_param_mult.attr('i2')
        self.posi.attr('parameterV') >> self.endtw_param_mult.attr('i2')

        # Auto Twist
        self.root_attw_mult = create('multDoubleLinear')
        self.end_attw_mult = create('multDoubleLinear')

        self.posi.attr('rootAutoTwist') >> self.root_attw_mult.attr('i1')
        self.posi.attr('autoTwist') >> self.root_attw_mult.attr('i2')
        self.posi.attr('endAutoTwist') >> self.end_attw_mult.attr('i1')
        self.posi.attr('autoTwist') >> self.end_attw_mult.attr('i2')

        # Twist Sum
        self.roottw_add = create('addDoubleLinear')
        self.endtw_add = create('addDoubleLinear')
        self.twsum_add = create('addDoubleLinear')
        self.tw_add = create('addDoubleLinear')

        self.posi.attr('rootTwist') >> self.roottw_add.attr('i1')
        self.root_attw_mult.attr('o') >> self.roottw_add.attr('i2')
        self.posi.attr('endTwist') >> self.endtw_add.attr('i1')
        self.end_attw_mult.attr('o') >> self.endtw_add.attr('i2')

        self.roottw_add.attr('o') >> self.roottw_param_mult.attr('i1')
        self.endtw_add.attr('o') >> self.endtw_param_mult.attr('i1')

        self.roottw_param_mult.attr('o') >> self.twsum_add.attr('i1')
        self.endtw_param_mult.attr('o') >> self.twsum_add.attr('i2')

        self.posi.attr('twist') >> self.tw_add.attr('i1')
        self.twsum_add.attr('o') >> self.tw_add.attr('i2')

        self.tw_e2q = create('eulerToQuat')
        self.tw_qprod = create('quatProd')
        self.tw_q2e = create('quatToEuler')

        self.dag.attr('rotateOrder') >> self.tw_e2q.attr('inputRotateOrder')
        self.dag.attr('rotateOrder') >> self.tw_q2e.attr('inputRotateOrder')

        self.tw_add.attr('o') >> self.tw_e2q.attr('inputRotateY')
        self.tw_e2q.attr('outputQuat') >> self.tw_qprod.attr('input1Quat')
        self.decomp.attr('outputQuat') >> self.tw_qprod.attr('input2Quat')
        self.tw_qprod.attr('outputQuat') >> self.tw_q2e.attr('inputQuat')
        self.tw_q2e.attr('outputRotate') >> dag.attr('r')


class Poc(object):

    def __init__(self, curve, dag, param):
        """Network of DG nodes to simulate a Point On Curve with rotation
        and twist to the given dag.
        The given dag aligns to the given curve with its Y axis.
        The dag's rotate order is set to YZX.

        Args:
            curve (Dag):
            dag (Dag):
            param (float):
        """
        dag.attr('rotateOrder').v = Ro.xzy

        # Point on Curve Info
        self.poci = create('pointOnCurveInfo')
        self.poci.attr('turnOnPercentage').v = True
        self.poci.attr('parameter').v = param

        curve.attr('local') >> self.poci.attr('ic')

        # Transform Matrices
        self.comp = create('composeMatrix')
        self.poci.attr('position') >> self.comp.attr('inputTranslate')

        self.multmat = create('multMatrix')
        self.decomp = create('decomposeMatrix')

        self.comp.attr('outputMatrix') >> self.multmat.attr('matrixIn').last()
        dag.attr('parentInverseMatrix[0]') \
            >> self.multmat.attr('matrixIn').last()
        self.multmat.attr('matrixSum') >> self.decomp.attr('inputMatrix')
        dag.attr('rotateOrder') >> self.comp.attr('inputRotateOrder')
        dag.attr('rotateOrder') >> self.decomp.attr('inputRotateOrder')

        self.decomp.attr('outputTranslate') >> dag.attr('t')
        self.decomp.attr('outputRotate') >> dag.attr('r')

        # Aim
        self.aimcon = Dag(create('aimConstraint').name)
        self.aimcon.name = '{}_aimConstraint'.format(dag.name)
        self.aimcon.snap(dag)
        self.aimcon.set_parent(dag)
        dag.attr('rotateOrder') >> self.aimcon.attr('constraintRotateOrder')
        self.poci.attr('normalizedTangent') >> self.aimcon.attr('tg[0].tt')
        self.aimcon.attr('aimVector').v = (0, 1, 0)
        self.aimcon.attr('upVector').v = (0, 1, 0)
        self.aimcon.attr('worldUpType').v = 3
        self.aimcon.attr('worldUpVector').v = (0, 1, 0)

        self.aim_e2q = create('eulerToQuat')
        self.aimcon.attr('cr') >> self.aim_e2q.attr('inputRotate')
        dag.attr('rotateOrder') >> self.aim_e2q.attr('inputRotateOrder')

        # Adds twist.
        self.poci.add(ln='twist', k=True)
        self.poci.add(ln='autoTwist', k=True, dv=1)
        self.poci.add(ln='rootTwist', k=True)
        self.poci.add(ln='endTwist', k=True)
        self.poci.add(ln='rootAutoTwist', k=True)
        self.poci.add(ln='endAutoTwist', k=True)

        # Twists by param.
        self.roottw_param_mult = create('multDoubleLinear')
        self.endtw_param_mult = create('multDoubleLinear')
        self.param_rev = create('reverse')

        self.poci.attr('parameter') >> self.param_rev.attr('ix')
        self.param_rev.attr('ox') >> self.roottw_param_mult.attr('i2')
        self.poci.attr('parameter') >> self.endtw_param_mult.attr('i2')

        # Auto Twist
        self.root_attw_mult = create('multDoubleLinear')
        self.end_attw_mult = create('multDoubleLinear')

        self.poci.attr('rootAutoTwist') >> self.root_attw_mult.attr('i1')
        self.poci.attr('autoTwist') >> self.root_attw_mult.attr('i2')
        self.poci.attr('endAutoTwist') >> self.end_attw_mult.attr('i1')
        self.poci.attr('autoTwist') >> self.end_attw_mult.attr('i2')

        # Sum Twists
        self.roottw_add = create('addDoubleLinear')
        self.endtw_add = create('addDoubleLinear')
        self.twsum_add = create('addDoubleLinear')
        self.tw_add = create('addDoubleLinear')

        self.poci.attr('rootTwist') >> self.roottw_add.attr('i1')
        self.root_attw_mult.attr('o') >> self.roottw_add.attr('i2')
        self.poci.attr('endTwist') >> self.endtw_add.attr('i1')
        self.end_attw_mult.attr('o') >> self.endtw_add.attr('i2')

        self.roottw_add.attr('o') >> self.roottw_param_mult.attr('i1')
        self.endtw_add.attr('o') >> self.endtw_param_mult.attr('i1')

        self.roottw_param_mult.attr('o') >> self.twsum_add.attr('i1')
        self.endtw_param_mult.attr('o') >> self.twsum_add.attr('i2')

        self.poci.attr('twist') >> self.tw_add.attr('i1')
        self.twsum_add.attr('o') >> self.tw_add.attr('i2')

        self.tw_e2q = create('eulerToQuat')
        self.tw_qprod = create('quatProd')

        dag.attr('rotateOrder') >> self.tw_e2q.attr('inputRotateOrder')

        self.tw_add.attr('o') >> self.tw_e2q.attr('inputRotateY')
        self.tw_e2q.attr('outputQuat') >> self.tw_qprod.attr('input1Quat')
        self.aim_e2q.attr('outputQuat') >> self.tw_qprod.attr('input2Quat')
        self.tw_qprod.attr('outputQuat') >> self.comp.attr('inputQuat')
        self.comp.attr('useEulerRotation').v = False
