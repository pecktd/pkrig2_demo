"""
Functions for generating spline weights.
The original code is from
https://gist.github.com/cyberiRex/d0f3cc101c330211a32ddd5d376b66a2
"""
import maya.cmds as mc


class CurveException(BaseException):
    """ Raised to indicate invalid curve parameters.
    """


def get_default_knots(count, degree=3):
    """Gets a default knot vector for a given number of cvs and degrees.

    Ex.
    count = 6
    degree = 3
    0 0 0 0 1 2 3 3 3 3

    Args:
        count (int): The number of cvs.
        degree (int): The curve degree.

    Returns:
        list: A list of knot values.
    """
    knots = [0]*degree + [i for i in range(count - degree + 1)]
    knots += [count - degree]*degree
    return [float(knot) for knot in knots]


def remap_param(order, knots, param):
    """Remaps the param to the range of knot values.

    Args:
        order (int):
        knots (list):
        param (float):

    Returns:
        float:
    """
    min_ = knots[order] - 1
    max_ = knots[len(knots) - 1 - order] + 1

    return (param * (max_ - min_)) + min_


def get_segment(degree, knots, order, param):
    """Determines which segment the param lies in.

    Args:
        degree (int):
        knots (list):
        order (int):
        param (flost):

    Returns:
        int:
    """
    segment = degree
    for index, knot in enumerate(knots[order:len(knots) - order]):
        if knot <= param:
            segment = index + order

    return segment


def de_boor_weights(cv_weights, degree, segment, knots, param):
    """Runs a modified version of de Boors algorithm to get CV's weights.

    Args:
        cv_weights (list): A list of dictionaries in a format of
            [{'cv0': 1.0}, {'cv1': 1.0}, ....]
        degree (int):
        segment (int):
        knots (list):
        param (float):

    Returns:
        list: A list of CVs and Weights, Weights is barycentric.
    """
    for r in range(1, degree + 1):
        for j in range(degree, r - 1, -1):
            right = j + 1 + segment - r
            left = j + segment - degree
            alpha = (param - knots[left])/(knots[right] - knots[left])

            weights = {}
            for cv, weight in cv_weights[j].iteritems():
                weights[cv] = weight*alpha

            for cv, weight in cv_weights[j - 1].iteritems():
                if cv in weights:
                    weights[cv] += weight*(1 - alpha)
                else:
                    weights[cv] = weight*(1 - alpha)

            cv_weights[j] = weights

    return cv_weights[degree]


def weight_matrices(mats_weights, name):
    """Creates a wtAddMatrix node connect the given matrices to it,
    assigns the given weight values respectively.

    Args:
        mats_weights (list): A list in a format of
            [{'loc1.matrix': 0.3}, {'loc2.matrix': 0.5}, {'loc3.matrix': 0.2}]
        name (str):

    Returns:
        str: A created wtAddMatrix node.
    """
    mat = mc.createNode('wtAddMatrix', name=name)
    for index, (matrix, weight) in enumerate(mats_weights):
        mc.connectAttr(
            matrix, '{}.wtMatrix[{}].matrixIn'.format(mat, index))
        mc.addAttr(mat, ln='w{}'.format(index), k=True, dv=weight)
        mc.setAttr('{}.w{}'.format(mat, index), l=True)
        mc.connectAttr(
            '{}.w{}'.format(mat, index),
            '{}.wtMatrix[{}].weightIn'.format(mat, index))

    return mat


def point_on_curve_weights(cvs, param, degree):
    """Creates a mapping of cvs to curve weight values on a spline curve.
    While all cvs are required, only the cvs with non-zero weights are returned.
    This function is based on de Boor's algorithm for evaluating splines
    and has been modified to consolidate weights.

    Args:
        cvs(list): A list of cvs, these are used for the return value.
        param(float): A parameter value.
        degree(int): The curve dimensions.

    Returns:
        list: A list of control point, weight pairs.
    """
    order = degree + 1
    if len(cvs) <= degree:
        msg = 'At least {} cvs is required.'.format(order)
        raise CurveException(msg)

    knots = get_default_knots(len(cvs), degree)

    # Converts cvs into hash-able indices
    _cvs = cvs
    cvs = [i for i in range(len(cvs))]

    # Remaps the param to the range of knot values.
    param = remap_param(order, knots, param)

    # Determines which segment the param lies in.
    segment = get_segment(degree, knots, order, param)

    # Filter out cvs we won't be using
    cvs = [cvs[j + segment - degree] for j in range(0, degree + 1)]

    # Run a modified version of de Boors algorithm.
    cv_weights = [{cv: 1.0} for cv in cvs]
    weights = de_boor_weights(cv_weights, degree, segment, knots, param)

    return [[_cvs[index], weight] for index, weight in weights.iteritems()]


def point_on_curve_weights2(cvs, param, degree):
    """Original point_on_curve_weights doesn't return the influences that have
    0 weight.
    point_on_curve_weights2 translates a list output to a dictionary output
    which given cvs are keys.
    And if replicated cvs are given, it also sumarizes the weight values.

    Args:
        cvs(list): A list of cvs, these are used for the return value.
        param(float): A parameter value.
        degree(int): The curve dimensions.

    Returns:
        dict: CVs are keys to weight values.
    """
    wgts = point_on_curve_weights(cvs, param, degree)
    output = {}
    for cv in cvs:
        if not cv in output.keys():
            output[cv] = 0.0
    
    for wgt in wgts:
        currkey = wgt[0]
        output[currkey] += wgt[1]
    
    return output


def tangent_on_curve_weights(cvs, param, degree, knots=None):
    """Creates a mapping of cvs to curve tangent weight values.
    While all cvs are required,
    only the cvs with non-zero weights will be returned.

    Args:
        cvs (list): A list of cvs, these are used for the return value.
        param (float): A parameter value.
        degree (int): The curve dimensions.
        knots (list): A list of knot values.

    Returns:
        list: A list of control point, weight pairs.
    """
    order = degree + 1  # Our functions often use order instead of degree
    if len(cvs) <= degree:
        msg = 'Curves of degree {} require at least {} cvs'.format(
            degree, degree + 1)
        raise CurveException(msg)

    # Defaults to even knot distribution.
    knots = knots or get_default_knots(len(cvs), degree)
    if len(knots) != len(cvs) + order:
        msg = 'Not enough knots provided.\n'
        msg += 'Curves with {} cvs must have '.format(len(cvs))
        msg += 'a knot vector of length {}.'.format(len(cvs) + order)
        msg += 'Received a knot vector of length {}: {}.\n'.format(
            len(knots), knots)
        msg += 'Total knot count must equal len(cvs) + degree + 1.'
        raise CurveException(msg)

    # Remap the t value to the range of knot values.
    param = remap_param(order, knots, param)

    # Determine which segment the t lies in
    segment = get_segment(degree, knots, order, param)

    # Convert cvs into hash-able indices
    _cvs = cvs
    cvs = [i for i in range(len(cvs))]

    # In order to find the tangent we need to find points
    # on a lower degree curve.
    degree = degree - 1
    q_weights = [{cv: 1.0} for cv in range(0, degree + 1)]

    # Get the DeBoor weights for this lower degree curve
    weights = de_boor_weights(q_weights, degree, segment, knots, param)

    # Take the lower order weights and match them to our actual cvs
    cv_weights = []
    for j in range(0, degree + 1):
        weight = weights[j]
        cv0 = j + segment - degree
        cv1 = j + segment - degree - 1
        divider = (knots[j + segment + 1] - knots[j + segment - degree])
        alpha = weight * (degree + 1)/divider
        cv_weights.append([cvs[cv0], alpha])
        cv_weights.append([cvs[cv1], -alpha])

    return [[_cvs[index], weight] for index, weight in cv_weights]


def _test_shade(name, color):
    shader = mc.shadingNode('lambert', asShader=True, n=name)
    mc.setAttr('{}.ambientColor'.format(shader), 0.1, 0.1, 0.1)
    mc.setAttr('{}.color'.format(shader), *color)
    sg = mc.sets(
        renderable=True, noSurfaceShader=True, empty=True,
        n='{}Sg'.format(shader))
    mc.connectAttr(
        shader + '.outColor', sg + ".surfaceShader", force=True)

    return shader, sg


def _assign_shade(node, shader, color):
    if not mc.objExists(shader):
        shader, sg = _test_shade(shader, color)
    else:
        sg = '{}Sg'.format(shader)
    mc.sets(node, fe=sg)


def test_cube(
        radius=1.0, color=(0.25, 0.5, 0.25), name='cube', position=(0, 0, 0)):
    """Creates a cube for testing purposes. """
    radius *= 2
    cube = mc.polyCube(name=name, h=radius, w=radius, d=radius, ch=False)[0]
    _assign_shade(cube, 'cube_lambert', color)
    mc.xform(cube, t=position)

    return cube


def point_on_curve(inmats, amount, degree, offset_pos, offset_tan, upvec):
    """Creates points on curve.
    The created transform nodes align Y axis to the curve's tangent and
    X axis to the given upvec attribute.

    Args:
        inmats (list): A list of matrix attributes e.g.,
            ['locator1.matrix',  'locator2.matrix',  'locator3.matrix', ...]
        amount (int):
        degree (int): A curve's degree.
        offset_pos (float): Offset values for the root/end positions.
        offset_tan (float): Offset values for the root/end tangents.
        degree (int): A curve's degree.
        upvec (str): A vector attrbute .e.g., vectorProduct.output.
            It's used as an Up Vector to all points.

    Returns:
        list: A list of str(s) names of transform nodes.
            Created transform nodes.
        list: A list of str(s) names of wtAddMatrix nodes.
            Position matrix nodes.
        list: A list of str(s) names of wtAddMatrix nodes.
            Tangent matrix nodes.
        list: A list of str(s) names of decomposeMatrix nodes.
            Position decompose matrix nodes.
        list: A list of str(s) names of decomposeMatrix nodes.
            Tangent decompose matrix nodes.
        list: A list of str(s) names of vectorProduct nodes.
            Normalized tangent vector nodes.
        list: A list of str(s) names of vectorProduct nodes.
            Temporary up vector nodes.
        list: A list of str(s) names of vectorProduct nodes.
            Point's normal nodes.
        list: A list of str(s) names of vectorProduct nodes.
            Up vector nodes.
        list: A list of str(s) names of composeMatrix nodes.
            Output matrix nodes.
        list: A list of str(s) names of decomposeMatrix nodes.
            Output transformation nodes.
    """
    points = []
    point_mats = []
    tan_mats = []
    point_decomps = []
    tan_decomps = []
    tan_norms = []
    tmpup_vecs = []
    pntnorm_vecs = []
    up_vecs = []
    comps = []
    decomps = []

    for ix in range(amount):
        param = float(ix)/(float(amount) - 1.00)
        point = test_cube(0.5, (0.25, 0.5, 0.25), 'p{}'.format(ix))
        points.append(point)

        # Position Matrix
        if ix == 0:
            param = offset_pos
        elif ix == amount - 1:
            param = 1 - offset_pos
        pnt_wts = point_on_curve_weights(inmats, param, degree)
        pnt_mat = weight_matrices(pnt_wts, 'pntmat_sum{}'.format(ix))
        point_mats.append(pnt_mat)

        # Create the tangent matrix
        if ix == 0:
            param = offset_tan
        elif ix == amount - 1:
            param = 1 - offset_tan
        tan_wts = tangent_on_curve_weights(inmats, param, degree)
        tan_mat = weight_matrices(tan_wts, 'tanmat{}'.format(ix))
        tan_mats.append(tan_mat)

        # Decompose the position matrix
        pnt_decomp = mc.createNode(
            'decomposeMatrix', name='pointDecompose{}'.format(ix))
        point_decomps.append(pnt_decomp)
        mc.connectAttr(
            '{}.matrixSum'.format(pnt_mat),
            '{}.inputMatrix'.format(pnt_decomp))

        # Decompose the tangent matrix
        tan_decomp = mc.createNode(
            'decomposeMatrix', name='tanDecompose{}'.format(ix))
        tan_decomps.append(tan_decomp)
        mc.connectAttr(
            '{}.matrixSum'.format(tan_mat),
            '{}.inputMatrix'.format(tan_decomp))

        # Tangent
        tan_norm = mc.createNode('vectorProduct', name='tanvec{}'.format(ix))
        tan_norms.append(tan_norm)
        mc.setAttr('{}.operation'.format(tan_norm), 0)
        mc.setAttr('{}.normalizeOutput'.format(tan_norm), True)
        mc.connectAttr(
            '{}.outputTranslate'.format(tan_decomp),
            '{}.input1'.format(tan_norm))

        def _do_cross(firstvec, secondvec, name):
            """Creats a vector node to cross the given firstvec to
            the given secondvec.

            Args:
                firstvec (str):
                secondvec (str):
                name (str): Node name.

            Returns:
                str: A vector product node.
            """
            vec = mc.createNode('vectorProduct', name=name)
            mc.setAttr('{}.operation'.format(vec), 2)
            mc.setAttr('{}.normalizeOutput'.format(vec), True)
            mc.connectAttr(firstvec, '{}.input1'.format(vec))
            mc.connectAttr(secondvec, '{}.input2'.format(vec))

            return vec

        # Temporary Up Vector - The first up vector calculated by
        # tangent cross with the given up vector attribute.
        tmpup_vec = _do_cross(
            upvec, '{}.o'.format(tan_norm), 'tmpupvec{}'.format(ix))
        tmpup_vecs.append(tmpup_vec)

        # Point Normal Vector
        pntnorm_vec = _do_cross(
            '{}.o'.format(tan_norm), '{}.o'.format(tmpup_vec),
            'pntnormvec{}'.format(ix))
        pntnorm_vecs.append(pntnorm_vec)

        # Up Vector
        up_vec = _do_cross(
            '{}.o'.format(tan_norm), '{}.o'.format(pntnorm_vec),
            'upvec{}'.format(ix))
        up_vecs.append(up_vec)

        # Composes transformation matrix.
        outmat_node = mc.createNode(
            'fourByFourMatrix', name='outputMatrix{}'.format(ix))
        comps.append(outmat_node)
        mc.connectAttr(
            '{}.ox'.format(up_vec), '{}.i00'.format(outmat_node))
        mc.connectAttr(
            '{}.oy'.format(up_vec), '{}.i01'.format(outmat_node))
        mc.connectAttr(
            '{}.oz'.format(up_vec), '{}.i02'.format(outmat_node))
        mc.connectAttr(
            '{}.ox'.format(tan_norm), '{}.i10'.format(outmat_node))
        mc.connectAttr(
            '{}.oy'.format(tan_norm), '{}.i11'.format(outmat_node))
        mc.connectAttr(
            '{}.oz'.format(tan_norm), '{}.i12'.format(outmat_node))
        mc.connectAttr(
            '{}.ox'.format(pntnorm_vec), '{}.i20'.format(outmat_node))
        mc.connectAttr(
            '{}.oy'.format(pntnorm_vec), '{}.i21'.format(outmat_node))
        mc.connectAttr(
            '{}.oz'.format(pntnorm_vec), '{}.i22'.format(outmat_node))
        mc.connectAttr(
            '{}.outputTranslateX'.format(pnt_decomp),
            '{}.i30'.format(outmat_node))
        mc.connectAttr(
            '{}.outputTranslateY'.format(pnt_decomp),
            '{}.i31'.format(outmat_node))
        mc.connectAttr(
            '{}.outputTranslateZ'.format(pnt_decomp),
            '{}.i32'.format(outmat_node))

        # Decomposes transformation matrix.
        decomp_node = mc.createNode(
            'decomposeMatrix', name='outputTransformations{}'.format(ix))
        decomps.append(decomp_node)
        mc.connectAttr(
            '{}.output'.format(outmat_node),
            '{}.inputMatrix'.format(decomp_node))

        mc.connectAttr(
            '{}.outputTranslate'.format(decomp_node),
            '{}.translate'.format(point))
        mc.connectAttr(
            '{}.outputRotate'.format(decomp_node),
            '{}.rotate'.format(point))

        return points, point_mats, tan_mats, point_decomps, tan_decomps, \
            tan_norms, tmpup_vecs, pntnorm_vecs, up_vecs, comps, decomps
