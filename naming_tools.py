"""
A set of functions to manipulate a name of maya node
regarding the Naming Convention.

Naming Convention:
[Name]_[Index]_[Side]_[Type]
CompoDesc(_[0-9])(_L|R)_Type

Example:
Root_Ctrl
LegFk_L_Ctrl
Index_1_L_Jnt
Spine_1_Jnt
"""


def is_legal(name):
    """Checks is the given name legal to pkrig.

    Args:
        name (str):

    Returns:
        True if the name is legal to the framework else False.
    """
    if '_' not in name:
        return False

    tokens = name.split('_')

    if len(tokens) > 4:
        return False

    if len(tokens) == 4:
        isnumber = tokens[1].isdigit()
        isside = tokens[2] in ('L', 'R')
        if not isside and isnumber:
            return False

    if len(tokens) == 3:
        isnumber = tokens[1].isdigit()
        isside = tokens[1] in ('L', 'R')
        if not (isnumber or isside):
            return False

    return True


def tokenize(name):
    """Uses '_' to tokenize the given name.

    Args:
        name (str): A node name.

    Returns:
        list: Component(str), index(None/str), side(None/str) and type_(str)
    """
    tokens = name.split('_')

    # Results
    compo = ''
    index = None
    side = None
    type_ = ''

    if len(tokens) == 1:
        # If there is only one token, uses 'None' as a type.
        compo = tokens[0]
        type_ = 'None'

    elif len(tokens) == 2:
        # If there are only two tokens,
        # only node component and node type were defined.
        compo = tokens[0]
        type_ = tokens[1]

    elif len(tokens) == 3:
        # If there are three tokens so there are two cases.
        # 1st case is compo with index.
        # 2nd case is compo with side.
        compo = tokens[0]
        type_ = tokens[2]

        if tokens[1].isdigit():
            index = tokens[1]
        else:
            side = tokens[1]

    elif len(tokens) == 4:
        # Current name has all elements.
        compo = tokens[0]
        index = tokens[1]
        side = tokens[2]
        type_ = tokens[3]

    return compo, index, side, type_


def capfirst(name):
    """Capitalizes the first character of the given string.

    Args:
        name (str):

    Returns:
        A string with capitalized the first character.
    """
    if not name:
        return None

    if len(name) > 1:
        return '{}{}'.format(name[0].capitalize(), name[1:])
    else:
        return name[0].capitalize()


def lowfirst(name):
    """Lowercase the first character of the given string.

    Args:
        name (str):

    Returns:
        A string with lowercased the first character.
    """
    if len(name) > 1:
        return '{}{}'.format(name[0].lower(), name[1:])
    else:
        return name[0].lower()


def compose(compo, index, side, type_):
    """Composes given tokens to a node name.

    Args:
        compo (str): A component.
        index (str/None/int): An index.
        side (str/None): A side.
        type_ (str): A node type.

    Returns:
        str: A composed name.
    """
    # Cleanup index.
    if index:
        index = str(index)
        index = index.strip('_')
        index = '_{}'.format(index)
    else:
        index = ''

    # Cleanup side.
    if side:
        side = side.strip('_')
        side = '_{}_'.format(side)
    else:
        side = '_'

    # Cleanup compo.
    compo = capfirst(compo)

    return '{compo}{index}{side}{type_}'.format(
        compo=compo, index=index, side=side, type_=type_)


def compose_attr_name(compo, index, side):
    """Composes the given arguments to the attribute name.
    lowerCamelCase is used to all attribute names in pkrig2.

    Ex. compose_attr_name('eyebrow', 1, prc.Side.left) -> leftEyebrow1.

    Args:
        compo (str):
        index (int/None):
        side (str/None):

    Returns:
        A composed name(str).
    """
    if not side:
        sidestr = ''
    elif str(side) == 'L':
        sidestr = 'left'
    elif str(side) == 'R':
        sidestr = 'right'
    else:
        sidestr = lowfirst(str(side))

    if sidestr:
        compo = capfirst(compo)
    else:
        compo = lowfirst(compo)

    indexstr = ''
    if index:
        indexstr = str(index)

    return '{s}{c}{i}'.format(s=sidestr, c=compo, i=indexstr)


def combine_attr_name(*args):
    """Combines a given list of words in an attribute name.
    Ex. combine_attr_name('This', 'is', 'a', 'name')
    -> 'thisIsAName
    """
    name = lowfirst(args[0])
    for compo in args[1:]:
        name = '{}{}'.format(name, capfirst(compo))
    
    return name


def add_desc(old_name, desc, type_):
    """Capitalizes the first charadter of desc then adds to the first part of
    the old_name. Old type will be replaced if type_ is defined.

    If the old component ends with Geo, script will keep Geo as the last
    part, e.g., add_desc('HairGeo_Grp', 'Guide', None) -> HairGuideGeo_Grp

    Args:
        old_name (str): An old name.
        desc (str): A description.
        type_ (str/None): A new type.

    Returns:
        str: A new name with description added. The old type is replaced if
            type_ is defined.
    """
    if not desc:
        desc = ''

    compo, index, side, currtype = tokenize(old_name)
    if type_:
        currtype = type_

    if len(desc) > 1:
        desc = '{}{}'.format(desc[0].capitalize(), desc[1:])
    else:
        desc = desc.capitalize()

    # If current component part ends with Geo, it is treated in a special way.
    # Geo in a component part is always the last word.
    if compo.endswith('Geo'):
        compo = '{c}{d}Geo'.format(c=compo[:-3], d=desc)
    else:
        compo = '{c}{d}'.format(c=compo, d=desc)

    return compose(compo, index, side, currtype)


def change_type(old_name, type_):
    """Changes type of the given old_name.

    Args:
        old_name (str): An old name.
        type_ (str): A new type.

    Returns:
        str: A new name with the given type.
    """
    compo, index, side, currtype = tokenize(old_name)
    return compose(compo, index, side, type_)
