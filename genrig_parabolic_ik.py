# System Modules
from imp import reload

# Maya Modules
import maya.cmds as mc
import maya.OpenMaya as om

# pkrig Modules
from . import core as prc
from . import utils as pru
from . import naming_tools as prnt
from . import rig_base


class ParabolicIk(rig_base.BaseRig):

    def __init__(
            self,
            root,
            end,
            joint_amount,
            side,
            mod,
            desc,
            ctrl_grp_parent,
            skin_parent
    ):
        """
        Args:
            root (prc.Dag):
            end (prc.Dag):
            joint_amount (int):
            side (prc.Side/None):
            mod (str):
            desc (str):
            ctrl_grp_parent (prc.Dag/None):
            skin_parent (prc.Dag/None):
            still_parent (prc.Dag/None):
        """
        # Main Groups
        super(ParabolicIk, self).__init__(None, mod, desc, False)

        self.meta.name = prnt.compose(
            '{m}{d}'.format(m=self.mod_name, d=self.desc), None, side, 'Grp')
        if ctrl_grp_parent:
            self.meta.snap(ctrl_grp_parent)
            self.meta.set_parent(ctrl_grp_parent)
        
        # Length
        rootvec = om.MVector(*root.world_pos)
        endvec = om.MVector(*end.world_pos)
        self.len = (endvec - rootvec).length()

        # Main Controllers
        def _create_tip_control(part):
            """Creates Root/End controller with its groups.

            Args:
                part (str):

            Returns:
                prc.Dag: Controller
                prc.Dag: Zero Group
                prc.Dag: Aim Group
            """
            ctrl = self.init_dag(
                prc.Controller(prc.Cp.plus), part, None, side, 'Ctrl')
            ctrl.attr('rotateOrder').v = prc.Ro.yzx
            ctrl.lhattrs('r', 's', 'v')
            ctrl.set_default_color(0)
            ctrlzr = self.init_dag(
                prc.group(ctrl), '{}Ctrl'.format(part), None, side, 'Zr')
            ctrlaim = self.init_dag(
                prc.Null(), '{}Ctrl'.format(part), None, side, 'Aim')

            ctrlaim.set_parent(ctrl)
            ctrlzr.set_parent(self.meta)

            return ctrl, ctrlzr, ctrlaim

        # Root/End/Mid
        (self.root_ctrl,
         self.root_ctrl_zr,
         self.root_ctrl_aim) = _create_tip_control('Root')
        (self.end_ctrl,
         self.end_ctrl_zr,
         self.end_ctrl_aim) = _create_tip_control('End')
        self.end_ctrl_zr.attr('ty').v = self.len

        self.mid_ctrl = self.init_dag(
            prc.Controller(prc.Cp.circle), 'Mid', None, side, 'Ctrl')
        self.mid_ctrl.attr('rotateOrder').v = prc.Ro.yzx
        self.mid_ctrl.lhattrs('r', 's', 'v')
        self.mid_ctrl.set_default_color(0)
        midctrl_shape = self.mid_ctrl.get_shape()
        midctrl_shape.add(ln='detailVis', k=True, min=0, max=1)

        self.mid_ctrl_ofst = self.init_dag(
            prc.group(self.mid_ctrl), 'MidCtrl', None, side, 'Ofst')
        self.mid_ctrl_aim = self.init_dag(
            prc.group(self.mid_ctrl_ofst), 'MidCtrl', None, side, 'Aim')
        self.mid_ctrl_zr = self.init_dag(
            prc.group(self.mid_ctrl_aim), 'MidCtrl', None, side, 'Zr')
        self.mid_ctrl_zr.set_parent(self.meta)
        prc.point_constraint(self.root_ctrl, self.end_ctrl, self.mid_ctrl_zr)

        self.root_ctrl.scale_shape(self.len/10.00)
        self.end_ctrl.scale_shape(self.len/10.00)
        self.mid_ctrl.scale_shape(self.len/5.00)

        # Aims
        prc.aim_constraint(
            self.end_ctrl, self.root_ctrl_aim, aim=(0, 1, 0), u=(1, 0, 0),
            wut='objectrotation', wuo=self.meta, wu=(1, 0, 0))
        prc.aim_constraint(
            self.root_ctrl, self.end_ctrl_aim, aim=(0, -1, 0), u=(1, 0, 0),
            wut='objectrotation', wuo=self.meta, wu=(1, 0, 0))
        prc.aim_constraint(
            self.end_ctrl, self.mid_ctrl_aim, aim=(0, 1, 0), u=(1, 0, 0),
            wut='objectrotation', wuo=self.meta, wu=(1, 0, 0))

        def _add_local_xform(name, hrc):
            """Gets the local xform from root/mid/end controllers.
            """
            name = '{}Local'.format(name)
            loc = self.init_dag(prc.Locator(), name, None, side, 'Loc')
            loc.set_parent(self.meta)
            
            multmat, decomp = pru.accumulate_xform_matrices(hrc, '')
            self.init_node(multmat, name, None, side, 'MultMat')
            self.init_node(decomp, name, None, side, 'Decomp')

            hrc[0].attr('localTranslate') >> loc.attr('t')
            hrc[0].attr('localRotate') >> loc.attr('r')
            hrc[0].attr('localScale') >> loc.attr('s')

            loc.hide = True

            return loc

        self.root_loc = _add_local_xform(
            'Root', (self.root_ctrl, self.root_ctrl_zr,))
        self.end_loc = _add_local_xform(
            'End', (self.end_ctrl, self.end_ctrl_zr))
        midhrc = (
            self.mid_ctrl, self.mid_ctrl_ofst, self.mid_ctrl_aim,
            self.mid_ctrl_zr)
        self.mid_loc = _add_local_xform('Mid', midhrc)

        # Detail Rigs
        self.det_grp = self.init_dag(prc.Null(), 'Det', None, side, 'Grp')
        self.det_grp.snap(self.meta)
        self.det_grp.set_parent(self.meta)

        def _add_det(param, idx):
            """Creates a detail rig using parametric blending of two vectors.
            Gets a point from a vector using (1 - t)A + tB formular.

            Args:
                param (float): Values between 0 - 1
                idx (int): An index number.
            """
            if param == 0:
                param = 0.05
            elif param == 1:
                param = 0.95

            # 1 - t
            invp = self.init_node(
                prc.create('plusMinusAverage'), 'InvertParam', idx, side, 'Sub')
            invp.attr('op').v = prc.Operator.subtract
            defattr = invp.add(ln='default', k=True, dv=1)
            defattr >> invp.attr('i1').last()
            paramattr = invp.add(ln='param', k=True, dv=param)
            paramattr >> invp.attr('i1').last()

            # 3 Points
            def _add_a_point(name, point_a, point_b):
                """Gets the result of (1 - t)A + tB as a plusMinusAverage node.

                Args:
                    name (str):
                    point_a (prc.Attr):
                    point_b (prc.Attr):
                
                Returns:
                    prc.Node:
                """
                plus = self.init_node(
                    prc.create('plusMinusAverage'), name, idx, side, 'Sub')

                multa = self.init_node(
                    prc.create('multiplyDivide'), '{}PntA'.format(name), idx,
                    side, 'Mult')
                invp.attr('o1') >> multa.attr('i1x')
                invp.attr('o1') >> multa.attr('i1y')
                invp.attr('o1') >> multa.attr('i1z')
                point_a >> multa.attr('i2')

                multb = self.init_node(
                    prc.create('multiplyDivide'), '{}PntB'.format(name), idx,
                    side, 'Mult')
                multb.attr('i1').v = (param,)*3
                point_b >> multb.attr('i2')

                multa.attr('o') >> plus.attr('i3').last()
                multb.attr('o') >> plus.attr('i3').last()

                return plus

            root_point = _add_a_point(
                'Start', self.root_loc.attr('t'), self.mid_loc.attr('t'))
            end_point = _add_a_point(
                'End', self.mid_loc.attr('t'), self.end_loc.attr('t'))
            result = _add_a_point(
                'Det', root_point.attr('o3'), end_point.attr('o3'))

            loc = prc.Locator()
            loc.set_parent(self.det_grp)
            result.attr('o3') >> loc.attr('t')

        for ix in range(joint_amount + 1):
            _add_det((1./joint_amount)*ix, ix + 1)
