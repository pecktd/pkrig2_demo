# Python modules.
from imp import reload
import math
from inspect import getmembers

# Maya modules.
import maya.OpenMaya as om
import maya.OpenMayaAnim as oma
import maya.cmds as mc

# pkrig modules.
from . import core as prc
from . import naming_tools as prnt

reload(prc)

# Max/Min values to be used on translation.
MAX_T = 1000
MIN_T = -1000


def print_local_vars(rigmod):
    """Returns all local variables of the given rigging module.

    Args:
        rigmod (str): A rigging module name in a format of mod_name.ClassName
            ex. genrig_fk.FkRig
    
    Returns:
        tuple: A tuple of local variables.
    """
    mod, class_ = rigmod.split('.')
    exec('from . import {}'.format(mod))
    members = eval('getmembers({m}.{c})'.format(m=mod, c=class_))
    for member in members:
        if '__init__' in member:
            return member[1].__code__.co_names


def connect_trs(daga, dagb):
    """Connects translate, rotate and scale attributes from the given
    daga to dagb.

    Args:
        daga (prc.Dag):
        dagb (prc.Dag):
    """
    for attr in ('t', 'r', 's'):
        daga.attr(attr) >> dagb.attr(attr)


def mvec_to_tuple(mvec):
    """Converts MVector to tuple.

    Args:
        mvec (om.MVector):
    
    Returns:
        tuple:
    """
    return (mvec.x, mvec.y, mvec.z)


def selected_edges_to_eyelid_curve(name):
    """Converts selected poly edges to an eyelid curve.
    Args:
        name (str):
    
    Returns:
        prc.Dag: Eyelid curve.
    """
    crv = prc.Dag(
        mc.polyToCurve(ch=False, form=0, degree=1)[0])
    crvshape = crv.get_shape()
    crvspans = crvshape.attr('spans').v

    cv0pos = mc.xform('{}.cv[0]'.format(crv.name), q=True, t=True, ws=True)
    cvlastpos = mc.xform(
        '{}.cv[{}]'.format(crv.name, crvspans), q=True, t=True, ws=True)
    if abs(cv0pos[0]) > abs(cvlastpos[0]):
        mc.reverseCurve(crv.name, ch=False)

    if name:
        side = prc.Side.left
        if cv0pos[0] < 0:
            side = prc.Side.right

        crv.name = prnt.compose(name, None, side, 'TmpCrv')

    return crv


def create_eyelid_joints(name, crv, ori):
    """Creates eyelid joints on the given crv.

    Args:
        name (str):
        crv (prc.Dag):
        ori (prc.Dag):
    
    Returns:
        tuple: Three eyelid joints.
    """
    crvshape = crv.get_shape()
    spans = crvshape.attr('spans').v

    inpos = get_point_at_curve_param(crv, spans*0.25)
    midpos = get_point_at_curve_param(crv, spans*0.5)
    outpos = get_point_at_curve_param(crv, spans*0.75)

    def _joint_at(subname, pos, par):
        jnt = prc.Joint()
        jnt.world_pos = pos
        jnt.snap_orient(ori)
        side = prc.Side.left
        if inpos[0] < 0:
            side = prc.Side.right
        jnt.name = prnt.compose(
            '{}{}'.format(name, subname), None, side, 'TmpJnt')
        if par:
            jnt.set_parent(par)

        return jnt
    
    mainjnt = _joint_at('', midpos, None)
    injnt = _joint_at('In', inpos, mainjnt)
    midjnt = _joint_at('Mid', midpos, mainjnt)
    outjnt = _joint_at('Out', outpos, mainjnt)

    return mainjnt, injnt, midjnt, outjnt


def mirror_selected_facial_joints():
    for sel in mc.ls(sl=True):
        mirjnts = list(prc.to_dags(mc.mirrorJoint(
            sel, mirrorYZ=True, mirrorBehavior=True,
            searchReplace=('_L_', '_R_'))))
        mirjnts.reverse()

        mirjnts[-1].freeze()

        jnthrc = []
        for mirjnt in mirjnts:
            jnthrc.append({mirjnt: mirjnt.get_parent()})

            mirjnt.set_parent(None)
            mirjnt.attr('rz').v = 180
            mirjnt.freeze()

        for jntpar in jnthrc:
            jntpar.keys()[0].set_parent(jntpar.values()[0])


def remove_connected_constraints(dag):
    """Removes all constraint nodes that connect to the given dag.

    Args:
        dag (prc.Dag):
    """
    mc.delete(mc.listConnections(dag, s=True, d=False, type='constraint'))


def curve_guide(root_dag, end_dag):
    """Creates a NURBs curve guide from the root_dag to the end_dag.

    Args:
        root_dag (prc.Dag):
        end_dag (prc.Dag):
    """
    def _rename_handle_shape(handle):
        shape = mc.listRelatives(
            handle.name, f=True, s=True, type='clusterHandle')[0]
        mc.rename(shape, '{}ClusterHandleShape'.format(handle.name))

    curve = prc.Dag(mc.curve(d=1, p=[(0, 0, 0), (0, 0, 0)]))

    root_clust, root_clust_handle = prc.cluster(
        '{}.cv[0]'.format(curve.name), wn=(root_dag.name, root_dag.name))
    _rename_handle_shape(root_dag)

    end_clust, end_clust_handle = prc.cluster(
        '{}.cv[1]'.format(curve.name), wn=(end_dag.name, end_dag.name))
    _rename_handle_shape(end_dag)

    curve.attr('overrideEnabled').v = True
    curve.attr('overrideDisplayType').v = 2
    curve.attr('inheritsTransform').v = False

    return curve, root_clust, root_clust_handle, end_clust, end_clust_handle


def orient_bone(jnt, up_axis, up_vec):
    """Aims Y axis of the given joint to the first child node.

    Args:
        jnt (prc.Dag):
        up_axis (tuple):
        up_vec (tuple):
    """
    parnode = jnt.get_parent()
    chdjnts = jnt.get_all_children()

    def _reset_ori(jnt_):
        """Sets rotation and joint orient of the given jnt_ to 0

        Args:
            jnt_ (prc.Joint):
        """
        jnt_.attr('r').v = (0, 0, 0)
        jnt_.attr('jo').v = (0, 0, 0)

    if chdjnts:
        for chdjnt in chdjnts:
            # Parents all child joints to world.
            chdjnt.set_parent(None)
        jnt.set_parent(None)

        aim(jnt, chdjnts[0], (0, 1, 0), up_axis, up_vec)

        for chdjnt in chdjnts:
            # Reparents all child joints.
            chdjnt.set_parent(jnt)

            if chdjnt.m_object.apiType() != om.MFn.kJoint:
                # Skips if current node is not joint.
                continue

            # If current child joint doesn't have grandchild joints,
            # resets its orientation.
            if not chdjnt.get_child():
                _reset_ori(chdjnt)
    else:
        _reset_ori(jnt)

    jnt.set_parent(parnode)
    mc.select(jnt.name, r=True)


def get_rotation_quaternion(dag):
    """Gets quaternion rotation from the given dag.

    Args:
        dag (prc.Dag):

    Returns:
        om.MQuaternion
    """
    xformfn = om.MFnTransform(dag.m_dag_path)

    utilx = om.MScriptUtil(0.0)
    ptrx = utilx.asDoublePtr()

    utily = om.MScriptUtil(0.0)
    ptry = utily.asDoublePtr()

    utilz = om.MScriptUtil(0.0)
    ptrz = utilz.asDoublePtr()

    utilw = om.MScriptUtil(0.0)
    ptrw = utilw.asDoublePtr()

    xformfn.getRotationQuaternion(
        ptrx, ptry, ptrz, ptrw, om.MSpace.kWorld)

    return om.MQuaternion(
        utilx.getDouble(ptrx), utily.getDouble(ptry), utilz.getDouble(ptrz),
        utilw.getDouble(ptrw))


def get_offset_rotate(dag, target):
    """Gets offset rotation values from 'dag' to 'target'.

    Args:
        dag (prc.Dag):
        target (prc.Dag):

    Returns:
        list: A list of rotate values(float)
    """
    dagquat = get_rotation_quaternion(dag)
    dagquat_inv = dagquat.inverse()
    tarquat = get_rotation_quaternion(target)

    ofstquat = tarquat*dagquat_inv
    ofsteul = ofstquat.asEulerRotation()
    ofsteul.reorderIt(dag.attr('rotateOrder').v)

    return [
        math.degrees(angle) for angle in [ofsteul.x, ofsteul.y, ofsteul.z]]


def add_and_mult(driver, driven, mult):
    """Uses addDoubleLinear to add additional value to the given driven.
    Then amplify/reduce the value from the driver using multDoubleLinear.

    Args:
        driver (prc.Attr):
        driven (prc.Attr):
        mult (float): Default multiplier.

    Returns:
        prc.Node: addDoubleLinear
        prc.Node: multDoubleLinear
    """
    add = prc.create('addDoubleLinear')
    add.add(ln='default', dv=driven.v)
    add.attr('default') >> add.attr('i2')
    add.attr('o') >> driven

    amper = attr_amper(
        driver, add.attr('i1'), mult, '{}Amp'.format(driver.attr))

    return add, amper


def add_vector_attr(node, attr_prefix, k):
    """Adds a vector attribute with the given attr_prefix to the given node.

    Args:
        node (prc.Node):
        attr_prefix (str):
        k (bool): Keyable?

    Returns:
        prc.Attr:
    """
    node.add(ln=attr_prefix, at='double3', k=k)
    node.add(ln='{}X'.format(attr_prefix), at='double', p=attr_prefix, k=k)
    node.add(ln='{}Y'.format(attr_prefix), at='double', p=attr_prefix, k=k)
    node.add(ln='{}Z'.format(attr_prefix), at='double', p=attr_prefix, k=k)

    return node.attr(attr_prefix)


def mult_by_parent_inverse_mat(dag, driver, t, r, s):
    """Uses multMatrix node to multiply between the driver(A matrix attribute)
    and the dag's parentInverseMatrix.
    decomposeMatrix is also created to get the transformation output.

    Usage:
        mult_by_parent_inverse_mat(
            prc.Dag('locator1'), prc.Node('fourByFourMatrix1').attr('output'),
            True, True, True)

    Args:
        dag (prc.Dag):
        driver (prc.Attr): A matrix output attribute.
        t (bool): True, dag.attr('t') is connected.
        r (bool): True, dag.attr('r') is connected.
        s (bool): True, dag.attr('s') is connected.

    Returns:
        prc.Node: multMatrix node.
        prc.Node: decomposeMatrix node.
    """
    multmat = prc.create('multMatrix')
    decomp = prc.create('decomposeMatrix')

    driver >> multmat.attr('matrixIn').last()
    dag.attr('parentInverseMatrix[0]') >> multmat.attr('matrixIn').last()
    multmat.attr('matrixSum') >> decomp.attr('inputMatrix')
    dag.attr('rotateOrder') >> decomp.attr('inputRotateOrder')

    if t:
        decomp.attr('outputTranslate') >> dag.attr('t')
    if r:
        decomp.attr('outputRotate') >> dag.attr('r')
    if s:
        decomp.attr('outputScale') >> dag.attr('s')

    return multmat, decomp


def swing_twist_nodes(axis):
    """Adds swing/twist node connections.

    Args:
        axis (str): A twist axis. Valid values are 'X', 'Y' and 'Z'

    Returns:
        prc.Node: eulerToQuat
        prc.Node: quatToEuler(twist)
        prc.Node: quatToEuler(swing)
        prc.Node: quatInvert
        prc.Node: quatProd
    """
    e2q = prc.create('eulerToQuat')
    q2etw = prc.create('quatToEuler')
    q2esw = prc.create('quatToEuler')
    quatinv = prc.create('quatInvert')
    quatprod = prc.create('quatProd')

    inquat = 'inputQuat{}'.format(axis.upper())
    outquat = 'outputQuat{}'.format(axis.upper())

    # Twist
    e2q.attr(outquat) >> q2etw.attr(inquat)
    e2q.attr('outputQuatW') >> q2etw.attr('inputQuatW')

    # Swing
    e2q.attr(outquat) >> quatinv.attr(inquat)
    e2q.attr('outputQuatW') >> quatinv.attr('inputQuatW')
    quatinv.attr('outputQuat') >> quatprod.attr('input1Quat')
    e2q.attr('outputQuat') >> quatprod.attr('input2Quat')
    quatprod.attr('outputQuat') >> q2esw.attr('inputQuat')

    return e2q, q2etw, q2esw, quatinv, quatprod


def add_non_roll_joint(dag, axis, parent):
    """Adds non-roll joint to the given dag and parent the created node
    to the given parent.
    The type of created node is defined by the given dag_type.

    Args:
        dag (prc.Dag): A target node.
        axis (str): 'x', 'y' or 'z'.
        parent (prc.Dag): A parent node that creating Dag will be parented to.

    Returns:
        prc.Joint: Non-roll Joint
        prc.Dag: pointConstraint
        prc.Dag: orientConstraint
        prc.Node: eulerToQuat
        prc.Node: quatToEuler(twist)
        prc.Node: quatToEuler(swing)
        prc.Node: quatInvert
        prc.Node: quatProd
    """
    nonroll = create_joint_at(dag)
    if parent:
        nonroll.set_parent(parent)

    pointcons = prc.point_constraint(dag, nonroll)
    oricons = prc.orient_constraint(dag, nonroll)

    if axis == 'y':
        nonroll.attr('ro').v = prc.Ro.yzx
    elif axis == 'x':
        nonroll.attr('ro').v = prc.Ro.xyz
    elif axis == 'z':
        nonroll.attr('ro').v = prc.Ro.zxy

    e2q, q2etw, q2esw, quatinv, quatprod = swing_twist_nodes(axis)

    nonroll.attr('ro') >> e2q.attr('inputRotateOrder')
    nonroll.attr('ro') >> q2etw.attr('inputRotateOrder')
    nonroll.attr('ro') >> q2esw.attr('inputRotateOrder')

    oricons.attr('cr') >> e2q.attr('inputRotate')

    # Twist
    nonroll.add(ln='twist', k=True)
    outrot = 'outputRotate{}'.format(axis.upper())
    q2etw.attr(outrot) >> nonroll.attr('twist')

    # Swing
    q2esw.attr('outputRotateX') >> nonroll.attr('rx')
    q2esw.attr('outputRotateY') >> nonroll.attr('ry')
    q2esw.attr('outputRotateZ') >> nonroll.attr('rz')

    return nonroll, pointcons, oricons, e2q, q2etw, q2esw, quatinv, quatprod


def extract_swing_twist(dag, axis):
    """Extracts swing and twist rotations from the given dag.

    Args:
        dag (prc.Dag):
        axis (str): A twist axis. Valid values are 'X', 'Y' and 'Z'

    Returns:
        prc.Node: eulerToQuat
        prc.Node: quatToEuler(twist)
        prc.Node: quatToEuler(swing)
        prc.Node: quatInvert
        prc.Node: quatProd
    """
    dag.add(ln='swing', at='double3')
    dag.add(ln='swingX', at='double', p='swing')
    dag.add(ln='swingY', at='double', p='swing')
    dag.add(ln='swingZ', at='double', p='swing')
    dag.add(ln='twist')

    e2q, q2etw, q2esw, quatinv, quatprod = swing_twist_nodes(axis)

    dag.attr('r') >> e2q.attr('inputRotate')
    dag.attr('ro') >> e2q.attr('inputRotateOrder')
    dag.attr('ro') >> q2etw.attr('inputRotateOrder')
    dag.attr('ro') >> q2esw.attr('inputRotateOrder')

    # Twist
    outrot = 'outputRotate{}'.format(axis.upper())
    q2etw.attr(outrot) >> dag.attr('twist')

    # Swing
    q2esw.attr('outputRotate') >> dag.attr('swing')

    return e2q, q2etw, q2esw, quatinv, quatprod


def add_divide_attr(ctrl, attrname):
    """Adds divide attribute to be used as a divider in channel box.
    It starts and ends with '__' and is locked.

    Args:
        ctrl (prc.Dag):
        attrname (str):

    Returns:
        prc.Attr: A created attribute.
    """
    attrname = '__{}__'.format(prnt.lowfirst(attrname.strip('_')))
    ctrl.add(ln=attrname, k=True)
    ctrl.attr(attrname).lock = True

    return ctrl.attr(attrname)


def add_shape_vis_controller(ctrl, target, attrname):
    """Adds the given 'attrname' to ctrl.shape to control the visibility of
    the shape node of the given target, if the given ctrl doesn't have
    a shape, adds to itself instead.

    Args:
        ctrl (prc.Dag):
        target (prc.Dag):
        attrname (str):

    Returns:

    """
    ctrlshape = ctrl.get_shape()
    if ctrlshape:
        if not ctrlshape.attr(attrname).exists:
            ctrlshape.add(ln=attrname, k=True, min=0, max=1)
        driver = ctrlshape.attr(attrname)
    else:
        if not ctrl.attr(attrname).exists:
            ctrl.add(ln=attrname, k=True, min=0, max=1)
        driver = ctrl.attr(attrname)

    if target.get_shape():
        target = target.get_shape()

    driver >> target.attr('v')


def add_offset_controller(ctrl, attr, drivens, mult, dv):
    """Makes the given ctrl.attr(attr) control the additional values to
    the given drivens e.g., for stretchy FK that uses offset translation.

    The given attr is created if it doesn't exist on the given ctrl.
    [attr]Amper is created at ctrl's shape node as a multiplier.

    Args:
        ctrl (prc.Dag):
        attr (str):
        drivens (list): A list of prc.Attr(s)
        mult (float): A default value of multiplier.
        dv (float): A default value.

    Returns:
        prc.Node: A multDoubleLinear.
        prc.Node: A addDoubleLinear.
    """
    if not ctrl.attr(attr).exists:
        ctrl.add(ln=attr, k=True)

    mult = attr_amper(ctrl.attr(attr), None, mult, '{}Amper'.format(attr))

    add = prc.create('addDoubleLinear')
    add.add(ln='default', k=True, dv=dv)
    add.attr('default') >> add.attr('i1')
    mult.attr('o') >> add.attr('i2')
    for driven in drivens:
        add.attr('o') >> driven

    return mult, add


def m_matrix_to_tuple(mat):
    """Translates om.MMatrix to a tuple.
    Ex.
        (
            (1, 0, 0, 0),
            (0, 1, 0, 0),
            (0, 0, 1, 0),
            (0, 0, 0, 1),
        )

    Args:
        mat (om.MMatrix):

    Returns:
        tuple:
    """
    return tuple(
        tuple(om.MScriptUtil.getDoubleArrayItem(mat[r], c) for c in range(4))
        for r in range(4))


def create_controller_groups(
        curve_param, group_types, compo, desc, index, side):
    """Creates a controller with its parent groups.

    Args:
        curve_param (prc.Cp):
        group_types (list): A list of group types(str).
        compo (str):
        desc (str):
        index (int):
        side (prc.Side):

    Returns:
        prc.Dag: A controller.
        list: A list of groups(prc.Dag).
    """
    ctrl = prc.Controller(curve_param)
    ctrl.name = prnt.compose(
        '{c}{d}'.format(c=compo, d=desc), index, side, 'Ctrl')

    groups = []
    child = ctrl
    for grouptype in group_types:
        if groups:
            child = groups[-1]
        currgrp = prc.group(child)
        currgrp.name = prnt.compose(
            '{c}Ctrl{d}'.format(c=compo, d=desc), index, side, grouptype)
        groups.append(currgrp)

    return ctrl, groups


def rotate_between_two_objects(obj, start_obj, end_obj, weight):
    """Sets rotation of the given obj to weighted rotate between
    start_obj and end_obj with the given weight.
    If the given weight is 0 the given obj is oriented to start_obj.
    If the given weight is 1 the given obj is oriented to end_obj.

    Args:
        obj (prc.Dag):
        start_obj (prc.Dag):
        end_obj (prc.Dag):
        weight (float): Parameter between 0.0 and 1.0

    Returns:
        None
    """
    start_obj_mat = start_obj.m_dag_path.inclusiveMatrix()
    start_obj_xform_mat = om.MTransformationMatrix(start_obj_mat)
    start_obj_quat = start_obj_xform_mat.rotation()

    end_obj_mat = end_obj.m_dag_path.inclusiveMatrix()
    end_obj_xform_mat = om.MTransformationMatrix(end_obj_mat)
    end_obj_quat = end_obj_xform_mat.rotation()

    halfway_quat = slerp(start_obj_quat, end_obj_quat, weight)

    xformfn = om.MFnTransform(obj.m_dag_path)
    eulerrot = halfway_quat.asEulerRotation()
    eulerrot.reorderIt(xformfn.rotationOrder() - 1)
    degrees = [
        math.degrees(angle) for angle in [eulerrot.x, eulerrot.y, eulerrot.z]]

    obj.attr('r').v = degrees


def slerp(quat_a, quat_b, weight):
    """Finds slerp between the given quat_a and quat_b with the given weight.
    The original code is from
    https://github.com/chadmv/cmt/blob/master/scripts/cmt/rig/swingtwist.py

    Args:
        quat_a (om.MQuaternion):
        quat_b (om.MQuaternion):
        weight (float): Parameter between 0.0 and 1.0

    Returns:
        om.MQuaternion:
    """
    result = om.MQuaternion()

    # Calculate angle between them.
    cos_half_theta = quat_a.w * quat_b.w \
        + quat_a.x * quat_b.x \
        + quat_a.y * quat_b.y \
        + quat_a.z * quat_b.z
    if abs(cos_half_theta) >= 1.0:
        result.w = quat_a.w
        result.x = quat_a.x
        result.y = quat_a.y
        result.z = quat_a.z
        return quat_a

    # Calculate temporary values
    half_theta = math.acos(cos_half_theta)
    sin_half_theta = math.sqrt(1.0 - cos_half_theta * cos_half_theta)
    # if theta = 180 degrees then result is not fully defined
    # we could rotate around any axis normal to quat_a or quat_b
    if math.fabs(sin_half_theta) < 0.001:
        result.w = quat_a.w * 0.5 + quat_b.w * 0.5
        result.x = quat_a.x * 0.5 + quat_b.x * 0.5
        result.y = quat_a.y * 0.5 + quat_b.y * 0.5
        result.z = quat_a.z * 0.5 + quat_b.z * 0.5
        return result

    ratio_a = math.sin((1 - weight) * half_theta) / sin_half_theta
    ratio_b = math.sin(weight * half_theta) / sin_half_theta

    # Calculate quaternion
    result.w = quat_a.w * ratio_a + quat_b.w * ratio_b
    result.x = quat_a.x * ratio_a + quat_b.x * ratio_b
    result.y = quat_a.y * ratio_a + quat_b.y * ratio_b
    result.z = quat_a.z * ratio_a + quat_b.z * ratio_b

    return result


def add_space_blender(
        target_nodes_and_attr_names, cons_func, constrained_node, controller,
        pivot):
    """Adds space blender with the given cons_func to the constrained_node.

    Args:
        target_nodes_and_attr_names (tuple): A tuple of dictionaries that have
            target node as keys and attribute enum names as values.
            Ex. ({prc.Dag('Head_Jnt'): 'head'}, {prc.Dag('Eye_Jnt'): 'eye'})
        cons_func (prc.orient_constraint/prc.point_constraint/
            prc.parent_constraint):
        constrained_node (prc.Dag): 'Local' space is always added as the first
            space blender.
            It uses constrained_node's parent as its space.
        controller (prc.Dag):
        pivot (prc.Dag):

    Returns:
        list: A list of prc.Dag, space nodes.
        list: A list of prc.Node, conditiont nodes.
    """
    # Defines attribute prefix.
    attrprefix = 'pointTo'
    if cons_func == prc.orient_constraint:
        attrprefix = 'orientTo'
    elif cons_func == prc.parent_constraint:
        attrprefix = 'parentTo'

    # Collects all target nodes.
    # The local space is added as the first space.
    parent = constrained_node.get_parent()
    tars_attrs = [{parent: 'local'}] + list(target_nodes_and_attr_names)

    # Adds enum attribute.
    enumnames = [tar_attr.values()[0] for tar_attr in tars_attrs]
    enum = ':'.join(enumnames)
    controller.add(
        longName=attrprefix, attributeType='enum', enumName=enum,
        keyable=True)

    # Creates target nodes.
    attrs = []
    space_pivs = []
    space_nodes = []
    for ix, tar_attr in enumerate(tars_attrs):
        currtar = tar_attr.keys()[0]
        currattr = tar_attr.values()[0]
        attrs.append(currattr)

        spc = prc.Null()
        spc.name = '{}{}'.format(constrained_node.name, prnt.capfirst(currattr))
        space_nodes.append(spc)

        spcpiv = prc.group(spc)
        spcpiv.name = '{}_Piv'.format(spc.name)
        space_pivs.append(spcpiv)

        spcpiv.snap(pivot)
        spc.snap(constrained_node)
        spcpiv.set_parent(parent)

        # Skips the local space.
        if ix > 0:
            prc.parent_constraint(currtar, spcpiv, mo=True)

    space_nodes.append(constrained_node)
    cons = cons_func(*space_nodes, mo=True)

    conds = []
    for ix in range(len(space_nodes[:-1])):
        currcond = prc.create('condition')
        currcond.name = '{}_Cnd'.format(space_nodes[ix])
        conds.append(currcond)

        controller.attr(attrprefix) >> currcond.attr('firstTerm')
        currcond.attr('secondTerm').v = ix
        currcond.attr('colorIfTrueR').v = 1
        currcond.attr('colorIfFalseR').v = 0
        currcond.attr('outColorR') >> cons.attr('w{}'.format(ix))

    return space_nodes, space_pivs, conds, attrs


def do_positions_match(dag1, dag2, tol=0.001):
    """Compares the positions of the given dag1 and dag2.
    Returns True if the positions match with the given tolerance.

    Args:
        dag1 (prc.Dag):
        dag2 (prc.Dag):
        tol (float):

    Returns:
        bool: True if the poistions match else False.
    """
    dag1pos = dag1.world_pos
    dag2pos = dag2.world_pos
    for ix in range(3):
        if not ((dag2pos[ix] - tol) <= dag1pos[ix] <= (dag2pos[ix] + tol)):
            return False
    return True


def do_vectors_match(vect1, vect2, tol=0.001):
    """Compares two vectors of three components.
    Returns True if all values are match with the given tolerance.

    Args:
        vect1 (list): A list of three floats.
        vect2 (list): A list of three floats.
        tol (float):
    
    Returns:
        bool: True if all values match else False.
    """
    for ix in range(3):
        if not ((vect2[ix] - tol) <= vect1[ix] <= (vect2[ix] + tol)):
            return False

    return True


def get_joint_heirarchy_with_sibling_first(root_name):
    """Builds a hierarchy of joints from the given root to the child most with
    the sibling nodes first.
    This function is used to build a local skin joint hierarchy.

    Ex.
    root
        |- head
        |- left_eye
        |   |- left_lid_a
        |   `- left_lid_b
        `- right_eye
    returns ['|head', '|head|left_eye', '|head|left_eye|left_lid_a',
    '|head|left_eye|left_lid_b', '|head|right_eye']

    Args:
        root_name (str):

    Returns:
        list: A list of joint's full path names(str).
    """
    root_dagpath = prc.dag_path_from_dag_str(root_name)
    dagiter = om.MItDag(om.MItDag.kBreadthFirst)
    dagiter.reset(root_dagpath)

    hierarchy = []
    while not dagiter.isDone():

        curr_dagpath = om.MDagPath()
        dagiter.getPath(curr_dagpath)
        curr_node = dagiter.currentItem()

        if curr_node.apiType() == om.MFn.kJoint:
            hierarchy.append(curr_dagpath.fullPathName())

        try:
            # Attempts to extend the path to the shape node.
            curr_dagpath.extendToShape()
            dagiter.next()
        except RuntimeError:
            # Do nothing if this operation fails.
            pass

        dagiter.next()

    return hierarchy


def build_local_joint_hierarchy(root_name):
    """Builds a hierarchy of local joints from the given root.

    Args:
        root_name (str):

    Returns:
        list: A list of created joints(prc.Joint).
    """
    root_dag = prc.dag_path_from_dag_str(root_name)
    root_fullpath = root_dag.fullPathName()
    hierarchy = get_joint_heirarchy_with_sibling_first(root_name)

    jnts = []
    parents = []
    for jnt in hierarchy:
        # Loops through each joint in the hierarchy.
        # Creates a local joint and snaps to current joint.
        # Uses full path of the root joint to split the joint's name
        # to get the local path.
        # Counts '|' sign in the local path to get the parent ID.
        # Tries assign current local joint to the parent ID(index)
        # of parent list, append if current index doesn't exist.
        # Continues if current joint is the local root.
        # Sets the latest parent as the parent of current local joint.
        # Connects translate, rotate and scale
        # from current joint to current local joint.
        jntobj = prc.to_pkrig_node(jnt)

        currjnt = create_joint_at(jntobj)
        jnts.append(currjnt)

        if prnt.is_legal(jntobj.name):
            compo, index, side, type_ = prnt.tokenize(jntobj.name)
            currjnt.name = prnt.compose(compo, index, side, 'LocJnt')

        fullpath = jntobj.m_dag_path.fullPathName()
        localpath = fullpath.split(root_fullpath)[-1]
        parentid = localpath.count('|')

        try:
            parents[parentid] = currjnt
        except IndexError:
            parents.append(currjnt)

        if not parentid:
            continue

        currjnt.set_parent(parents[parentid - 1])
        currjnt.attr('jointOrient').v = jntobj.attr('jointOrient').v
        currjnt.attr('r').v = jntobj.attr('r').v

        for trs in ('t', 'r', 's'):
            jntobj.attr(trs) >> currjnt.attr(trs)

    return jnts


def remap_to_blend_shape_driver(driver, driven_node, attr_name):
    """Uses a remap node to remap the given driver to 0 to max,
    creates an attr_name at the driven_node,
    creates a max attribute at the driven_node to control the max value,
    connects the remap output to the driven_node.attr_name.

    The max attribute is named as attrNameMax

    Args:
        driver (prc.Attr):
        driven_node (prc.Node):
        attr_name (str):

    Returns:
        prc.Node: A remap node.
        prc.Attr: An output attribute.
        prc.Attr: A max attribute.
    """
    max_attrname = '{}Max'.format(attr_name)
    driven_node.add(longName=attr_name, keyable=True)
    driven_node.add(longName=max_attrname, keyable=True)

    remap = prc.create('remapValue')
    driven_node.attr(max_attrname).v = 1
    remap.attr('outputMax').v = 1

    driven_node.attr(max_attrname) >> remap.attr('inputMax')
    driver >> remap.attr('inputValue')
    remap.attr('outValue') >> driven_node.attr(attr_name)

    return remap, driven_node.attr(attr_name), driven_node.attr(max_attrname)


def get_param_of_closest_point_on_curve(curve, dag):
    """Gets curve paramter of the given curve that has the closest
    distant to the given dag.

    Args:
        curve (prc.Dag): Its shape has to be NURBs curve.
        dag (prc.Dag):

    Returns
        float: A curve paramter.
    """
    curvedag = prc.dag_path_from_dag_str(curve.name)

    curvedag.extendToShape()
    curvefn = om.MFnNurbsCurve(curvedag)

    point = om.MPoint(*dag.world_pos)
    paramutil = om.MScriptUtil()
    paramptr = paramutil.asDoublePtr()

    curvefn.closestPoint(point, paramptr)

    return paramutil.getDouble(paramptr)


def get_param_of_closest_point_on_surface(nurb, dag):
    """Gets surface paramters of the given nurb that has the closest
    distant to the given dag.

    Args:
        nurb (prc.Dag): Its shape has to be NURBs surface.
        dag (prc.Dag):

    Returns
        tuple: U and V parameters.
    """
    nurbdag = prc.dag_path_from_dag_str(nurb.name)

    nurbdag.extendToShape()
    nurbfn = om.MFnNurbsSurface(nurbdag)

    point = om.MPoint(*dag.world_pos)

    closest_point = nurbfn.closestPoint(point)

    paramutil = om.MScriptUtil()
    uptr = paramutil.asDoublePtr()
    vptr = paramutil.asDoublePtr()
    nurbfn.getParamAtPoint(closest_point, uptr, vptr)

    return paramutil.getDouble(uptr), paramutil.getDouble(vptr)


def get_point_at_surface_param(nurb, u, v):
    """Gets world position at the given surface parameters.

    Args:
        nurb (prc.Dag):
        u (float):
        v (float):

    Returns:
        tuple: A world space vector.
    """
    nurbdag = prc.dag_path_from_dag_str(nurb.name)

    nurbdag.extendToShape()
    nurbfn = om.MFnNurbsSurface(nurbdag)

    point = om.MPoint()
    nurbfn.getPointAtParam(u, v, point)

    return point.x, point.y, point.z


def get_point_at_curve_param(curve, p):
    """Gets world position at the given curve parameters.

    Args:
        curve (prc.Dag):
        p (float):

    Returns:
        tuple: A world space vector.
    """
    curvedag = prc.dag_path_from_dag_str(curve.name)

    curvedag.extendToShape()
    curvefn = om.MFnNurbsCurve(curvedag)

    point = om.MPoint()
    curvefn.getPointAtParam(p, point, om.MSpace.kWorld)

    return point.x, point.y, point.z


def get_normal_at_curve_param(curve, p):
    """Gets normal vector at the given curve parameters.

    Args:
        curve (prc.Dag):
        p (float):

    Returns:
        tuple: A normal vector.
    """
    curvedag = prc.dag_path_from_dag_str(curve.name)

    curvedag.extendToShape()
    curvefn = om.MFnNurbsCurve(curvedag)

    n = curvefn.normal(p, om.MSpace.kWorld)

    return n.x, n.y, n.z


def get_tangent_at_curve_param(curve, p):
    """Gets tangent vector at the given curve parameters.

    Args:
        curve (prc.Dag):
        p (float):

    Returns:
        tuple: A normal vector.
    """
    curvedag = prc.dag_path_from_dag_str(curve.name)

    curvedag.extendToShape()
    curvefn = om.MFnNurbsCurve(curvedag)

    t = curvefn.tangent(p, om.MSpace.kWorld)

    return t.x, t.y, t.z


def get_pnuv_at_surface_param(nrb, pu, pv):
    """Gets point, normal, tangent u and tangent v at the given
    surface parameter.

    Args:
        nrb (prc.Dag):
        pu (float):
        pv (float):
    
    Returns:
        tuple: Point.
        tuple: Normal vector.
        tuple: Tangent U.
        tuple: Tangent V.
    """
    sellist = om.MSelectionList()
    sellist.add(nrb.name)
    nrbdag = om.MDagPath()
    sellist.getDagPath(0, nrbdag)

    nrbdag.extendToShape()
    nrbfn = om.MFnNurbsSurface(nrbdag)

    point = om.MPoint()
    nrbfn.getPointAtParam(pu, pv, point)

    normal = nrbfn.normal(pu, pv, om.MSpace.kWorld)
    
    tu = om.MVector()
    tv = om.MVector()
    nrbfn.getTangents(pu, pv, tu, tv, om.MSpace.kWorld)

    return mvec_to_tuple(point), mvec_to_tuple(normal), mvec_to_tuple(tu), \
        mvec_to_tuple(tv)


def move_dag_to_surface(
        dag, nrb, pu, pv, aimvec, upvec, target_translate, world_up):
    """Moves the given DAG node to the point at surface paramter.

    Args:
        dag (prc.Dag):
        nrb (prc.Dag): NURBs surface.
        pu (float): Parameter U.
        pv (float): Parameter V.
        aimvec (tuple): A tuple of three floats as an aim vector.
        upvec (tuple): A tuple of three floats as an up vector.
        target_translate (str): Valid strs are 'u', 'v' and 'n' as along U,
            along V and along Normal.
        world_up (str): Valid strs are 'u', 'v' and 'n' as along U, along V
            and along Normal.
    """
    sellist = om.MSelectionList()
    sellist.add(nrb.name)
    nrbdag = om.MDagPath()
    sellist.getDagPath(0, nrbdag)
    nrbdag.extendToShape()

    nrbfn = om.MFnNurbsSurface(nrbdag)

    point = om.MPoint()
    nrbfn.getPointAtParam(pu, pv, point)

    normal = nrbfn.normal(pu, pv, om.MSpace.kWorld)
    
    tu = om.MVector()
    tv = om.MVector()
    nrbfn.getTangents(pu, pv, tu, tv, om.MSpace.kWorld)

    tt = mvec_to_tuple(normal)
    if target_translate == 'u':
        tt = mvec_to_tuple(tu)
    elif target_translate == 'v':
        tt = mvec_to_tuple(tv)

    wu = mvec_to_tuple(normal)
    if world_up == 'u':
        wu = mvec_to_tuple(tu)
    elif world_up == 'v':
        wu = mvec_to_tuple(tv)
    
    dag.world_pos = mvec_to_tuple(point)
    aimcon = prc.create('aimConstraint')
    aimcon.attr('cr') >> dag.attr('r')
    aimcon.attr('aimVector').v = aimvec
    aimcon.attr('upVector').v = upvec
    aimcon.attr('tg[0].tt').v = tt
    aimcon.attr('worldUpVector').v = wu

    mc.delete(aimcon)


def create_pos(nurb, dag, normal_to, u_to, v_to):
    """Uses fourByFourMatrix, multMatrix and decomposeMatrix nodes to
    get a rivet likes rig to the given DAG node.

    Args:
        nurb (prc.Dag): A transform node of the NURBs surface.
        dag (prc.Dag): As a rivet node.
        normal_to (int): 0, 1 or 2 as a row of Matrix that normalized normal
            connects to.
        u_to (int): 0, 1 or 2 as a row of Matrix that normalized tangent U
            connects to.
        v_to (int): 0, 1 or 2 as a row of Matrix that normalized tangent V
            connects to.

    Returns:
        prc.Node: pointOnSurfaceInfo
        prc.Node: fourByFourMatrix
        prc.Node: multMatrix
        prc.Node: decomposeMatrix
    """
    shape = nurb.get_shape()

    posi = prc.create('pointOnSurfaceInfo')
    fbf = prc.create('fourByFourMatrix')

    shape.attr('worldSpace[0]') >> posi.attr('inputSurface')
    posi.attr('positionX') >> fbf.attr('in30')
    posi.attr('positionY') >> fbf.attr('in31')
    posi.attr('positionZ') >> fbf.attr('in32')

    posi.attr('normalizedNormalX') >> fbf.attr('in{}0'.format(normal_to))
    posi.attr('normalizedNormalY') >> fbf.attr('in{}1'.format(normal_to))
    posi.attr('normalizedNormalZ') >> fbf.attr('in{}2'.format(normal_to))

    posi.attr('normalizedTangentUX') >> fbf.attr('in{}0'.format(u_to))
    posi.attr('normalizedTangentUY') >> fbf.attr('in{}1'.format(u_to))
    posi.attr('normalizedTangentUZ') >> fbf.attr('in{}2'.format(u_to))

    posi.attr('normalizedTangentVX') >> fbf.attr('in{}0'.format(v_to))
    posi.attr('normalizedTangentVY') >> fbf.attr('in{}1'.format(v_to))
    posi.attr('normalizedTangentVZ') >> fbf.attr('in{}2'.format(v_to))

    multmat, decomp = mult_by_parent_inverse_mat(
        dag, fbf.attr('output'), True, True, False)

    return posi, fbf, multmat, decomp


def rotate_by_two_vectors(
        dag, aim_axis, up_axis, aim_vector, up_vector):
    """Aligns aim_axis(axis) of the given DAG to the given
    aim_vector, uses up_axis(axis) as an up axis to the given
    up_vector.

    Args:
        dag (prc.Dag): A transform node.
        aim_axis (tuple): A tuple of three values.
        up_axis (tuple): A tuple of three values.
        aim_vector (tuple): A tuple of three values.
        up_vector (tuple): A tuple of three values.

    Returns:
        list: A list of rotatation values(floats).
    """
    aimaxis = om.MVector(*aim_axis)
    upaxis = om.MVector(*up_axis)

    uvect = om.MVector(*aim_vector).normal()

    tmpup = om.MVector(*up_vector).normal()

    wvect = (uvect ^ tmpup).normal()
    vvect = (wvect ^ uvect).normal()

    quatu = om.MQuaternion(aimaxis, uvect)
    quat = quatu

    upaxis_rotated = upaxis.rotateBy(quatu)
    upaxis_rotated.normalize()

    angle = math.acos(upaxis_rotated*vvect)

    quatv = om.MQuaternion(angle, uvect)

    if not vvect.isEquivalent(upaxis_rotated.rotateBy(quatv), 1.0e-5):
        angle = (2 * math.pi) - angle
        quatv = om.MQuaternion(angle, uvect)

    quat *= quatv
    eulerrot = quat.asEulerRotation()

    xformfn = om.MFnTransform(dag.m_dag_path)
    eulerrot.reorderIt(xformfn.rotationOrder() - 1)
    degrees = [
        math.degrees(angle) for angle in [eulerrot.x, eulerrot.y, eulerrot.z]]

    return degrees


def meuler_rotation_to_tuple(eul):
    """Converts MEulerRotation to a tuple of three floats.

    Args:
        eul (om.MEulerRotation):
    
    Returns:
        tuple:
    """
    return (math.degrees(eul.x), math.degrees(eul.y), math.degrees(eul.z))


def aim(dag, target, dag_aim, dag_up, up_vec):
    """Orients dag_aim axis of dag to target node and aligns dag_up axis
    to up_vec.

    Args:
        dag (prc.Dag):
        target (prc.Dag):
        dag_aim (tuple): An aim vector.
        dag_up (tuple): An up vector.
        up_vec (tuple): A world up vector.
    """
    mc.delete(
        prc.aim_constraint(
            target, dag, aim=dag_aim, u=dag_up, wu=up_vec, wut='vector'))


def aim_by_vectors(
        dag, aim_vector, up_vector, target_translate, wolrd_up_vector):
    """Uses an aimConstrain node to orient the given DAG regarding the given
    Target Translate and World Up Vector.
    """
    aimcon = prc.create('aimConstraint')
    dag.attr('ro') >> aimcon.attr('constraintRotateOrder')
    aimcon.attr('aimVector').v = aim_vector
    aimcon.attr('upVector').v = up_vector
    aimcon.attr('tg[0].tt').v = target_translate
    aimcon.attr('worldUpVector').v = wolrd_up_vector

    loc = prc.Locator()
    loc.attr('t').v = target_translate
    loc.attr('t') >> aimcon.attr('tg[0].tt')

    dag.attr('r').v = aimcon.attr('cr').v

    mc.delete(aimcon)
    mc.delete(loc)


def add_two_ways_clamp(driver, minr, maxr, ming, maxg):
    """Clamps the driver, using min/max values, to two channels.

    Args:
        driver (prc.Attr):
        minr (float): A min value of R channel.
        maxr (float): A max value of R channel.
        ming (float): A min value of G channel.
        maxg (float): A max value of G channel.

    Returns:
        prc.Node: A created clamp node.
    """
    clamp = prc.create('clamp')
    clamp.attr('minR').v = minr
    clamp.attr('maxR').v = maxr
    clamp.attr('minG').v = ming
    clamp.attr('maxG').v = maxg

    driver >> clamp.attr('inputR')
    driver >> clamp.attr('inputG')

    return clamp


def separate_positive_negative_values(
        driver, drivennode, plus_attrstr, minus_attrstr, keyable):
    """Uses clamp node to separate postive and negative values
    of the given driver to two channels.
    Adds the given plus_attrstr and minus_attrstr to the given drivennode.
    Connects the output R and G to plus_attrstr and minus_attrstr respectively.

    Args:
        driver (prc.Attr):
        drivennode (prc.Dag): A node that output attribute will be added to.
        plus_attrstr (str): An attribute name of positive values.
        minus_attrstr (str): An attribute name of negative values.
        keyable (bool):

    Returns:
        prc.Node: A clamp node.
        prc.Node: A multDoubleLinear node.
    """
    clamper = add_two_ways_clamp(driver, 0, MAX_T, MIN_T, 0)
    drivennode.add(longName=plus_attrstr, k=keyable)
    drivennode.add(longName=minus_attrstr, k=keyable)

    clamper.attr('outputR') >> drivennode.attr(plus_attrstr)

    amper = attr_amper(
        clamper.attr('outputG'), drivennode.attr(minus_attrstr), -1,
        '{}Amp'.format(minus_attrstr))

    return clamper, amper


def add_udoipp(ctrl):
    """Adds Up/Down, Out/In and Pull/Push controller to the given ctrl.
    ctrl.ty controls Up/Down, ctrl.tx controls Out/In and ctrl.tz controls
    Pull/Push.
    Amplifiers for each channel are added to shape node of the given controller.

    Uses a clamp node to extract postive/negative values from ty and tx.
    ty+ = up
    ty- = down
    tx+ = out
    tx- = in
    tz+ = pull
    tz- = push

    Args:
        ctrl (prc.Dag):

    Returns:
        prc.Node: Up/Down clamp node.
        prc.Node: Out/In clamp node.
        prc.Node: Up multiplier.
        prc.Node: Down multiplier.
        prc.Node: Out multiplier.
        prc.Node: In multiplier.
    """
    ctrl.add(ln='up')
    ctrl.add(ln='down')
    ctrl.add(ln='out')
    ctrl.add(ln='in')
    ctrl.add(ln='pull')
    ctrl.add(ln='push')

    ctrl.add(ln='origUp')
    ctrl.add(ln='origDown')
    ctrl.add(ln='origOut')
    ctrl.add(ln='origIn')
    ctrl.add(ln='origPull')
    ctrl.add(ln='origPush')

    ud_clamp = add_two_ways_clamp(ctrl.attr('ty'), 0, MAX_T, MIN_T, 0)
    oi_clamp = add_two_ways_clamp(ctrl.attr('tx'), 0, MAX_T, MIN_T, 0)
    pp_clamp = add_two_ways_clamp(ctrl.attr('tz'), 0, MAX_T, MIN_T, 0)

    ud_clamp.attr('outputR') >> ctrl.attr('origUp')
    ud_clamp.attr('outputG') >> ctrl.attr('origDown')
    oi_clamp.attr('outputR') >> ctrl.attr('origOut')
    oi_clamp.attr('outputG') >> ctrl.attr('origIn')
    pp_clamp.attr('outputR') >> ctrl.attr('origPull')
    pp_clamp.attr('outputG') >> ctrl.attr('origPush')

    upamp = attr_amper(ud_clamp.attr('outputR'), ctrl.attr('up'), 1, 'upAmp')
    downamp = attr_amper(
        ud_clamp.attr('outputG'), ctrl.attr('down'), -1, 'downAmp')
    outamp = attr_amper(oi_clamp.attr('outputR'), ctrl.attr('out'), 1, 'outAmp')
    inamp = attr_amper(oi_clamp.attr('outputG'), ctrl.attr('in'), -1, 'inAmp')
    pullamp = attr_amper(
        pp_clamp.attr('outputR'), ctrl.attr('pull'), 1, 'outAmp')
    pushamp = attr_amper(
        pp_clamp.attr('outputG'), ctrl.attr('push'), -1, 'inAmp')

    return ud_clamp, oi_clamp, pp_clamp, upamp, downamp, outamp, inamp, \
        pullamp, pushamp


def attr_clamper(
        driver,
        driven,
        min_value,
        max_value
):
    """Adds a clamp node to clamp value of the driver regarding the given
    min/max_value.
    The output from clamp node connects to the driven if it's given.

    Args:
        driver (prc.Attr):
        driven (prc.Attr):
        min_value (float):
        max_value (float):

    Returns:
        prc.Node: A clamp node.
    """
    clamper = prc.create('clamp')

    clamper.attr('minR').v = min_value
    clamper.attr('maxR').v = max_value

    driver >> clamper.attr('inputR')
    if driven:
        clamper.attr('outputR') >> driven

    return clamper


def attr_amper(
        driver,
        driven,
        dv,
        amp_attr
):
    """Adds an attribute amplifier to the driver that drives
    the driven attribute. If the driver node has shape the amp_attr is added
    to its shape else is added to the driver node.

    Args:
        driver (prc.Attr):
        driven (prc.Attr/None): None if just want to create a multiplier.
        dv (float): A default value of the multiplier.
        amp_attr (str/None): A name of the amp attribute.
            If this is None the amp_attr is not added to the driver node.

    Returns:
        prc.Node: A multDoubleLinear node.
    """
    mult = prc.create('multDoubleLinear')
    mult.add(ln='amp', k=True, dv=dv)

    # Defines the driver node.
    # If current driver node is a DAG and has a shape,
    # its shape is a driver node
    # else the driver itself is a driver node.
    drivernode = driver.node
    if prc.Dag in type(drivernode).__mro__:
        drivershape = driver.node.get_shape()
        if drivershape:
            drivernode = drivershape

    # Defines a way to modify the amplifier.
    # If the amp_attr is given, checks first if it doesn't exist; add it
    # to the driver node else use the existed one.
    # If the amp_attr isn't given, an 'amp' attribute in created mul is used
    # to modofy the amplifier.
    if amp_attr:
        if not drivernode.attr(amp_attr).exists:
            drivernode.add(ln=amp_attr, k=True, dv=dv)
        drivernode.attr(amp_attr) >> mult.attr('amp')

    mult.attr('amp') >> mult.attr('i1')
    driver >> mult.attr('i2')
    if driven:
        mult.attr('o') >> driven

    return mult


def create_joint_at(dag, **kwargs):
    """Creates a joint at the position of the given transform.

    Args:
        dag (prc.Dag/prc.Component):

    Kwargs:
        t (bool): Apply freeze translate.
        r (bool): Apply freeze rotate.
        s (bool): Apply freeze scale.

    Returns:
        prc.Joint:
    """
    jnt = prc.Joint()
    if prc.Dag in type(dag).__mro__:
        jnt.snap(dag)
        jnt.freeze(**kwargs)
        jnt.attr('rotateOrder').v = dag.attr('rotateOrder').v
    elif prc.Component in type(dag).__mro__:
        jnt.world_pos = dag.world_pos

    return jnt


def create_joint_and_its_zero_at(dag):
    """Creates a joint and its zero group at the position of
    the given transform.

    Args:
        dag (prc.Dag):

    Returns:
        prc.Joint:
        prc.Dag: A zero group.
    """
    jnt = prc.Joint()
    zr = prc.group(jnt)
    zr.snap(dag)

    return jnt, zr


def create_local_skin_joint_at(dag):
    """Creates a local skin joint at the position of the given DAG object.
    Local Skin Joint is a joint that operates with a skin cluster at the origin.

    Creates a joint and connects xform attrbutes from the given DAG
    to the created joint.

    Args:
        dag (prc.Dag):

    Returns:
        prc.Joint:
    """
    jnt = prc.Joint()
    jnt.snap(dag)
    jnt.freeze()
    if mc.nodeType(dag.name) == 'joint':
        dag.attr('rotateOrder') >> jnt.attr('rotateOrder')
        dag.attr('ssc') >> jnt.attr('ssc')

    dag.attr('t') >> jnt.attr('t')
    dag.attr('r') >> jnt.attr('r')
    dag.attr('s') >> jnt.attr('s')

    return jnt


def accumulate_xform_matrices(hierarchy, attr_prefix):
    """Uses multMatrix to accumulate transformation matrices by the order of
    the given hierarchy(from child to parent).
    Translate, rotate and scale attrbutes are added with the given
    attr_prefix to the first node in the given hierarchy.

    Args:
        hierarchy (tuple): An order of prc.Dag from child to parent.
        attr_prefix (str): Prefix of output transform attributes.
            attr_prefix is set to 'local' if it has not been defined.

    Returns:
        prc.Node: A multMatrix node.
        prc.Node: A decomposeMatrix node.
    """
    multmat = prc.create('multMatrix')
    decomp = prc.create('decomposeMatrix')

    # Accumulates local matrices.
    for node in hierarchy:
        node.attr('matrix') >> multmat.attr('matrixIn').last()

    multmat.attr('matrixSum') >> decomp.attr('inputMatrix')
    hierarchy[0].attr('rotateOrder') >> decomp.attr('inputRotateOrder')

    # Sets to 'local' if attr_prefix has not been defined.
    if not attr_prefix:
        attr_prefix = 'local'

    # Adds output translate, rotate and scale attribute.
    for attr in ('Translate', 'Rotate', 'Scale'):
        currattr = '{pf}{attr}'.format(pf=attr_prefix, attr=attr)
        if hierarchy[0].attr(currattr).exists:
            # Skip, if the attribute has been added.
            continue

        # Adding output attrs to the first node in hierarchy.
        hierarchy[0].add(longName=currattr, attributeType='double3')
        for axis in ('X', 'Y', 'Z'):
            hierarchy[0].add(
                longName='{attr}{ax}'.format(attr=currattr, ax=axis),
                attributeType='double',
                parent=currattr
            )

        decompout = 'output{}'.format(attr)
        decomp.attr(decompout) >> hierarchy[0].attr(currattr)

    return multmat, decomp


def read_nurbs_curve(dag):
    """Generates data from a NURBs curve.

    Args:
        dag (prc.Dag):

    Returns:
        dict: A dictionary of curve's degree, form, knots and cv positions.
    """
    dagpath = prc.dag_path_from_dag_str(dag.name)
    curvefn = om.MFnNurbsCurve(dagpath)

    data = dict()
    data['degree'] = int()
    data['form'] = int()
    data['spans'] = int()
    data['knots'] = list()
    data['cvs'] = list()

    data['degree'] = int(curvefn.degree())
    data['form'] = int(curvefn.form()) - 1
    data['spans'] = int(curvefn.numSpans())

    knots = om.MDoubleArray()
    curvefn.getKnots(knots)
    data['knots'] = [int(ix) for ix in knots]

    cvs = om.MPointArray()
    curvefn.getCVs(cvs, om.MSpace.kObject)

    data['cvs'] = [
        (cvs[ix].x, cvs[ix].y, cvs[ix].z) for ix in range(cvs.length())
    ]

    data['num_knots'] = curvefn.numKnots()

    return data


def build_curve_from_data(curve_data, dag):
    """Uses curve cache attribute to build a NURBs curve shape
    from the data given from read_nurbs_curve
    and attache to the given xform.

    Args:
        curve_data (dict): Data given from read_nurbs_curve
        dag (prc.Dag):

    Returns:
        prc.Dag: The created shape.
    """
    shape = prc.create(
        'nurbsCurve', n='{n}Shape'.format(n=dag.name), p=dag)

    values = [
        '{s}.cc'.format(s=shape),
        curve_data['degree'],
        curve_data['spans'],
        curve_data['form'],
        False, 3,
        curve_data['knots'],
        curve_data['num_knots'],
        len(curve_data['cvs'])
    ]
    values += curve_data['cvs']
    mc.setAttr(*values, type='nurbsCurve')

    return shape


def add_zero_group(dag):
    """Creates a zero group to the given dag node.

    Args:
        dag (prc.Dag):

    Returns:
        prc.Dag: The created zero group.
    """
    parent = dag.get_parent()

    zr = prc.Null()
    zr.snap(dag)
    dag.set_parent(zr)

    if parent:
        zr.set_parent(parent)

    return zr


def add_gimbal_helper(controller):
    """Adds gimbal helper to the given controller.

    Args:
        controller (prc.Dag):

    Returns:
        prc.Dag: A gimbal helper.
    """
    gimbal = prc.Null()
    curvedata = read_nurbs_curve(controller)
    build_curve_from_data(curvedata, gimbal)

    gimbal.snap(controller)
    gimbal.set_parent(controller)
    gimbal.lhattrs('s', 'v')
    gimbal.color = prc.Color.white
    controller.attr('ro') >> gimbal.attr('ro')
    gimbal.scale_shape(0.8)

    controllershape = controller.get_shape()
    if not controllershape.attr('gimbalHelper').exists:
        controllershape.add(ln='gimbalHelper', k=True, min=0, max=1)

    controllershape.attr('gimbalHelper') >> gimbal.get_shape().attr('v')

    return gimbal


def add_con_controller(controller):
    """Con Controller is used to be constrained to the target.

    Args:
        controller (prc.Dag):

    Returns:
        prc.Dag: A con controller.
    """
    con = prc.Null()
    curvedata = read_nurbs_curve(controller)
    build_curve_from_data(curvedata, con)

    con.snap(controller)
    controller.set_parent(con)
    con.lhattrs('s', 'v')
    con.color = prc.Color.brown
    controller.attr('ro') >> con.attr('ro')
    con.scale_shape(1.2)

    controllershape = controller.get_shape()
    if not controllershape.attr('conCtrl').exists:
        controllershape.add(ln='conCtrl', k=True, min=0, max=1)

    controllershape.attr('conCtrl') >> con.get_shape().attr('v')

    return con


def dup_and_clean_unused_intermediate_shape(xform):
    """Duplicates a given xform and remove
    all related unused intermediate shapes.

    Args:
        xform (prc.Dag): A transform node.

    Returns:
        prc.Dag: A duplicated transform node.
    """
    dupped = prc.to_pkrig_node(mc.duplicate(xform, rr=True)[0])
    shapes = dupped.get_all_shapes()

    for attr in ('t', 'r', 's', 'v'):
        dupped.attr(attr).lock = False
        dupped.attr(attr).hide = False

    if not shapes:
        return dupped

    for shape in shapes:
        if shape.attr('intermediateObject').v:
            mc.delete(shape)

    return dupped


def tr_blend(dag1, dag2, blended_dag, driver):
    """Adds translate and rotate blend using a pairBlend node.

    Args:
        dag1 (prc.Dag): The first target.
        dag2 (prc.Dag): The second target.
        blended_dag (prc.Dag):
        driver (prc.Attribute):
    
    Returns:
        prc.Node: A pairBlend node.
    """
    blend = prc.create('pairBlend')
    blend.attr('rotInterpolation').v = 0

    driver >> blend.attr('weight')
    dag1.attr('t') >> blend.attr('inTranslate1')
    dag1.attr('r') >> blend.attr('inRotate1')
    dag2.attr('t') >> blend.attr('inTranslate2')
    dag2.attr('r') >> blend.attr('inRotate2')

    blend.attr('outTranslate') >> blended_dag.attr('t')
    blend.attr('outRotate') >> blended_dag.attr('r')
    
    return blend

def s_blend(dag1, dag2, blended_dag, driver):
    """Adds scale blend using a blendColors node.

    Args:
        dag1 (prc.Dag): The first target.
        dag2 (prc.Dag): The second target.
        blended_dag (prc.Dag):
        driver (prc.Attribute):
    
    Returns:
        prc.Node: A blendColors node.
    """
    blend = prc.create('blendColors')

    driver >> blend.attr('blender')
    dag1.attr('s') >> blend.attr('color1')
    dag2.attr('s') >> blend.attr('color2')
    blend.attr('output') >> blended_dag.attr('s')
    
    return blend


def get_all_mesh_xforms():
    """
    Returns:
        list: A list of geometry transform nodes.
    """
    meshes = mc.ls(type='mesh', l=True)
    results = []

    if not meshes:
        return results

    results = []
    for mesh in meshes:

        pars = mc.listRelatives(mesh, p=True, f=True)

        if mc.referenceQuery(pars[0], isNodeReferenced=True):
            continue

        if pars[0] in results:
            continue
    
        results.append(pars[0])

    return results


def scale_controller(vals, scale_node, controller):
    """Sets scale of the scale_node as the given vals and retain
    the shape of the given controller.

    Args:
        vals (tuple): A tuple of scaleX, scaleY and scaleZ.
        scale_node (prc.Dag):
        controller (prc.Dag):
    """
    compoes = prc.get_all_components(controller.name)
    poses = []
    for compo in compoes:
        poses.append(mc.xform(compo, q=True, ws=True, t=True))
    
    chdn = controller.get_all_children()
    if chdn:
        for chd in chdn:
            chd.set_parent(None)

    scale_node.attr('s').v = vals

    for compo, pos in zip(compoes, poses):
        mc.xform(compo, ws=True, t=pos)
    
    if chdn:
        for chd in chdn:
            chd.set_parent(controller)
