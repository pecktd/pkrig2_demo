# System Modules
from imp import reload

# Maya Modules
import maya.OpenMaya as om
import maya.OpenMayaAnim as oma
import maya.cmds as mc

# pkrig Modules
from . import core as prc
from . import utils as pru
from . import spline_weight

reload(prc)
reload(pru)


def get_inf_index(skin, joint):
    """Gets the index of the given joint of the given skin cluster table.

    Args:
        skin (oma.MFnSkinCluster):
        joint (prc.Dag):

    Returns:
        int/None: An influence index if it is found else None.
    """
    dagary = om.MDagPathArray()
    skin.influenceObjects(dagary)

    infid = None
    for ix in range(dagary.length()):
        curr_fullname = dagary[ix].fullPathName()
        if curr_fullname == joint.m_dag_path.fullPathName():
            infid = ix

    return infid


def get_affected_components(geo, joint):
    """Gets affected vertices of the give joint.

    Args:
        geo (prc.Dag):
        joint (prc.Dag):

    Returns:
        list: A list of component names.
    """
    skin = get_related_skin_mfn(geo)
    mc.select(cl=True)
    mc.skinCluster(skin.name(), e=True, siv=joint.m_dag_path.fullPathName())

    return mc.ls(sl=True, fl=True)


def copy_weight(from_this, to_these):
    """Copies skin weights from 'from_this' to 'to_this' node.

    Args:
        from_this (prc.Dag):
        to_this (list): A list of prc.Dag nodes.

    Returns:
        list: A list of skinCluster names.
    """
    infs = mc.skinCluster(from_this.name, q=True, inf=True)

    skins = []
    for dag in to_these:
        skinname = mc.skinCluster(infs, dag.name, tsb=True)[0]

        mc.select((from_this.name, dag.name), r=True)
        mc.copySkinWeights(
            noMirror=True,
            surfaceAssociation="closestPoint",
            influenceAssociation="closestJoint",
        )

        skins.append(prc.Node(skinname))

    return skins


def copy_compo_weight(from_this, to_these):
    """Copies the skin weight values from the given 'from_this' component
    to the given 'to_these' components.
    All the shapes have to bound to the similar order of the influent nodes.
    The script only sets the weightList attribute of the 'to_these' components.

    Args:
        from_this (str):
        to_these (list): A list of component names.
    """
    cid = prc.get_compo_index(from_this)
    dag = prc.Dag(from_this.split(".")[0])
    dagpath = prc.dag_path_from_dag_str(from_this)
    skinfn = get_related_skin_mfn(dag)
    infdags = om.MDagPathArray()
    skinfn.influenceObjects(infdags)
    infno = infdags.length()
    scutil = om.MScriptUtil(infno)
    infptr = scutil.asUintPtr()
    weights = om.MDoubleArray()
    skinfn.getWeights(dagpath, om.MObject(), weights, infptr)
    wlists = [
        weights[ix : ix + infno] for ix in range(0, weights.length(), infno)
    ]

    for compo in to_these:
        currid = prc.get_compo_index(compo)
        currdag = prc.Dag(compo.split(".")[0])
        currskinfn = get_related_skin_mfn(currdag)
        currskin = prc.Node(currskinfn.name())
        wlattr = "weightList[{}].weights[0:{}]".format(currid, infno - 1)
        mc.setAttr("{}.{}".format(currskin, wlattr), *wlists[cid])


def move_weight(compo_str, joints_weights, source_joint):
    """Moves weight values of the given compo_str from the given source_joint
    to the given joint with multiply to the given weight.

    Args:
        compo_str (str):
        joints_weights (dict): {prc.Dag: float} Ex. {joint1: 0.5, joint2: 0.5}
        source_joint (prc.Dag):
    """
    geo = prc.Dag(compo_str.split(".")[0])
    skin = get_related_skin_mfn(geo)

    srcidx = get_inf_index(skin, source_joint)

    skinweights = mc.skinPercent(skin.name(), compo_str, q=True, v=True)
    val = skinweights[srcidx]

    for joint, weight in joints_weights.iteritems():
        curr_movedval = val * float(weight)
        mc.skinPercent(
            skin.name(), compo_str, tv=[(joint.name, curr_movedval)]
        )


def spline_weight_divide(geo, source_joint, target_joints, start_pos, end_pos):
    """Using cubic spline weight function to distribute weights from
    the source_joint to the given target_joints.
    Args:
        geo (prc.Dag):
        source_joint (prc.Dag):
        target_joints (list):
        start_pos (tuple): A reference vector.
        end_pos (tuple): A reference position.
    """
    start_pos = om.MVector(*start_pos)
    end_pos = om.MVector(*end_pos)
    refvec = end_pos - start_pos
    current_len = refvec.length()
    refvec.normalize()

    compoes = get_affected_components(geo, source_joint)
    for compostr in compoes:
        compo = prc.Component(compostr)
        compovec = om.MVector(*compo.world_pos) - start_pos
        currdot = compovec * refvec

        param = currdot / current_len
        if param < 0:
            param = 0
        if param > 1:
            param = 1

        weights = spline_weight.point_on_curve_weights(target_joints, param, 3)
        joints_weights = dict(weights)
        move_weight(compostr, joints_weights, source_joint)


def weight_mask(mask_jnt, mask_geo, geo, base_jnt):
    """Uses weight values of the given mask_jnt on mask_geo as a weight mask.
    Apply (1 - mask weight value) to all weight values of the given base_jnt
    that influences to the given geo.

    Args:
        mask_jnt (prc.Dag):
        mask_geo (prc.Dag):
        geo (prc.Dag):
        base_jnt (prc.Dag):

    Returns:
        None
    """
    mask_weight_data = get_skin_data_from_bound_object(mask_geo)
    mask_inf_id = mask_weight_data["influences"].index(mask_jnt.name)
    mask_weights = [wv[mask_inf_id] for wv in mask_weight_data["weights"]]

    skinfn = get_related_skin_mfn(geo)
    for ix, vtx in enumerate(prc.get_all_components(geo.name)):
        mc.skinPercent(
            skinfn.name(),
            vtx,
            transformValue=[(base_jnt.name, 1 - mask_weights[ix])],
        )


def direct_copy_influence_weights(
    source_jnt, source_geo, target_jnt, target_geo, multiplier
):
    """Copies weight values of the given source_jnt on source_geo

    Args:
        source_jnt (prc.Dag):
        source_geo (prc.Dag):
        target_jnt (prc.Dag):
        target_geo (prc.Dag):
        multiplier (float):

    Returns:
        None
    """
    source_weight_data = get_skin_data_from_bound_object(source_geo)
    source_inf_id = source_weight_data["influences"].index(source_jnt.name)
    source_weights = [
        wv[source_inf_id] for wv in source_weight_data["weights"]
    ]

    skinfn = get_related_skin_mfn(target_geo)
    for ix, vtx in enumerate(prc.get_all_components(target_geo.name)):
        mc.skinPercent(
            skinfn.name(),
            vtx,
            transformValue=[
                (target_jnt.name, source_weights[ix] * multiplier)
            ],
        )


def get_related_skin_mfn(dag):
    """Gets a skin cluster(oma.MFnSkinCluster) that is connected
    to the given prc.Dag.

    Args:
        dag (prc.Dag):

    Returns:
        oma.MFnSkinCluster:
    """
    histories = mc.listHistory(dag.get_shape().name)
    for his in histories:
        if mc.nodeType(his) == "skinCluster":
            mobj = prc.mobject_from_str(his)
            return oma.MFnSkinCluster(mobj)


def get_skin_data_from_bound_object(dag):
    """Gets skinning data from given DAG node.

    Args:
        dag(prc.Dag): A DAG node.

    Returns:
        A dictionary in a format of
        {
            'name': A name(str) of skin cluster node.,
            'method': A skinning method(str).,
            'influences': A list of names(str) of influences.,
            'weights': A list of skin weight values(float).,
            'blendWeights': A list of blend weight values(float).
        }
    """
    skinfunc = get_related_skin_mfn(dag)

    method = mc.getAttr("{}.skinningMethod".format(skinfunc.name()))
    compoes = prc.get_all_components(dag.name)
    infs = mc.skinCluster(skinfunc.name(), q=True, inf=True)
    weights = [
        mc.skinPercent(skinfunc.name(), compo, q=True, v=True)
        for compo in compoes
    ]
    blendweights = [
        mc.getAttr("{s}.blendWeights[{i}]".format(s=skinfunc.name(), i=ix))
        for ix in range(len(compoes))
    ]
    if not any(blendweights):
        blendweights = None

    return {
        "name": skinfunc.name(),
        "method": method,
        "influences": infs,
        "weights": weights,
        "blendWeights": blendweights,
    }


def get_weights_of(geo, joint):
    """Get weight values of the given joint to all components of the given geo.

    Args:
        geo (prc.Dag):
        joint (prc.Dag):

    Returns:
        list : Weight values of the given joint to all components.
    """
    skinfunc = get_related_skin_mfn(geo)
    infs = mc.skinCluster(skinfunc.name(), q=True, inf=True)
    infid = infs.index(joint.name)
    weights = []
    for compo in prc.get_all_components(geo.name):
        weights.append(
            mc.skinPercent(skinfunc.name(), compo, q=True, v=True)[infid]
        )

    return weights


def bind_skin_data_to(skin_data, dag):
    """Binds the given skin data to the given DAG node.

    Args:
        skin_data (dict): A skin data retrieved from
            get_skin_data_from_bound_object
        dag (prc.Dag):

    Returns:
        A created skin cluster node(prc.Node)
    """
    oldskin = get_related_skin_mfn(dag)
    if oldskin:
        mc.skinCluster(oldskin.name(), e=True, ub=True)
    skin = prc.Node(mc.skinCluster(skin_data["influences"], dag, tsb=True)[0])
    skin.attr("skinningMethod").v = skin_data["method"]

    # Unlocks all influence joints.
    for inf in skin_data["influences"]:
        currinf = prc.Dag(inf)
        currinf.attr("liw").v = False

    skin.attr("normalizeWeights").v = False
    mc.skinPercent(skin.name, dag.name, nrm=False, prw=100)
    skin.attr("normalizeWeights").v = True

    for ix in range(len(skin_data["weights"])):
        for iy in range(len(skin_data["influences"])):
            weightval = skin_data["weights"][ix][iy]
            if not weightval:
                continue
            wlattr = "weightList[{ix}].weights[{iy}]".format(ix=ix, iy=iy)
            skin.attr(wlattr).v = weightval
            mc.setAttr("{n}.{a}".format(n=skin.name, a=wlattr), weightval)

            if not skin_data["blendWeights"]:
                continue
            bwattr = "blendWeights[{}]".format(ix)
            bwvals = skin_data["blendWeights"][ix]
            mc.setAttr("{n}.{a}".format(n=skin.name, a=bwattr), bwvals)

    return skin


def set_skin_data_to(skin_data, dag):
    """Binds the given skin data to the given DAG node.

    Args:
        skin_data (dict): A skin data retrieved from
            get_skin_data_from_bound_object
        dag (prc.Dag):

    Returns:
        A created skin cluster node(prc.Node)
    """
    skin = prc.Node(get_related_skin_mfn(dag).name())
    skin.attr("skinningMethod").v = skin_data["method"]

    # Unlocks all influence joints.
    for inf in skin_data["influences"]:
        currinf = prc.Dag(inf)
        currinf.attr("liw").v = False

    skin.attr("normalizeWeights").v = False
    mc.skinPercent(skin.name, dag.name, nrm=False, prw=100)
    skin.attr("normalizeWeights").v = True

    for ix in range(len(skin_data["weights"])):
        for iy in range(len(skin_data["influences"])):
            weightval = skin_data["weights"][ix][iy]
            if not weightval:
                continue
            wlattr = "weightList[{ix}].weights[{iy}]".format(ix=ix, iy=iy)
            skin.attr(wlattr).v = weightval
            mc.setAttr("{n}.{a}".format(n=skin.name, a=wlattr), weightval)

            if not skin_data["blendWeights"]:
                continue

            bwattr = "blendWeights[{}]".format(ix)
            bwvals = skin_data["blendWeights"][ix]
            mc.setAttr("{n}.{a}".format(n=skin.name, a=bwattr), bwvals)

    return skin


def rebuild_list_from_object_positions(dag_names, target_dag_names):
    """Builds a new list from the members of the given 'dag_names'
    that have the same position as DAG nodes in 'dag_names'

    If any of DAG node in 'target_dag_names' doesn't have a matched DAG,
    raises an error.

    This functions is used to rebuild a list of influent joints in 'dag_names'
    that have the same position as joints in 'target_dag_names'.

    Args:
        dag_names (list): A list of DAG nodes(str) to be selected
            to the return list if the position matches with any in
            target_dag_names.
        target_dag_names (list): A list of DAG nodes(str)
            as the referent positions.

    Returns:
        A list of DAG(str) nodes or None.
    """
    results = list()

    for target_dag_name in target_dag_names:
        foundmatch = False
        target_dag = prc.Dag(target_dag_name)
        for dag_name in dag_names:
            dag = prc.Dag(dag_name)
            if pru.do_positions_match(dag, target_dag):
                results.append(dag.name)
                foundmatch = True
                break
        if not foundmatch:
            raise ValueError(
                "There is no match object for {}".format(target_dag_name)
            )

    return results


def bind_skin_data_with_new_influences_to(skin_data, geo, new_influences):
    """Binds the given skin_data to the given geo.
    Replaces the influence list in the skin_data with the given new_influences.

    A number of members in skin_data['influences'] and new_influences need to
    match.
    Matching position is used to pair the order of incluencers.
    If the matchin process is not finished, skin_data is not bound to the geo.

    Args:
        skin_data (dict): A skin_data returned from
            get_skin_data_from_bound_object
        geo (prc.Dag):
        new_influences (list): A list of DAG(str) objects.

    Returns:
        A created skin cluster node(prc.Node)
    """
    matchedinfs = rebuild_list_from_object_positions(
        new_influences, skin_data["influences"]
    )
    skin_data["influences"] = matchedinfs
    skin_data["matrix"] = {i: jnt for i, jnt in enumerate(matchedinfs)}

    return bind_skin_data_to(skin_data, geo)


def connect_parent_inverse_matrix(dag, jnt, skin):
    infs = mc.skinCluster(skin.name, q=True, inf=True)
    idx = infs.index(jnt.name)

    dag.attr("parentInverseMatrix") >> skin.attr(
        "bindPreMatrix[{}]".format(idx)
    )


def copy_weight_from_curve_to_plane(curve, plane):
    """Copies skin weight from the given curve to the given lip poly plane.

    Args:
        curv (prc.Dag):
        plane (prc.Dag):
    """
    curve_shape = curve.get_shape()
    plane_shape = plane.get_shape()

    curve_dag = prc.dag_path_from_dag_str(curve_shape)

    curve_fn = om.MFnNurbsCurve(curve_dag)
    cvs = om.MPointArray()
    curve_fn.getCVs(cvs)

    # Finds middle vertices and connected vertices.
    mesh_dag, mesh_compo = prc.dag_path_and_mobject_from_dag_str(plane_shape)

    # A dict contains vtx IDs that positions match with CVs as keys,
    # match CV as a value in 'cv' and connected vertex IDs as values
    # in 'connected.
    matchvtx_dict = {}
    convtcs = om.MIntArray()
    vtxiter = om.MItMeshVertex(mesh_dag, mesh_compo)
    while not vtxiter.isDone():
        vtxiter.getConnectedVertices(convtcs)
        for ix in range(cvs.length()):
            if pru.do_vectors_match(vtxiter.position(), cvs[ix]):
                matchvtx_dict[vtxiter.index()] = {
                    "cv": ix,
                    "connected": tuple(convtcs),
                }
                break

        vtxiter.next()

    crv_skinfn = get_related_skin_mfn(curve)

    # Gets curve influences.
    infs = mc.skinCluster(curve, q=True, inf=True)
    inf_count_util = om.MScriptUtil(len(infs))
    inf_count_ptr = inf_count_util.asUintPtr()
    influence_indices = om.MIntArray()
    for ix in range(len(infs)):
        influence_indices.append(ix)

    # Gets curve weights.
    curve_weights = om.MDoubleArray()
    crv_skinfn.getWeights(
        curve_dag, om.MObject(), curve_weights, inf_count_ptr
    )

    mc.skinCluster(infs, plane, tsb=True)
    for vtx_mid_id in matchvtx_dict.keys():
        # Gets plane vertex IDs those aren't in the middle vertices.
        con_ids = list(
            set(matchvtx_dict[vtx_mid_id]["connected"]).difference(
                matchvtx_dict.keys()
            )
        )
        con_ids.append(vtx_mid_id)

        # Copies weights from the match CV to middle and connected vertices.
        cvname = "{}.cv[{}]".format(curve, matchvtx_dict[vtx_mid_id]["cv"])
        vtxnames = []
        for conid in con_ids:
            vtxnames.append("{}.vtx[{}]".format(plane, conid))

        copy_compo_weight(cvname, vtxnames)


def copy_weight_from_selected_loops_to_connected_loops():
    """Copies skin weights from the selected vertex loop
    to the connected loop, horizontal loop, of each vertex.
    """

    def _get_id(compo):
        """Gets component an ID as as int."""
        return int(compo.split("[")[1].split("]")[0])

    _plcc = mc.polyListComponentConversion

    # Finds selected vertex loop.
    sel_vtxloop = mc.ls(sl=True, fl=True)
    if ".e[" in sel_vtxloop[0]:
        sel_vtxloop = mc.ls(_plcc(fe=True, tv=True))

    # Finds edges that are connected to the edge loop.
    xform = prc.Dag(sel_vtxloop[0].split(".")[0])
    mid_edges = mc.ls(_plcc(sel_vtxloop, te=True, internal=True), fl=True)
    mid_edges_id = mc.polySelect(xform, elb=_get_id(mid_edges[0]), q=True)
    mid_vtcs = mc.ls(_plcc(mid_edges, fe=True, tv=True), fl=True)
    con_midedges = mc.ls(_plcc(mid_vtcs, fv=True, te=True), fl=True)
    mid_edges_loop = []
    for edge in mid_edges_id:
        edgename = "{o}.e[{eid}]".format(o=xform, eid=edge)
        mid_edges_loop.append(edgename)

    # A dictionary contains middle vertices as keys and connected edges,
    # horizontal edges, are values.
    v2e = {}
    for edge in con_midedges:
        if edge in mid_edges_loop:
            continue
        curr_vtcs = mc.ls(_plcc(edge, fe=True, tv=True), fl=True)
        for vtx in curr_vtcs:
            if vtx in v2e.keys():
                continue
            if vtx in mid_vtcs:
                v2e[vtx] = edge

    # Finds horizontal vtx loop and set weights.
    for vtx in v2e:
        edgeloop = mc.polySelect(xform, el=_get_id(v2e[vtx]), q=True)
        if len(edgeloop) == 1:
            edgeloop = mc.polySelect(xform, elb=_get_id(v2e[vtx]), q=True)

        horedges = []
        for edge in edgeloop:
            edgename = "{o}.e[{eid}]".format(o=xform, eid=edge)
            horedges.append(edgename)

        vtcs = mc.ls(_plcc(horedges, fe=True, tv=True), fl=True)
        copy_compo_weight(vtx, vtcs)
