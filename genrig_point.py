# Maya Modules
from imp import reload
import maya.cmds as mc

# pkrig Modules
from . import core as prc
from . import utils as pru
from . import naming_tools as prnt
from . import rig_base

reload(rig_base)


class PointRig(rig_base.BaseRig):

    def __init__(
            self,
            tmpjnt,
            index,
            side,
            mod,
            desc,
            ctrl_shape,
            constrain_func,
            parents_attrnames,
            ctrl_grp_parent,
            skin_parent
    ):
        """A generic Point Rig module.

        Args:
            tmpjnt (str):
            index (int):
            side (prc.Side):
            mod (str):
            desc (str):
            ctrl_shape (prc.Cp):
            constrain_func (prc.point_constraint/prc.orient_constraint/
                prc.parent_constraint): A constrain function that is used by
                space blender.
            parents_attrnames (list/None): A list of dictionaries that keys are
                parent nodes(prc.Dag) and values are attribute names.
                E.g., [{root_rig.root_jnt: 'root'},{neck_rig.neck_jnt: 'neck'}]
            ctrl_grp_parent (prc.Dag):
            skin_parent (prc.Dag):
        """
        # Casts args to pkrig objects.
        jnt = prc.Dag(tmpjnt)

        # Main Groups
        super(PointRig, self).__init__(None, mod, desc, False)

        self.meta.name = prnt.compose(
            '{m}{d}'.format(m=self.mod_name, d=self.desc), None, side, 'Grp')
        self.meta.snap(jnt)
        if ctrl_grp_parent:
            self.meta.set_parent(ctrl_grp_parent)

        self.jnt = self.init_dag(
            pru.create_joint_at(jnt), '', index, side, 'Jnt')
        self.jnt.attr('ssc').v = False

        if skin_parent:
            prc.parent_constraint(skin_parent, self.meta, mo=True)
            prc.scale_constraint(skin_parent, self.meta, mo=True)
            self.jnt.set_parent(skin_parent)

        self.ctrl = self.init_dag(
            prc.Controller(ctrl_shape), '', index, side, 'Ctrl')
        self.ctrl.lhattrs('v')
        self.ctrl.set_default_color(0)

        self.gmbl = self.init_dag(
            pru.add_gimbal_helper(self.ctrl), '', index, side, 'Gmbl')
        self.ctrl_ofst = self.init_dag(
            prc.group(self.ctrl), 'Ctrl', index, side, 'Ofst')
        self.ctrl_ori = self.init_dag(
            prc.group(self.ctrl_ofst), 'Ctrl', index, side, 'Ori')
        self.ctrl_zr = self.init_dag(
            prc.group(self.ctrl_ori), 'Ctrl', index, side, 'Zr')
        self.ctrl_zr.snap(self.jnt)
        self.ctrl_zr.set_parent(self.meta)

        prc.parent_constraint(self.ctrl, self.jnt)
        self.ctrl.attr('s') >> self.jnt.attr('s')

        # Adds space blend.
        if parents_attrnames:
            self.add_space_blender(
                parents_attrnames, constrain_func, self.ctrl_zr, self.ctrl,
                self.meta)

        self.skin_jnts = [self.jnt]
