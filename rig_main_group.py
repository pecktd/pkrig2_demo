# pkrig modules
from . import core as prc
from . import naming_tools as prnt
from . import rig_base


class MainGroup(rig_base.BaseRig):
    
    def __init__(self, asset_name):
        """Main groups for all asset.

        Args:
            asset_name (str):

        Attributes:
            place_ctrl (prc.Dag):
            offset_ctrl (prc.Dag):
            ctrl_grp (prc.Dag):
            skin_grp (prc.Dag):
            still_grp (prc.Dag):
        """
        super(MainGroup, self).__init__(None, '', '', False)

        if not asset_name:
            # Uses 'Asset' if the asset_name has not been defined.
            asset_name = 'Asset'
        self.meta.name = prnt.compose(asset_name, None, None, 'Grp')

        self.place_ctrl = self.init_dag(
            prc.Controller(prc.CurveParam.cross), 'Place', None,
            None, 'Ctrl')
        self.place_ctrl.lhattrs('v')
        self.place_ctrl.color = prc.Color.yellow
        self.place_ctrl.set_parent(self.meta)

        self.offset_ctrl = self.init_dag(
            prc.Controller(prc.CurveParam.circle), 'Offset', None,
            None, 'Ctrl')
        self.offset_ctrl.lhattrs('s', 'v')
        self.offset_ctrl.color = prc.Color.yellow
        self.offset_ctrl.set_parent(self.place_ctrl)

        self.ctrl_grp = self.init_dag(
            prc.Null(), 'Ctrl', None, None, 'Grp')
        self.ctrl_grp.set_parent(self.offset_ctrl)
        self.ctrl_grp.lhtrs()

        self.skin_grp = self.init_dag(
            prc.Null(), 'Skin', None, None, 'Grp')
        self.skin_grp.set_parent(self.offset_ctrl)
        self.skin_grp.lhtrs()

        self.still_grp = self.init_dag(
            prc.Null(), 'Still', None, None, 'Grp')
        self.still_grp.set_parent(self.meta)
        self.still_grp.lhtrs()

        self.skin_set = self.init_node(
            prc.create('objectSet'), 'Skin', None, None, 'Set')

        # Sets default values
        self.still_grp.attr('v').v = False
