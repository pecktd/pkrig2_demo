import time
from copy import deepcopy

# Maya Modules
import maya.OpenMaya as om
import maya.cmds as mc

# pkrig Modules
from . import core as prc


def get_all_vertices(dag_path):
    """Gets all mesh's vertex names.

    Args:
        dag_path (om.MDagPath): An MDagPath of the shape node.
    
    Returns:
        tuple: A tuple of vertex names.
    """
    meshfn = om.MFnMesh(dag_path)
    compoes = '{dp}.vtx[0:{vtx}]'.format(
        dp=dag_path.fullPathName(), vtx=meshfn.numVertices() - 1)

    return tuple(mc.ls(compoes, fl=True))


def get_all_surface_cvs(dag_path):
    """Gets all NURBs' CV names.

    Args:
        dag_path (om.MDagPath): An MDagPath of the shape node.
    
    Returns:
        tuple: A tuple of CV names.
    """
    nurbsfn = om.MFnNurbsSurface(dag_path)
    spans_u = nurbsfn.numSpansInU()
    degree_u = nurbsfn.degreeU()
    spans_v = nurbsfn.numSpansInV()
    degree_v = nurbsfn.degreeV()
    form_u = nurbsfn.formInU()
    form_v = nurbsfn.formInV()
    num_u = spans_u + degree_u

    if form_u == 3:
        num_u -= degree_u

    num_v = spans_v + degree_v

    if form_v == 3:
        num_v -= degree_v

    compoes = '{dp}.cv[0:{u}][0:{v}]'.format(
        dp=dag_path.fullPathName(), u=num_u - 1, v=num_v - 1)

    return tuple(mc.ls(compoes, fl=True))


def get_all_curve_cvs(dag_path):
    """Gets all curve's CV names.

    Args:
        dag_path (om.MDagPath): An MDagPath of the shape node.
    
    Returns:
        tuple: A tuple of CV names.
    """
    curvefn = om.MFnNurbsCurve(dag_path)
    spans = curvefn.numSpans()
    degree = curvefn.degree()
    form = curvefn.form()

    num_cvs = spans + degree
    if form == 3:
        num_cvs -= degree

    compoes = '{dp}.cv[0:{cv}]'.format(
        dp=dag_path.fullPathName(), cv=num_cvs - 1)

    return tuple(mc.ls(compoes, fl=True))


def get_all_point_names(dag):
    """Gets all vertex/cv names of the given dag.

    Args:
        dag (prc.Dag):

    Returns:
        tuple: vertex/cv names.
    """
    dagpath = dag.m_dag_path
    dagpath.extendToShape()
    # Mesh
    if dagpath.hasFn(om.MFn.kMesh):
        return get_all_vertices(dagpath)
    # NURBs Curve
    elif dagpath.hasFn(om.MFn.kNurbsCurve):
        return get_all_curve_cvs(dagpath)
    # NURBs Surface
    elif dagpath.hasFn(om.MFn.kNurbsSurface):
        return get_all_surface_cvs(dagpath)


def point_array_to_tuple(point_array):
    """Converts the given point_array to a tuple of 3 values tuples.

    Args:
        point_array (om.MPointArray):

    Returns:
        tuple: A tuple of three members tuples.
    """
    pnts = []
    for ix in range(point_array.length()):
        vctr = om.MVector(point_array[ix])
        pnts.append((vctr.x, vctr.y, vctr.z))

    return tuple(pnts)


def get_all_point_positions(dag):
    """Gets all point positions of the given dag.
    Args:
        dag (prc.Dag):
    
    Returns:
        tuple: A tuple of positions.
    """
    dagpath = dag.m_dag_path
    dagpath.extendToShape()
    pntary = om.MPointArray()
    # Mesh
    if dagpath.hasFn(om.MFn.kMesh):
        meshfn = om.MFnMesh(dagpath)
        meshfn.getPoints(pntary, om.MSpace.kObject)
    # NURBs Curve
    elif dagpath.hasFn(om.MFn.kNurbsCurve):
        curvefn = om.MFnNurbsCurve(dagpath)
        curvefn.getCVs(pntary, om.MSpace.kObject)
    # NURBs Surface
    elif dagpath.hasFn(om.MFn.kNurbsSurface):
        # Temporary solution for querying surface's CV positions.
        return tuple([
            tuple(mc.xform(cv, q=True, os=True, t=True)) 
            for cv in get_all_point_names(dag)
        ])

    return point_array_to_tuple(pntary)


def set_point_positions(dag, positions):
    """Sets all point positions of the given dag.
    Args:
        dag (prc.Dag):
    """
    pntnames = get_all_point_names(dag)
    for pntname, pos in zip(pntnames, positions):
        mc.xform(pntname, os=True, t=pos)


def copy_shape(from_this, to_this):
    """Matches all point positions of "to_this" to "from_this".

    Args:
        from_this (prc.Dag):
        to_this (prc.Dag):
    """
    set_point_positions(to_this, get_all_point_positions(from_this))


def copy_selected_shapes():
    """Matches all point positions of the first selection to the second
    selection.
    """
    sels = mc.ls(sl=True)
    fromthis = prc.Dag(sels[0])
    tothis = prc.Dag(sels[1])
    copy_shape(fromthis, tothis)


def do_positions_match(posa, posb, tol=0.001):

    for ix in range(3):
        if not ((posb[ix] - tol) <= posa[ix] <= (posb[ix] + tol)):
            return False

    return True


def flip_shape(base_geo, target_geo, tol=0.01):
    """Flips the shape of the given target_geo.
    The given base_geo is used as the based symetrical shape
    for the calculation.

        Args:
            base_geo (prc.Dag):
            target_geo (prc.Dag):
            tol (float):
    """
    basedag = base_geo.m_dag_path
    targetdag = target_geo.m_dag_path

    bmfn = om.MFnMesh(basedag)
    tmfn = om.MFnMesh(targetdag)

    bpa = om.MPointArray()
    bmfn.getPoints(bpa, om.MSpace.kObject)

    tpa = om.MPointArray()
    tmfn.getPoints(tpa, om.MSpace.kObject)

    for ix in range(bpa.length()):

        px, py, pz = bpa[ix].x, bpa[ix].y, bpa[ix].z

        if px > tol:
            l_id = ix
            l_vtx = '{t}.vtx[{id}]'.format(t=target_geo.name, id=l_id)
            l_mpos = om.MPoint(tpa[l_id].x, tpa[l_id].y, tpa[l_id].z)

            # Finds a matched right ID.
            flip_point = om.MPoint(-px, py, pz)
            vtxdist = []
            for ix in range(bpa.length()):
                vtxdist.append([bpa[ix].distanceTo(flip_point)])

            if min(vtxdist)[0] > tol:
                continue

            r_id = vtxdist.index(min(vtxdist))
            r_vtx = '{t}.vtx[{id}]'.format(t=target_geo, id=r_id)
            r_mpos = om.MPoint(tpa[r_id].x, tpa[r_id].y, tpa[r_id].z)

            mc.xform(r_vtx, t=(l_mpos.x*(-1), l_mpos.y, l_mpos.z))
            mc.xform(l_vtx, t=(r_mpos.x*(-1), r_mpos.y, r_mpos.z))

        elif -tol < px <tol:
            m_id = ix
            m_vtx = '{t}.vtx[{m}]'.format(t=target_geo.name, m=m_id)
            m_mpos = om.MPoint(tpa[m_id].x, tpa[m_id].y, tpa[m_id].z)
        
            mc.xform(m_vtx, t=(m_mpos.x*(-1), m_mpos.y, m_mpos.z))
